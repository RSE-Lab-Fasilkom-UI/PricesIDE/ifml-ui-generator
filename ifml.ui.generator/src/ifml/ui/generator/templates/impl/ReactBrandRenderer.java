package ifml.ui.generator.templates.impl;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import ifml.ui.generator.services.ConsolePrinter;
import ifml.ui.generator.templates.ReactTemplateRenderer;

public class ReactBrandRenderer extends ReactTemplateRenderer {
	
	public ReactBrandRenderer(String productsName) {
		super("BrandTemplate", productsName);
	}

	@Override
	protected IFile getOutputFile(IProject targetProject) {
		// TODO Auto-generated method stub
		return targetProject.getFolder("src")
				.getFolder("commons")
				.getFolder("components")
				.getFolder("Brand")
				.getFile("index.js");
	}

}
