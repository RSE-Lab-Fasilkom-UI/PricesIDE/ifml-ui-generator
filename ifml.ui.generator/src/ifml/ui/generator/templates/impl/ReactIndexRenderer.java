package ifml.ui.generator.templates.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;

import freemarker.template.TemplateException;
import ifml.ui.generator.templates.ReactTemplateRenderer;

public class ReactIndexRenderer extends ReactTemplateRenderer {

	public ReactIndexRenderer(String productsName) {
		super("IndexTemplate", productsName);
	}

	@Override
	protected IFile getOutputFile(IProject targetProject) {
		return targetProject.getFolder("public")
				.getFile("index.html");
	}

}
