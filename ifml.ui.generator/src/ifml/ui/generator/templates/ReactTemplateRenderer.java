package ifml.ui.generator.templates;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ifml.ui.generator.Activator;

public abstract class ReactTemplateRenderer {
	
	private String templateName;
	private String productsName;
	
	public String getProductsName() {
		return productsName;
	}
	
	public ReactTemplateRenderer(String templateName, String productsName) {
		// TODO Auto-generated constructor stub
		this.templateName = templateName;
		this.productsName = productsName;
	}
	
	public void render(IProject targetProject) {
		Map<String, Object> dataModel = extractDataModel();
		Template template = TemplateManager.getTemplate(loadTemplateFilename());
		IFile outputFile = getOutputFile(targetProject);
		write(dataModel, template, outputFile);
	}
	
	protected Map<String, Object> extractDataModel() {
		Map<String, Object> dataModel = new HashMap();
		
		dataModel.put("productsname", this.productsName);
		
		return dataModel;
	}
	
	protected abstract IFile getOutputFile(IProject targetProject);
	
	private String loadTemplateFilename() {
		return this.templateName + ".ftl";
	};
	
	private void write(Map<String, Object> dataModel, Template template, IFile outputFile) {
		Writer writer = new StringWriter();
		try {
			template.process(dataModel, writer);
			writer.close();
		} catch (TemplateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		InputStream contentStream = new ByteArrayInputStream(writer.toString().getBytes());
		try {
			if (outputFile.exists()) outputFile.setContents(contentStream, false, false, null);
			else outputFile.create(contentStream, false, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
