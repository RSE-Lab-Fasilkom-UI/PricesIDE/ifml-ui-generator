package ifml.ui.generator.templates;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.osgi.framework.Bundle;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.Version;
import ifml.ui.generator.Activator;

public final class TemplateManager {
	private final static String TEMPLATE_PATH = "/templates/";
	private final static Map<String, File> templateFiles = new HashMap();
	
	private static File templateDir;
	
	public static void loadTemplate() {
		Bundle myBundle = Activator.getDefault().getBundle();
		URL entry = myBundle.getEntry(TEMPLATE_PATH);
		try {
			URL fileUrl = FileLocator.toFileURL(entry);
			URI locatorUri = new File(fileUrl.getFile()).toURI();
			templateDir = new File(locatorUri);
			for (File file : templateDir.listFiles()) {
				templateFiles.put(file.getName(), file);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Template getTemplate(String templateName) {
		if (templateDir == null) {
			loadTemplate();
		}
		File templateFile = templateFiles.get(templateName);
		try {
			FileInputStream templateStream = new FileInputStream(templateFile);
			String templateString = new String(templateStream.readAllBytes(), StandardCharsets.UTF_8);
			templateStream.close();
			StringTemplateLoader stringloader = new StringTemplateLoader();
			stringloader.putTemplate(templateName, templateString);
			Configuration configuration = new Configuration(new Version(2, 3, 31));
			configuration.setTemplateLoader(stringloader);
			return configuration.getTemplate(templateName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
