package ifml.ui.generator.utils;

import java.util.Arrays;
import java.util.Map;

public class TemplateUtils {
	static final String TEMPLATE_PATH = "app_example/";
	static String[] templateSelection = {"template1","template2","template3"};
	static final Map<String, String> TEMPLATE_SELECTION = Map.of(
		    "Standard", "standard", 
		    "Bookshelf", "bookshelf",
		    "Cushion", "cushion",
		    "Desk", "desk"
	);
	
	public static String[] getTemplateSelection() {
		String[] templateKeys = TEMPLATE_SELECTION.keySet().toArray(new String[TEMPLATE_SELECTION.size()]);
		Arrays.sort(templateKeys);
		return templateKeys;
	}
	
	public static String getValue(String selectedKey) {
		return TEMPLATE_SELECTION.get(selectedKey);
	}
	
	public static String getSelectedTemplatePath(String selection) {
		return TEMPLATE_PATH + "template_" + selection;
	}
	
	
}

