package ifml.ui.generator.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressConstants;
import org.osgi.framework.Bundle;

import ifml.ui.generator.Activator;
import ifml.ui.generator.templates.impl.ReactBrandRenderer;
import ifml.ui.generator.templates.impl.ReactIndexRenderer;

public final class TargetProjectMaker {
	
	public static Job getCopyTemplateJob(IProject targetProject,String template_path) {
		Job copyJob = null;
		try {
			Bundle myBundle = Activator.getDefault().getBundle();
			URL entry = myBundle.getEntry(template_path);
			URL fileUrl = FileLocator.toFileURL(entry);
			URI locatorUri = new File(fileUrl.getFile()).toURI();
			File exampleDir = new File(locatorUri);
			copyJob = Job.create("Copy Template to Target Folder", (ICoreRunnable) monitor -> {
				SubMonitor subMonitor = SubMonitor.convert(monitor);
				copyFolder(exampleDir, targetProject, subMonitor) ;
				subMonitor.done();
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return copyJob;
	}
	
	public static void showMonitor() {
		try {
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
			IWorkbenchPage page = win.getActivePage();
			IViewPart view = page.showView(IProgressConstants.PROGRESS_VIEW_ID);
			view.setFocus();
		} catch (PartInitException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void copyTemplate(IProject targetProject,String template_path) {
		try {
			Bundle myBundle = Activator.getDefault().getBundle();
			URL entry = myBundle.getEntry(template_path);
			URL fileUrl = FileLocator.toFileURL(entry);
			URI locatorUri = new File(fileUrl.getFile()).toURI();
			File exampleDir = new File(locatorUri);
			Job copyJob = Job.create("Copy Template to Target Folder", (ICoreRunnable) monitor -> {
				SubMonitor subMonitor = SubMonitor.convert(monitor);
				copyFolder(exampleDir, targetProject, subMonitor) ;
				subMonitor.done();
			});
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
			IWorkbenchPage page = win.getActivePage();
			IViewPart view = page.showView(IProgressConstants.PROGRESS_VIEW_ID);
			view.setFocus();
			copyJob.schedule();
		} catch (IOException | PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void insertProductsNameToApp(IProject targetProject, String productsName) {
		generateTemplate(targetProject, productsName);
	}
	
	private static void copyFolder(File folder, IProject targetProject, SubMonitor subMonitor) {
		copyFolder(folder, "", targetProject, subMonitor);
	}
	
	private static void copyFolder(File folder, String prefix, IProject targetProject, SubMonitor subMonitor) {
		File[] folderContents = folder.listFiles();
		subMonitor.setWorkRemaining(folderContents.length);
		for (File file : folderContents) {
			String pathName = prefix + file.getName();
			SubMonitor childMonitor = subMonitor.newChild(1);
			childMonitor.subTask(pathName);
			if (file.isDirectory()) {
				IFolder targetFolder = targetProject.getFolder(pathName);
				if (!targetFolder.exists()) {
					try {
						targetFolder.create(false, false, null);
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				copyFolder(file, prefix + file.getName() + File.separator, targetProject, subMonitor);
			} else {
				IFile targetFile = targetProject.getFile(pathName);
				copyFile(file, targetFile);
			}
		}
	}
	
	private static void copyFile(File source, IFile target) {
		try {
			InputStream fileInputStream = new FileInputStream(source);
			if (target.exists()) {
				target.setContents(fileInputStream, true, false, null);
			} else {
				target.create(fileInputStream, true, null);
			}
		} catch (FileNotFoundException | CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void generateTemplate(IProject targetProject, String productsName) {
		new ReactIndexRenderer(productsName).render(targetProject);
		new ReactBrandRenderer(productsName).render(targetProject);
	}

}
