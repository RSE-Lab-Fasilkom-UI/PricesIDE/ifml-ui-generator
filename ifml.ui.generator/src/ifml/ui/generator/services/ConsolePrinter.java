package ifml.ui.generator.services;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsolePrinter {
	final static String CONSOLE_NAME = "IFML Console";
	
	private static MessageConsoleStream consoleStream = initConsole();
	
	public ConsolePrinter() {};
	
	private static MessageConsoleStream initConsole() {
		MessageConsole console = new MessageConsole(CONSOLE_NAME, null);
		console.activate();
		ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ (IConsole) console });
		return console.newMessageStream();
	}
	
	public static void println() {
		consoleStream.println();
	}
	
	public static void println(String message) {
		consoleStream.println(message);
	}
	
	public static void print(String message) {
		consoleStream.print(message);
	}
}
