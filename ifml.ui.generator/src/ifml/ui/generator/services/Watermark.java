package ifml.ui.generator.services;

import java.time.LocalDate;
import java.time.format.*;

public class Watermark {
	public static String version;
	public static int year;
	public static int month;
	public static int date;
	
	public Watermark(String version, int year, int month, int date) {
		Watermark.version = version;
		Watermark.year = year;
		Watermark.month = month;
		Watermark.date = date;
	}
	
	public static String getTodayDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate result = LocalDate.of(year, month, date);
	    return result.format(formatter);  
	}
	
	public static String generateWatermark() {
		return String.format("/*\n\tGenerated on %s by UI Generator PRICES-IDE\n\thttps://amanah.cs.ui.ac.id/research/ifml-regen\n\tversion %s\n*/", getTodayDate(), version);
	}
}
