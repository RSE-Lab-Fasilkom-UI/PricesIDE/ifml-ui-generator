package ifml.ui.generator.services;

import java.io.File;
import java.util.Scanner;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.io.UnsupportedEncodingException;

public class SelectionName {
	//sumber : urldecoder.io
	public static String decodeValue(String value) {
        try {
            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
	
	public static String getType(String type){
        try {
        	if(type.contains("eProxyURI:")){
	            String getProxyURI = type.split("eProxyURI: file:/")[1].substring(0,type.split("eProxyURI: file:/")[1].length()-1);
	            String[] files = getProxyURI.split("#");
	            String fileName = decodeValue(files[0]);
	            String keyword = files[1];
	            File f = new File(fileName);
	            Scanner reader = new Scanner(f);
	            while (reader.hasNextLine()) {
	                String data = reader.nextLine();
	                if (data.contains("id=\""+keyword+"\"")){
	                    String getName = data.split("name=\"")[1].split("\"")[0];
	                    return getName;
	                }
	            }
	            reader.close();
        	} else {
	              String[] arrSplit = type.split("name: ");
	              String className = arrSplit[1].split(",")[0];
	              return className;
        	}
        } catch (Exception e) {
        	e.printStackTrace();
        	ConsolePrinter.println("error in SelectionName.java");
        	ConsolePrinter.println(e.toString());
            return "An error occurred.";
        }
        return "not found"; 
    }
}
//
//import java.io.File;
//import java.util.Scanner;
//import java.net.URLDecoder;
//import java.nio.charset.StandardCharsets;
//import java.io.UnsupportedEncodingException;
//
//public class SelectionName {
//	//sumber : urldecoder.io
//	public static String decodeValue(String value) {
//        try {
//            return URLDecoder.decode(value, StandardCharsets.UTF_8.toString());
//        } catch (UnsupportedEncodingException ex) {
//            throw new RuntimeException(ex.getCause());
//        }
//    }
//	
//	public static String getType(String type){
//        try {
//        	ConsolePrinter.println(type);
//            if(type.contains("eProxyURI:")){
//            	ConsolePrinter.println("case 1");
//                String getProxyURI = type.split("eProxyURI: file:/")[1].substring(0,type.split("eProxyURI: file:/")[1].length()-1);
//                String[] files = getProxyURI.split("#");
//                String fileName = decodeValue(files[0]);
//                String keyword = files[1];
//                File f = new File(fileName);
//                Scanner reader = new Scanner(f);
//                while (reader.hasNextLine()) {
//                    String data = reader.nextLine();
//                    if (data.contains("id=\""+keyword+"\"")){
//                        String getName = data.split("name=\"")[1].split("\"")[0];
//                        ConsolePrinter.println(getName);
//                        return getName;
//                    }
//                }
//                reader.close();
//            } else {
//            	ConsolePrinter.println("case 2");
//                String[] arrSplit = type.split("name: ");
//                ConsolePrinter.println(arrSplit[1]);
//                String className = arrSplit[1].split(",")[0];
//                ConsolePrinter.println(className);
//                return className;
//            }
//            
//        } catch (Exception e) {
//        	e.printStackTrace();
//        	ConsolePrinter.println("error in SelectionName.java");
//            return "An error occurred.";
//        }
//        return "not found"; 
//    }
//    
//}
//
