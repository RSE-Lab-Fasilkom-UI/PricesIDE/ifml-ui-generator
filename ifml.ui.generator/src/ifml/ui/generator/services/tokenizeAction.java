package ifml.ui.generator.services;

import org.eclipse.ocl.util.Bag;

public class tokenizeAction {
	public static String getMethod(String actionName) {
		String[] tokenize = actionName.split(" "); 
		return tokenize[0];
	}
	
	public static String getNameService(String actionName) {
		String[] tokenize = actionName.split(" "); 
		return tokenize[tokenize.length-1];
	}
	public static String concatTokens(Bag<String> bags) {
		String result = "";
		for (String str : bags) {
			result.concat(str);
		}
		return result;
	}
}
