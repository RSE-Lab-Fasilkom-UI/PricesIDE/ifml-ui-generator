import React from "react";

import { Spinner } from "commons/components";

const DetailContainerLayout = ({
  title,
  singularName,
  isLoading,
  items,
  children,
  isCorrelatedWithAnotherComponent,
}) => {
  return (
    <div className={`prose ${isCorrelatedWithAnotherComponent ? 'w-full max-w-screen-xl' : 'max-w-screen-lg'} mx-auto p-6`}>
      {isLoading ? (
        <div className={"py-8 text-center"}>
          <Spinner />
        </div>
      ) : Object.keys(items).length ? (
        <>
          <h2>{title}</h2>
          <div className='flex flex-row justify-between'>
            {children}
          </div>
        </>
      ) : (
        <div className="py-8 text-center">
          Tidak ada detail data untuk ditampilkan
        </div>
      )}
    </div>
  );
};

export default DetailContainerLayout;
