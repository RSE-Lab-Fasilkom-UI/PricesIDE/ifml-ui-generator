import React from "react";

import { ListItem, VisualizationAttr } from "commons/components";

const CardRow = ({ item, itemsAttrs, itemsEvents }) => {
  return (
    <ListItem>
      <figure>
        {/* {imageVA} */}
        {itemsAttrs?.map(
          (itemsAttr) =>
            itemsAttr.featureName == "logoUrl" && (
              <VisualizationAttr
                content={item[itemsAttr.featureName]}
              />
            )
        )}
      </figure>
      <div className="card-body">
        {itemsAttrs?.map(
          (itemsAttr) =>
            itemsAttr.featureName != "logoUrl" && (
              <VisualizationAttr
                content={item[itemsAttr.featureName]}
              />
            )
        )}
        <div className="card-actions justify-end">
          {/* View Element Event [singularName /] Card Element*/}
          {itemsEvents(item)?.map((event) => event)}
        </div>
      </div>
    </ListItem>
  );
};

export default CardRow;