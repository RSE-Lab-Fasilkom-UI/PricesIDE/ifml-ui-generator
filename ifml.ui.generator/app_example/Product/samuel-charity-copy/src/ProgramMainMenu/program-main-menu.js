import React from 'react'
import MenuItem from '../components/MenuItem/MenuItem'
import MenuLink from '../components/MenuLink/MenuLink'
import queryString from 'query-string'
import CallProgramListService from '../services/call-program-list.service'

class ProgramMainMenu extends React.Component {
  state = {}
  onClickFunc = async () => {
    const data = await CallProgramListService.call()

    this.props.history.push({
      pathname: '/daftar-program',
      search: queryString.stringify({
        jsonAllProgram: JSON.stringify(
          data['data'] ? data['data']['data'] : []
        ),
      }),
    })
  }

  currencyFormatDE(num) {
    if (!num) {
      return '0,00'
    }
    return num
      .toFixed(2)
      .replace('.', ',')
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

  render() {
    return (
      <MenuItem variant={this.props.variant}>
        <MenuLink
          variant={this.props.variant}
          href="#"
          onClick={e => {
            e.preventDefault()
            this.onClickFunc()
          }}
        >
          Program
        </MenuLink>
      </MenuItem>
    )
  }
}

export default ProgramMainMenu
