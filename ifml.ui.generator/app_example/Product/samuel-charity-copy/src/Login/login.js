import React from 'react'
import { Navigate } from 'react-router-dom'
import _debounce from 'lodash/debounce'
import { AuthConsumer } from '../Authentication'
import Button from '../components/Button/Button'
import Detail from '../components/Detail/Detail'

class LoginPage extends React.Component {
  handleLoginWithGoogle = () => {
    this.props.loginGoogle()
  }

  render() {
    if (this.props.isAuth) {
      return <Navigate to="/" />
    }

    return (
      <div>
        {/* <div id={GOOGLE_BUTTON_ID} /> */}
        <Detail
          variant={{ shape: 'default', borderColor: 'blue', bgColor: 'white' }}
        >
          <Button
            onClick={this.handleLoginWithGoogle}
            text="Login with Google"
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
          />
        </Detail>
      </div>
    )
  }
}

const withAuth = props => (
  <AuthConsumer>{values => <LoginPage {...props} {...values} />}</AuthConsumer>
)

export default withAuth
