import React from 'react'

const TableBody = props => {
  return <tbody className={`tbody_base`}>{props.children}</tbody>
}
export default TableBody
