import React from 'react'

const TableCell = props => {
  const { content, currency_fmt } = props

  const format_currency = (currency, amount) => {
    switch (currency) {
      case 'CA':
        return new Intl.NumberFormat('en-CA', {
          currency: 'CAD',
          style: 'currency',
        }).format(amount)
      case 'JP':
        return new Intl.NumberFormat('jp-JP', {
          style: 'currency',
          currency: 'JPY',
        }).format(amount)
      case 'US':
        return new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'USD',
        }).format(amount)
      case 'ID':
        return new Intl.NumberFormat('id-ID', {
          style: 'currency',
          currency: 'IDR',
        }).format(amount)
      default:
        return new Intl.NumberFormat('id-ID', {
          style: 'currency',
          currency: 'IDR',
        }).format(amount)
    }
  }

  return (
    <td
      data-target={content}
      style={{ textAlign: currency_fmt ? 'right' : 'inherit' }}
    >
      {currency_fmt ? format_currency(currency_fmt, content) : content}
      {props.children}
    </td>
  )
}

export default TableCell
