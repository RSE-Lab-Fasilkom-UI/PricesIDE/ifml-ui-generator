import React from 'react'
import { TableStyle } from './style'

const Table = props => {
  const { variant } = props

  return <TableStyle {...variant}>{props.children}</TableStyle>
}

export default Table
