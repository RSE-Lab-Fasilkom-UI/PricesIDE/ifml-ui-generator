import styled from 'styled-components'

export const MenuChildrenStyle = styled.div`
  display: flex;

  .submenu {
    display: none;
    left: 0;
    position: absolute;
    top: 100%;
    list-style-type: none;
    margin: 0;
    padding: 0;
    z-index: 9;
    width: auto;
    white-space: nowrap;
    background-color: ${props => (props.bgColor ? props.bgColor : 'lightgray')};
  }

  .submenu .menu_list {
    width: 100%;
  }

  .submenu .submenu {
    left: 100%;
    position: absolute;
    top: 0;
  }

  .menu_list:hover > .submenu {
    display: block;
  }
`
