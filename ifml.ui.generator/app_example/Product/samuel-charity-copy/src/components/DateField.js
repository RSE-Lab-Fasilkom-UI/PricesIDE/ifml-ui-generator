import React, { forwardRef, useState } from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import DateInputField from './DateInputField'

const DateField = props => {
  const {
    camel_name,
    dasherize_name,
    label,
    placeholder,
    default_value,
    ref_func,
  } = props
  const dasherized = 'input-' + dasherize_name

  const [startDate, setStartDate] = useState(null)

  return (
    <div>
      <label htmlFor={dasherized}>
        <strong>{label}</strong>
      </label>
      <DatePicker
        selected={startDate}
        onChange={date => setStartDate(date)}
        customInput={
          <DateInputField
            camel_name={camel_name}
            dasherized={dasherized}
            placeholder={placeholder}
            default_value={default_value}
            ref={ref_func}
          />
        }
      />
      <br />
    </div>
  )
}

export default DateField
