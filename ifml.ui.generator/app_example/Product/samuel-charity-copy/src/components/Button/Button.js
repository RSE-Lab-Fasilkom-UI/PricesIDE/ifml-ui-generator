import React from 'react'
import PropTypes from 'prop-types'
import { ButtonContainer, ButtonStyle } from './style'

const Button = props => {
  const { variant, disabled, text, onClick } = props

  return (
    <ButtonContainer>
      <ButtonStyle disabled={disabled} {...variant} onClick={onClick}>
        {text}
      </ButtonStyle>
    </ButtonContainer>
  )
}

Button.propTypes = {
  variant: PropTypes.shape({
    shape: PropTypes.oneOf(['default', 'rounded']),
    color: PropTypes.string,
    bgColor: PropTypes.string,
    borderColor: PropTypes.string,
  }),
  disabled: PropTypes.bool,
  text: PropTypes.string,
  onClick: PropTypes.func,
}

Button.defaultProps = {
  variant: {
    shape: 'default',
    color: '#101010',
    bgColor: 'orange',
    borderColor: 'orange',
  },
  disabled: false,
  text: 'Button',
  onClick: undefined,
}

export default Button
