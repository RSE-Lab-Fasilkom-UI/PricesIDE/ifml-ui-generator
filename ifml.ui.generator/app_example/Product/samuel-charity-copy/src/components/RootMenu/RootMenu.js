import React from 'react'
import { RootMenuStyle } from './style'

const RootMenu = props => {
  const { variant } = props

  const renderChildren = () => {
    if (props.isAuth) {
      return (
        <RootMenuStyle {...variant}>
          <span className="root_span">
            <React.Fragment>{props.children}</React.Fragment>
          </span>
        </RootMenuStyle>
      )
    }
    return null
  }

  return renderChildren()
}

export default RootMenu
