import styled from 'styled-components'

export const MenuItemStyle = styled.div`
  display: flex;
  .menu_list {
    float: left;
    position: relative;
  }

  .menu_list:hover {
    background-color: ${props =>
      props.itemBgColor ? props.itemBgColor : 'gray'};
  }
`
