import React from 'react'

const MenuItem = props => {
  return <li className="menu_list">{props.children}</li>
}

export default MenuItem
