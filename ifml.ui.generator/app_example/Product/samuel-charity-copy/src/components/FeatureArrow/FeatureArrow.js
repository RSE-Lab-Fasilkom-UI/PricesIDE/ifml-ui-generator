import React from 'react'
import './FeatureArrow.css'

const FeatureArrow = props => {
  const { feat_type } = props

  let feature_class

  switch (feat_type) {
    case 'root':
      feature_class = 'root_feature'
      break
    case 'sub':
      feature_class = 'sub_feature'
      break
    default:
      feature_class = 'sub_feature'
      break
  }
  return <span className={`arrow ${feature_class}`} />
}

export default FeatureArrow
