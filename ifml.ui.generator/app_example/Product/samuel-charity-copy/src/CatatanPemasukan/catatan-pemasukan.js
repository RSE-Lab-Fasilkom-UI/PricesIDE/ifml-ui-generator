//Catatan Pemasukan Containers
import React from 'react'
import { AuthConsumer } from '../Authentication'
import queryString from 'query-string'

import IncomeMenuFeatureComponent from '../IncomeMenuFeature/income-menu-feature.js'

import Table from '../components/Table/Table'
import TableRow from '../components/Table/TableRow'
import TableCell from '../components/Table/TableCell'
import TableBody from '../components/Table/TableBody'
import TableHead from '../components/Table/TableHead'

import TableIncomeContentComponent from '../TableIncomeContent/table-income-content.js'

class CatatanPemasukan extends React.Component {
  state = {}

  componentDidMount = async () => {
    this.setState({
      jsonAllIncome: JSON.parse(
        queryString.parse(this.props.location.search).jsonAllIncome
      ),
    })
  }

  render() {
    return (
      <React.Fragment>
        <AuthConsumer>
          {values => {
            return <IncomeMenuFeatureComponent {...values} />
          }}
        </AuthConsumer>
        <pre>{JSON.stringify(this.state.jsonAllIncome, 0, 2)}</pre>
        <Table
          id="table-TableIncomeContentComponent"
          variant={{
            headBgColor: 'blue',
            headColor: 'white',
            headTextAlign: 'center',
            headBorderColor: 'blue',
            headUppercase: 'yes',
            headFontWeight: '700',
            bodyBorderColor: 'black',
            bodyFontWeight: 'normal',
            evenBgColor: 'lightgray',
            evenColor: 'black',
            evenBorderColor: 'black',
            distinctBgColor: 'lightblue',
            distinctColor: 'black',
          }}
        >
          <TableHead>
            <TableRow>
              {/* //Data Binding Income List Element*/}
              <TableCell id="Tanggal">Tanggal</TableCell>
              <TableCell id="Program">Program</TableCell>
              <TableCell id="Jumlah">Jumlah</TableCell>
              <TableCell id="Deskripsi">Deskripsi</TableCell>
              <TableCell id="Metode Pembayaran">Metode Pembayaran</TableCell>
              <TableCell id="Detail">Detail</TableCell>
              <TableCell id="Edit">Edit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.jsonAllIncome &&
              this.state.jsonAllIncome.map(jsonAllIncome => (
                <TableIncomeContentComponent
                  {...this.props}
                  jsonAllIncome={jsonAllIncome}
                />
              ))}
          </TableBody>
        </Table>
      </React.Fragment>
    )
  }
}

export default CatatanPemasukan
