//Halaman Tambah Program Containers
import React from 'react'
import { AuthConsumer } from '../Authentication'
import queryString from 'query-string'
import { withRouter } from 'react-router-dom'

import TambahkanProgramComponent from '../TambahkanProgram/tambahkan-program.js'

class HalamanTambahProgram extends React.Component {
  state = {}

  componentDidMount = async () => {
    this.setState({
      namaProgram: JSON.parse(
        queryString.parse(this.props.location.search).namaProgram
      ),
      deskripsi: JSON.parse(
        queryString.parse(this.props.location.search).deskripsi
      ),
      target: JSON.parse(queryString.parse(this.props.location.search).target),
      partner: JSON.parse(
        queryString.parse(this.props.location.search).partner
      ),
      urlGambarProgram: JSON.parse(
        queryString.parse(this.props.location.search).urlGambarProgram
      ),
    })
  }

  render() {
    return (
      <React.Fragment>
        {
          <TambahkanProgramComponent
            {...this.props}
          ></TambahkanProgramComponent>
        }
      </React.Fragment>
    )
  }
}

export default HalamanTambahProgram
