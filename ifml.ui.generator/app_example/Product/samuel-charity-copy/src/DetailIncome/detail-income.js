import React from 'react'
import queryString from 'query-string'
import Button from '../components/Button/Button'
import Detail from '../components/Detail/Detail'
import VisualizationAttr from '../components/VisualizationAttr/VisualizationAttr'
import CallincomedeleteService from '../services/call-income-delete.service'
class DetailIncome extends React.Component {
  state = {}

  componentWillMount = () => {
    this.incomeData = this.props.objectDetailIncome
    this.idIncome = String(this.incomeData.id)
  }

  Hapus = async () => {
    const data = await CallincomedeleteService.call({
      //call/income/delete
      id: this.idIncome,
    })

    this.props.history.push({
      pathname: '/catatan-pemasukan',
      search: queryString.stringify({
        jsonAllIncome: JSON.stringify(data['data'] ? data['data']['data'] : []),
      }),
    })
  }
  render() {
    return (
      <Detail
        variant={{ shape: 'default', borderColor: 'blue', bgColor: 'white' }}
      >
        {/* //Data Binding Income Data*/}
        <VisualizationAttr
          title_name="Tanggal"
          content={this.incomeData && this.incomeData.datestamp}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <VisualizationAttr
          title_name="Deskripsi"
          content={this.incomeData && this.incomeData.description}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <VisualizationAttr
          title_name="Jumlah"
          content={this.incomeData && this.incomeData.amount}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <VisualizationAttr
          title_name="Nama Program"
          content={this.incomeData && this.incomeData.programName}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <VisualizationAttr
          title_name="Jenis Pemasukan"
          content={this.incomeData && this.incomeData.idCoa}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <VisualizationAttr
          title_name="Metode Pembayaran"
          content={this.incomeData && this.incomeData.paymentMethod}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <Button
          onClick={e => {
            e.preventDefault()
            this.Hapus()
          }}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Hapus"
        />
      </Detail>
    )
  }
}

export default DetailIncome
