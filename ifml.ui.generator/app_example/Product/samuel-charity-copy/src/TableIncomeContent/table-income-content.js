//Table
import React from 'react'
import Button from '../components/Button/Button'
import TableRow from '../components/Table/TableRow'
import TableCell from '../components/Table/TableCell'
import queryString from 'query-string'
import CallincomedetailService from '../services/call-income-detail.service'

class TableIncomeContent extends React.Component {
  state = {}

  componentWillMount = () => {
    this.incomeListElement = this.props.jsonAllIncome
    this.idIncome = this.incomeListElement.id
  }
  currencyFormatDE(num) {
    if (!num) {
      return 0
    }
    return num
  }

  indentBasedOnLevel(content, level) {
    switch (level) {
      case 1:
        return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
      case 2:
        return (
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
        )
      case 5:
        return (
          <span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {content}
          </span>
        )
      default:
        return content
    }
  }

  setDistinctRow(content) {
    const DISTINCT_ROW = [
      'Aktivitas Operasional',
      'Aktivitas Investasi',
      'Aktivitas Pendanaan',
      '',
    ]
    if (DISTINCT_ROW.indexOf(content) > -1) {
      return true
    }
    return false
  }

  Detail = async () => {
    const data = await CallincomedetailService.call({
      id: this.idIncome,
    })

    this.props.history.push({
      pathname: '/halaman-detail-pemasukan',
      search: queryString.stringify({
        objectDetailIncome: JSON.stringify(
          data['data'] ? data['data']['data'] : []
        ),
      }),
    })
  }
  Edit = async () => {
    this.props.history.push({
      pathname: '/halaman-ubah-pemasukan',
      search: queryString.stringify({
        objectIncome: JSON.stringify(this.incomeListElement),
      }),
    })
  }

  render() {
    return (
      <TableRow distinct={false}>
        {/* //Data Binding Income List Element*/}

        <TableCell
          content={this.incomeListElement && this.incomeListElement.datestamp}
        />

        <TableCell
          content={this.incomeListElement && this.incomeListElement.programName}
        />

        <TableCell
          content={this.incomeListElement && this.incomeListElement.amount}
        />

        <TableCell
          content={this.incomeListElement && this.incomeListElement.description}
        />

        <TableCell
          content={
            this.incomeListElement && this.incomeListElement.paymentMethod
          }
        />
        <TableCell variant='{"borderColor": "blue", "bgColor": "white", "shape": "default", "alignment": "left"}'>
          <Button
            onClick={e => {
              e.preventDefault()
              this.Detail()
            }}
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
            text="Detail"
          />
        </TableCell>
        <TableCell variant='{"borderColor": "blue", "bgColor": "white", "shape": "default", "alignment": "left"}'>
          <Button
            onClick={e => {
              e.preventDefault()
              this.Edit()
            }}
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
            text="Edit"
          />
        </TableCell>
      </TableRow>
    )
  }
}

export default TableIncomeContent
