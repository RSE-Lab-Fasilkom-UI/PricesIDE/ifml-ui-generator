import React from "react";
import { Button, Form, InputField } from "../components";

const StaticPageSaveForm = ({ onSubmit, onCancel }) => {
  let inputTitle = ''

  const handleSubmitData = (e) => {
    e.preventDefault()
    if (inputTitle?.value !== '') {
      onSubmit(inputTitle.value);
    } else {
      alert('Title cannot be empty')
    }
  }

  return (
    <div style={{backgroundColor: 'rgb(0, 0, 0, 0.6)'}}>
      <Form
        title="Tambahkan Pemasukan"
        id_name="tambahkan-pemasukan"
        variant={{
          shape: 'default',
          color: 'blue',
          borderColor: 'transparent',
          bgColor: 'white',
          alignment: 'center',
        }}
        on_submit={handleSubmitData}
      >
        <InputField
          camel_name="title"
          type=""
          dasherized="input-title"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Title"
          ref_func={e => {
            inputTitle = e
          }}
        />
        <Button
          onClick={handleSubmitData}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Save"
        />
        <Button
          onClick={onCancel}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Cancel"
        />
      </Form>
    </div>
  )
}

export default StaticPageSaveForm;