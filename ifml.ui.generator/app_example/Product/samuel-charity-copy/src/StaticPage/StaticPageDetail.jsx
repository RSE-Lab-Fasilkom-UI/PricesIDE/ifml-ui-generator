import axios from 'axios';
import { useState, useEffect } from 'react';
import ReactHTMLParser from 'html-react-parser';
import { useNavigate, useParams } from 'react-router-dom';

import { Button } from '../components';


const StaticPageDetail = () => {
  const { staticPageId } = useParams();
  const navigate = useNavigate()
  
  const [ isLoading, setIsLoading ] = useState(true);
  const [ isError, setIsError ] = useState(false);
  const [ errorMessage, setErrorMessage ] = useState('');
  const [ htmlData, setHtmlData ] = useState();

  const handleEditStaticPage = () => {
    navigate(`/static-page/edit/${staticPageId}`)
  }

  // References: https://stackoverflow.com/a/60533623
  useEffect(() => {
    const head = document.head;
    const style = document.createElement('style')
    const setStyleElement = (cssData = '') => {
      style.innerHTML = cssData;

      head.appendChild(style);
    }

    const fetchData = async() => {
      try {
        const response = await axios.get(`http://localhost:3003/static-data/${staticPageId}`);
        setStyleElement(response.data.cssData)
        setHtmlData(response.data.htmlData)
        setIsLoading(false)
      } catch (e) {
        setIsLoading(false)
        setIsError(true)
        setErrorMessage(e?.response?.statusText || 'Failed, please try again later!')
      }
    }

    fetchData()

    return () => {
      if (head.contains(style)) head.removeChild(style)
    }
  }, [staticPageId])

  if (isLoading) return (<p>Loading....</p>)
  
  if (isError) return (<p>Error - {errorMessage}</p>)

  return (
    <>
      {htmlData && ReactHTMLParser(htmlData)}
      <Button
        text='Edit'
        variant={{
          shape: 'rounded',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
          position: 'fixed',
          right: 0,
          bottom: 0,
          maxWidth: '100px',
        }}
        onClick={handleEditStaticPage}
      />
    </>
  );
}

export default StaticPageDetail;