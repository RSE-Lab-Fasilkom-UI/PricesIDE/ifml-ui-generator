import React, { useEffect, useState, useCallback } from "react";
import grapesjs from 'grapesjs'
import 'grapesjs/dist/css/grapes.min.css'
import 'grapesjs/dist/grapes.min.js'
import 'grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min.css'
import 'grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min.js'
import axios from "axios";
import generateUuid from 'uuid-random';

import { Button } from "../components";
import StaticPageSaveForm from "./StaticPageSaveForm";

const StaticPageBuilder = ({
  staticId = null,
  initialComponentData = null,
  initialStyleData = null,
  isEditMode = false,
  actionSaveData = () => null,
}) => {
  const [ editor, setEditor ] = useState({});
  const [ isFormVisible, setIsFormVisible ] = useState(false);

  useEffect(() => {
    console.log('init')
    const _editor = grapesjs.init({
      container: '#gjs',
      height: '560px',
      width: '100%',
      plugins: ['gjs-preset-webpage'],
      storageManager: {
        type: 'local',
        autosave: true,
      },
      deviceManager: {
        devices:
        [
          {
            id: 'desktop',
            name: 'Desktop',
            width: '',
          },
          {
            id: 'tablet',
            name: 'Tablet',
            width: '768px',
            widthMedia: '992px',
          },
          {
            id: 'mobilePortrait',
            name: 'Mobile portrait',
            width: '320px',
            widthMedia: '575px',
          },
        ]
      },
      styleManager : {
        sectors: false,
      },
      pluginsOpts: {
        'gjs-preset-webpage': {
          blocksBasicOpts:  {
            blocks: ['text', 'image', 'video', 'column1', 'column2', 'column3', 'column3-7', 'link'],
            flexGrid: 1,
          },
          navbarOpts: false,
          countdownOpts: false,
          formsOpts: false,
          exportOpts: false,
          blocks: [],
          modalImportContent: editor => editor.getHtml()
        },
      }
    })

    _editor.setComponents(initialComponentData);
    _editor.setStyle(initialStyleData);

    setEditor(_editor);
  }, [initialComponentData, initialStyleData, isFormVisible])

  const actionSaveToDB = useCallback((title) => {
    console.log('actionSaveToDB')
    const staticPageId = `${title}-${generateUuid()}`

    const htmlData = editor.getHtml();
    const cssData = editor.getCss();
    try {
      axios.post(`http://localhost:3003/static-data`, {
        'id': staticPageId,
        htmlData,
        cssData,
      })
      alert(`Static Page with id '${staticPageId}' successfully created`)
      setIsFormVisible(false)
    } catch(e) {
      alert(e?.response?.statusText || 'Failed to create static page')
    }
    
  }, [editor])

  const actionUpdateDB = useCallback(() => {
    console.log('actionUpdateDB')
    const htmlData = editor.getHtml();
    const cssData = editor.getCss();
    try {
      axios.patch(`http://localhost:3003/static-data/${staticId}`, {
        htmlData,
        cssData,
      })
      alert(`Static Page with id '${staticId}' successfully updated`)
      actionSaveData()
    } catch(e) {
      alert(e?.response?.statusText || 'Failed to create static page')
    }
    
  }, [editor, staticId, actionSaveData])

  return (
    <>
      {isFormVisible ? (
        <StaticPageSaveForm
          onSubmit={actionSaveToDB}
          onCancel={() => setIsFormVisible(false)}
        />
      ) : (
        <>
          <Button
            onClick={isEditMode ? actionUpdateDB : () => setIsFormVisible(true)}
            text='Save'
            variant={{
              shape: 'rounded',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
          />
          <div id="gjs"></div>
        </>
      )}
    </>
  );
}

export default StaticPageBuilder;