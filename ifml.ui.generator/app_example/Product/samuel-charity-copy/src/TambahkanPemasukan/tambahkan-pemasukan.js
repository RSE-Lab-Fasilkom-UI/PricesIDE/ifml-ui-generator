import React from 'react'
import InputField from '../components/InputField/InputField'
import SelectionField from '../components/SelectionField/SelectionField'
import Form from '../components/Form/Form'
import Button from '../components/Button/Button'
import queryString from 'query-string'
import CallincomesaveService from '../services/call-income-save.service'

class TambahkanPemasukan extends React.Component {
  state = {}
  componentWillMount = () => {
    this.program = this.props.program
    this.chartofaccount = this.props.chartofaccount
  }

  Kirim = async () => {
    const data = await CallincomesaveService.call({
      datestamp: this.tanggalInput.value,
      description: this.deskripsiInput.value,
      amount: this.jumlahInput.value,
      idProgram: this.namaProgramTerkaitInput.value,
      idCoa: this.kodeAkunInput.value,
      paymentMethod: this.metodePembayaranInput.value,
    })

    this.props.history.push({
      pathname: '/catatan-pemasukan',
      search: queryString.stringify({
        jsonAllIncome: JSON.stringify(data['data'] ? data['data']['data'] : []),
      }),
    })
  }

  render() {
    return (
      <Form
        title="Tambahkan Pemasukan"
        id_name="tambahkan-pemasukan"
        variant={{
          shape: 'default',
          color: 'blue',
          borderColor: 'transparent',
          bgColor: 'white',
          alignment: 'center',
        }}
      >
        <InputField
          camel_name="tanggalInput"
          type=""
          dasherized="input-tanggal"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Tanggal"
          placeholder="Fill the Tanggal"
          ref_func={e => {
            this.tanggalInput = e
          }}
        />
        <InputField
          camel_name="deskripsiInput"
          type=""
          dasherized="input-deskripsi"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Deskripsi"
          placeholder="Fill the Deskripsi"
          ref_func={e => {
            this.deskripsiInput = e
          }}
        />
        <InputField
          camel_name="jumlahInput"
          type=""
          dasherized="input-jumlah"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Jumlah"
          placeholder="Fill the Jumlah"
          ref_func={e => {
            this.jumlahInput = e
          }}
        />
        <InputField
          camel_name="metodePembayaranInput"
          type=""
          dasherized="input-metode-pembayaran"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Metode Pembayaran"
          placeholder="Fill the Metode Pembayaran"
          ref_func={e => {
            this.metodePembayaranInput = e
          }}
        />
        <SelectionField
          options={this.program}
          camel_name="namaProgramTerkaitInput"
          type=""
          dasherized="input-nama-program-terkait"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Nama Program Terkait"
          placeholder="Fill the Nama Program Terkait"
          ref_func={e => {
            this.namaProgramTerkaitInput = e
          }}
        />
        <SelectionField
          options={this.chartofaccount}
          camel_name="kodeAkunInput"
          type=""
          dasherized="input-kode-akun"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Kode Akun"
          placeholder="Fill the Kode Akun"
          ref_func={e => {
            this.kodeAkunInput = e
          }}
        />
        <Button
          onClick={e => {
            e.preventDefault()
            this.Kirim()
          }}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Kirim"
        />
      </Form>
    )
  }
}

export default TambahkanPemasukan
