import React from 'react'
import { AuthProvider, AuthConsumer, withAuth } from '../Authentication'
import { BrowserRouter, Route, Link, Routes } from 'react-router-dom'
import { CookiesProvider } from 'react-cookie'
import { RootMenu, MenuItem, MenuLink } from '../components'
import FeatureMainMenuComponent from '../FeatureMainMenu/feature-main-menu.js'

//Program
import DaftarProgramComponent from '../DaftarProgram/daftar-program.js'
import HalamanTambahProgramComponent from '../HalamanTambahProgram/halaman-tambah-program.js'
import HalamanDetailProgramComponent from '../HalamanDetailProgram/halaman-detail-program.js'
import HalamanUbahProgramComponent from '../HalamanUbahProgram/halaman-ubah-program.js'

//Income
import CatatanPemasukanComponent from '../CatatanPemasukan/catatan-pemasukan.js'
import HalamanTambahPemasukanComponent from '../HalamanTambahPemasukan/halaman-tambah-pemasukan.js'
import HalamanDetailPemasukanComponent from '../HalamanDetailPemasukan/halaman-detail-pemasukan.js'
import HalamanUbahPemasukanComponent from '../HalamanUbahPemasukan/halaman-ubah-pemasukan.js'

import LoginComponent from '../Login/login.js'
import HomepageComponent from '../Homepage/homepage.js'
import LandingPageComponent from '../LandingPage/landing-page.js'

import { StaticPageBuilder, StaticPageList, StaticPageDetail, StaticPageEdit } from '../StaticPage';

class App extends React.Component {
  state = {}
  onLogoutClicked = e => {
    e.preventDefault()
    this.props.logout()
  }

  currencyFormatDE(num) {
    if (!num) {
      return 0
    }
    return num
  }

  indentBasedOnLevel(content, level) {
    switch (level) {
      case 1:
        return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
      case 2:
        return (
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
        )
      case 5:
        return (
          <span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {content}
          </span>
        )
      default:
        return content
    }
  }

  setDistinctRow(content) {
    const DISTINCT_ROW = [
      'Aktivitas Operasional',
      'Aktivitas Investasi',
      'Aktivitas Pendanaan',
      '',
    ]
    if (DISTINCT_ROW.indexOf(content) > -1) {
      return true
    }
    return false
  }

  render() {
    return (
      <CookiesProvider>
        <BrowserRouter>
          <AuthProvider>
            <React.Fragment>
              <AuthConsumer>
                {values => {
                  this.props = values
                  console.log(values)
                  if (values.isAuth) {
                    return (
                      <FeatureMainMenuComponent
                        variant={{
                          bgColor: 'blue',
                          submenuBgColor: 'lightblue',
                          submenuHoverBgColor: 'lightblue',
                          submenuItemBgColor: 'blue',
                          submenuColor: 'white',
                          uppercase: 'no',
                          itemBgColor: 'darkblue',
                          color: 'white',
                        }}
                      />
                    )
                  } else {
                    return <HomepageComponent />
                  }
                }}
              </AuthConsumer>
              <Routes>
                {/* Program */}
                <Route
                  exact
                  path="/daftar-program"
                  element={withAuth(DaftarProgramComponent)}
                />
                <Route
                  exact
                  path="/halaman-tambah-program"
                  element={withAuth(HalamanTambahProgramComponent)}
                />
                <Route
                  exact
                  path="/halaman-detail-program"
                  element={withAuth(HalamanDetailProgramComponent)}
                />
                <Route
                  exact
                  path="/halaman-ubah-program"
                  element={withAuth(HalamanUbahProgramComponent)}
                />
                {/* Income */}
                <Route
                  path="/catatan-pemasukan"
                  element={withAuth(CatatanPemasukanComponent)}
                />
                <Route
                  exact
                  path="/halaman-tambah-pemasukan"
                  element={withAuth(HalamanTambahPemasukanComponent)}
                />
                <Route
                  exact
                  path="/halaman-ubah-pemasukan"
                  element={withAuth(HalamanUbahPemasukanComponent)}
                />
                <Route
                  exact
                  path="/halaman-detail-pemasukan"
                  element={withAuth(HalamanDetailPemasukanComponent)}
                />

                <Route exact path="/login" element={<LoginComponent />} />
                <Route path='/static-page'>
                  <Route path="create" element={<StaticPageBuilder />} />
                  <Route path="list" element={<StaticPageList />} />
                  <Route path="detail/:staticPageId" element={<StaticPageDetail />} />
                  <Route path="edit/:staticPageId" element={<StaticPageEdit />} />
                </Route>
                <Route exact path="/" element={<LandingPageComponent />} />
              </Routes>
            </React.Fragment>
          </AuthProvider>
        </BrowserRouter>
      </CookiesProvider>
    )
  }
}

export default App
