//Halaman Detail Pemasukan Containers
import React from 'react'
import { AuthConsumer } from '../Authentication'
import queryString from 'query-string'
import { withRouter } from 'react-router-dom'

import DetailIncomeComponent from '../DetailIncome/detail-income.js'

class HalamanDetailPemasukan extends React.Component {
  state = {}

  componentDidMount = async () => {
    this.setState({
      objectDetailIncome: JSON.parse(
        queryString.parse(this.props.location.search).objectDetailIncome
      ),
    })
  }

  render() {
    return (
      <React.Fragment>
        {this.state.objectDetailIncome && (
          <DetailIncomeComponent
            {...this.props}
            objectDetailIncome={this.state.objectDetailIncome}
          ></DetailIncomeComponent>
        )}
      </React.Fragment>
    )
  }
}

export default HalamanDetailPemasukan
