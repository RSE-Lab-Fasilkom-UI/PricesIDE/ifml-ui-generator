import React from 'react'
import InputField from '../components/InputField/InputField'
import SelectionField from '../components/SelectionField/SelectionField'
import Form from '../components/Form/Form'
import Button from '../components/Button/Button'
import queryString from 'query-string'
import CallprogramsaveService from '../services/call-program-save.service'

class TambahkanProgram extends React.Component {
  state = {}
  componentWillMount = () => {}

  Kirim = async () => {
    const data = await CallprogramsaveService.call({
      name: this.namaProgramInput.value,
      description: this.deskripsiInput.value,
      target: this.targetInput.value,
      partner: this.partnerInput.value,
      logoUrl: this.urlGambarProgramInput.value,
    })

    this.props.history.push({
      pathname: '/daftar-program',
      search: queryString.stringify({
        jsonAllProgram: JSON.stringify(
          data['data'] ? data['data']['data'] : []
        ),
      }),
    })
  }

  render() {
    return (
      <Form
        title="Tambahkan Program"
        id_name="tambahkan-program"
        variant={{
          shape: 'default',
          color: 'blue',
          borderColor: 'transparent',
          bgColor: 'white',
          alignment: 'center',
        }}
      >
        <InputField
          camel_name="namaProgramInput"
          type=""
          dasherized="input-nama-program"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Nama Program"
          placeholder="Fill the Nama Program"
          ref_func={e => {
            this.namaProgramInput = e
          }}
        />
        <InputField
          camel_name="deskripsiInput"
          type=""
          dasherized="input-deskripsi"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Deskripsi"
          placeholder="Fill the Deskripsi"
          ref_func={e => {
            this.deskripsiInput = e
          }}
        />
        <InputField
          camel_name="targetInput"
          type=""
          dasherized="input-target"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Target"
          placeholder="Fill the Target"
          ref_func={e => {
            this.targetInput = e
          }}
        />
        <InputField
          camel_name="partnerInput"
          type=""
          dasherized="input-partner"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Partner"
          placeholder="Fill the Partner"
          ref_func={e => {
            this.partnerInput = e
          }}
        />
        <InputField
          camel_name="urlGambarProgramInput"
          type=""
          dasherized="input-url-gambar-program"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Url Gambar Program"
          placeholder="Fill the Url Gambar Program"
          ref_func={e => {
            this.urlGambarProgramInput = e
          }}
        />
        <Button
          onClick={e => {
            e.preventDefault()
            this.Kirim()
          }}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Kirim"
        />
      </Form>
    )
  }
}

export default TambahkanProgram
