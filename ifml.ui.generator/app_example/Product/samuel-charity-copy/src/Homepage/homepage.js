import React from 'react'
import MenuItem from '../components/MenuItem/MenuItem'
import MenuLink from '../components/MenuLink/MenuLink'
import MenuChildren from '../components/MenuChildren/MenuChildren'
import FeatureArrow from '../components/FeatureArrow/FeatureArrow'
import RootMenu from '../components/RootMenu/RootMenu'

class Homepage extends React.Component {
  state = {}

  currencyFormatDE(num) {
    if (!num) {
      return 0
    }
    return num
  }

  indentBasedOnLevel(content, level) {
    switch (level) {
      case 1:
        return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
      case 2:
        return (
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
        )
      case 5:
        return (
          <span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {content}
          </span>
        )
      default:
        return content
    }
  }

  setDistinctRow(content) {
    const DISTINCT_ROW = [
      'Aktivitas Operasional',
      'Aktivitas Investasi',
      'Aktivitas Pendanaan',
      '',
    ]
    if (DISTINCT_ROW.indexOf(content) > -1) {
      return true
    }
    return false
  }

  render() {
    return (
      <React.Fragment>
        <RootMenu
          isAuth
          variant={{
            bgColor: 'blue',
            submenuBgColor: 'lightblue',
            submenuHoverBgColor: 'lightblue',
            submenuItemBgColor: 'blue',
            submenuColor: 'white',
            uppercase: 'no',
            itemBgColor: 'darkblue',
            color: 'white',
          }}
        >
          <MenuItem>
            <MenuLink to={'/'}>Home</MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to={'/static-page/create'}>Create Static Page</MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to={'/static-page/list'}>List Static Page</MenuLink>
          </MenuItem>
          <MenuItem>
            <MenuLink to={'/login'}>Login</MenuLink>
          </MenuItem>
        </RootMenu>
      </React.Fragment>
    )
  }
}

export default Homepage
