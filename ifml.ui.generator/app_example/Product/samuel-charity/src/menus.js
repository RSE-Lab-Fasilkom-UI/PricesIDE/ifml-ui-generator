import React from 'react'
import { Link } from 'react-router-dom'

import AuthConsumer from 'commons/auth'
import RootMenu from 'commons/components/RootMenu/RootMenu'
import MenuItem from 'commons/components/MenuItem/MenuItem'
import MenuLink from 'commons/components/MenuLink/MenuLink'

import { FeatureArrow, MenuChildren } from 'commons/components'

class FeatureMainMenu extends React.Component {
  state = {}

  currencyFormatDE(num) {
    if (!num) {
      return '0,00'
    }
    return num
      .toFixed(2)
      .replace('.', ',')
      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

  render() {
    const appVariant = this.props.variant
    return (
      <AuthConsumer>
        {values => {
          return (
            <RootMenu {...values} variant={appVariant}>
              <MenuItem variant={appVariant}>
                <MenuLink>
                  Info Organisasi
                  <FeatureArrow feat_type="root" />
                </MenuLink>
                <MenuChildren isFirstLevel>
                  <MenuItem>
                    <Link
                      to="/about">
                      Tentang Kami
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link
                      to="/contact">
                      Kontak
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link
                      to="/partners">
                      Mitra Kami
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link
                      to="/maps">
                      Lokasi
                    </Link>
                  </MenuItem>
                  <MenuItem>
                    <Link
                      to="/donation/accounts">
                      Informasi Rekening
                    </Link>
                  </MenuItem>
                </MenuChildren>
              </MenuItem>
              <MenuItem variant={appVariant}>
                <MenuLink to="/programs" variant={appVariant}>
                  Program
                </MenuLink>
              </MenuItem>
              <MenuItem variant={appVariant}>
                <MenuLink variant={appVariant} href="#" onClick="#">
                  Financial Report
                  <FeatureArrow feat_type="root" />
                </MenuLink>
                <MenuChildren isFirstLevel variant={appVariant}>
                  <MenuItem variant={appVariant}>
                    <MenuLink to="/income" variant={appVariant}>
                      Catatan Pemasukan
                    </MenuLink>
                  </MenuItem>
                </MenuChildren>
              </MenuItem>
            </RootMenu>
          )
        }}
      </AuthConsumer>
    )
  }
}

export default FeatureMainMenu
