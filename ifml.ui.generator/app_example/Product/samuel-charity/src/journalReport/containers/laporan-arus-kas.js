//Laporan Arus Kas Containers
import React, { useEffect, useState } from 'react'

import Table from 'commons/components/Table/Table'

import TableChartOfAccountEntry from '../components/TableChartOfAccountEntry.js'
import CallautomaticreporttwolevellistService from 'journalReport/services/call-automatic-report-twolevel-list.service.js'

const LaporanArusKas = props => {
  const [chartOfAccounts, setChartOfAccounts] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: coa } = await CallautomaticreporttwolevellistService.call()
      setChartOfAccounts(coa.data)
    }
    fetch()
  }, [])

  return (
    <div class="mx-auto max-w-screen-md px-6 py-12">
      <div class="prose max-w-full mb-6">
        <h2>Laporan Arus Kas</h2>
      </div>
      <Table id="table-TableChartOfAccountEntryComponent" compact>
        {chartOfAccounts && (
          <TableChartOfAccountEntry entries={chartOfAccounts} />
        )}
      </Table>
    </div>
  )
}

export default LaporanArusKas
