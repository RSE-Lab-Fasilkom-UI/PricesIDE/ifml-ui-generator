import { withAuth } from 'commons/auth'

import LaporanArusKas from './containers/laporan-arus-kas'

const path = '/report'

const reportRoutes = [
  {
    path: path + '/arus-kas',
    element: withAuth(LaporanArusKas),
  },
]

export default reportRoutes
