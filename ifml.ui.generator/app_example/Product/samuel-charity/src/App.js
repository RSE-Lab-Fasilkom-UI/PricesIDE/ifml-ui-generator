import React from 'react'
import { AuthProvider, AuthConsumer } from './commons/auth'
import { CookiesProvider } from 'react-cookie'
import FeatureMainMenuComponent from './menus.js'

import HomepageComponent from './commons/containers/homepage.js'
import GlobalRoutes from './routes'
import { BrowserRouter as Router } from 'react-router-dom'

import 'commons/styles/global.css'
import AppLayout from 'commons/components/AppLayout'

class App extends React.Component {
  state = {}
  onLogoutClicked = e => {
    e.preventDefault()
    this.props.logout()
  }

  currencyFormatDE(num) {
    if (!num) {
      return 0
    }
    return num
  }

  indentBasedOnLevel(content, level) {
    switch (level) {
      case 1:
        return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
      case 2:
        return (
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
        )
      case 5:
        return (
          <span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {content}
          </span>
        )
      default:
        return content
    }
  }

  setDistinctRow(content) {
    const DISTINCT_ROW = [
      'Aktivitas Operasional',
      'Aktivitas Investasi',
      'Aktivitas Pendanaan',
      '',
    ]
    if (DISTINCT_ROW.indexOf(content) > -1) {
      return true
    }
    return false
  }

  render() {
    return (
      <CookiesProvider>
        <AuthProvider>
          <Router>
            <AppLayout>
              <AuthConsumer>
                {values => {
                  if (values.isAuth) {
                    return (
                      <FeatureMainMenuComponent
                        {...values}
                        variant={{
                          bgColor: 'blue',
                          submenuBgColor: 'lightblue',
                          submenuHoverBgColor: 'lightblue',
                          submenuItemBgColor: 'blue',
                          submenuColor: 'white',
                          uppercase: 'no',
                          itemBgColor: 'darkblue',
                          color: 'white',
                        }}
                      />
                    )
                  } else {
                    return <HomepageComponent {...values} />
                  }
                }}
              </AuthConsumer>
              <div class="flex-1">
                <GlobalRoutes />
              </div>
            </AppLayout>
          </Router>
        </AuthProvider>
      </CookiesProvider>
    )
  }
}

export default App
