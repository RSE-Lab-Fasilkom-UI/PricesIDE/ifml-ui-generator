//Table
import React from 'react'
import Button from 'commons/components/Button/Button'
import TableRow from 'commons/components/Table/TableRow'
import TableCell from 'commons/components/Table/TableCell'
import { Link } from 'react-router-dom'

const TableIncomeContent = ({ incomeItem }) => {
  return (
    <TableRow distinct={false}>
      {/* //Data Binding Income List Element*/}

      <TableCell>{incomeItem.datestamp}</TableCell>

      <TableCell>{incomeItem.programName}</TableCell>

      <TableCell isCurrency>{incomeItem.amount}</TableCell>

      <TableCell>{incomeItem.description}</TableCell>

      <TableCell>{incomeItem.paymentMethod}</TableCell>
      <TableCell variant='{"borderColor": "blue", "bgColor": "white", "shape": "default", "alignment": "left"}'>
        <div class="flex gap-2">
          <Link to={`/income/${incomeItem.id}`}>
            <Button className="btn-sm btn-ghost" text="Detail" />
          </Link>
          <Link to={`/income/ubah?id=${incomeItem.id}`}>
            <Button className="btn-sm btn-primary btn-outline" text="Edit" />
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
}

export default TableIncomeContent
