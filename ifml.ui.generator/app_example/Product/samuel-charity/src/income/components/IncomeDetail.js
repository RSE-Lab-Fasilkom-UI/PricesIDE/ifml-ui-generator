import React from 'react'
import Button from 'commons/components/Button/Button'
import Detail from 'commons/components/Detail/Detail'
import VisualizationAttr from 'commons/components/VisualizationAttr/VisualizationAttr'
import CallincomedeleteService from '../services/call-income-delete.service'
import { useNavigate } from 'react-router-dom'

const IncomeDetail = ({ income }) => {
  const navigate = useNavigate()
  const Hapus = async () => {
    await CallincomedeleteService.call({
      id: income.id.toString(),
    })
    navigate('/income')
  }

  const Ubah = async () => {
    navigate(`/income/ubah?id=${income.id}`)
  }

  return (
    <Detail
      variant={{ shape: 'default', borderColor: 'blue', bgColor: 'white' }}
    >
      {/* //Data Binding Income Data*/}
      <VisualizationAttr
        title_name="Tanggal"
        content={income.datestamp}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Deskripsi"
        content={income.description}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Jumlah"
        currency_fmt
        content={income.amount}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Nama Program"
        content={income.programName}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Jenis Pemasukan"
        content={income.coaName}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Metode Pembayaran"
        content={income.paymentMethod}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <div class="w-full flex gap-4 mt-6">
        <div className="flex-1">
          <Button
            onClick={Hapus}
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
            text="Hapus"
          />
        </div>
        <div className="flex-1">
          <Button
            onClick={Ubah}
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
            text="Ubah"
          />
        </div>
      </div>
    </Detail>
  )
}

export default IncomeDetail
