//Halaman Tambah Pemasukan Containers
import React, { useEffect, useState } from 'react'

import CallprogramlistService from 'programs/services/call-program-list.service'
import CallchartofaccountlistService from 'journalReport/services/call-chart-of-account-list.service'
import TambahkanPemasukanComponent from '../components/FormTambahIncome.js'

const HalamanTambahPemasukan = props => {
  const [programs, setPrograms] = useState()
  const [chartOfAccounts, setChartOfAccounts] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: programList } = await CallprogramlistService.call()
      const { data: coa } = await CallchartofaccountlistService.call()
      setPrograms(programList.data)
      setChartOfAccounts(coa.data)
    }
    fetch()
  }, [])

  return (
    <>
      {programs && chartOfAccounts && (
        <TambahkanPemasukanComponent
          {...props}
          programs={programs}
          chartOfAccounts={chartOfAccounts}
        ></TambahkanPemasukanComponent>
      )}
    </>
  )
}

export default HalamanTambahPemasukan
