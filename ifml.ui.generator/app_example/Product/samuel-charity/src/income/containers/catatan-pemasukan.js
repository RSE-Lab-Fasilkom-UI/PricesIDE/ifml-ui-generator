//Catatan Pemasukan Containers
import React, { useEffect, useState } from 'react'
import { AuthConsumer } from '../../commons/auth'

import IncomeMenuFeatureComponent from '../components/IncomeFeature.js'

import Table from 'commons/components/Table/Table'
import TableRow from 'commons/components/Table/TableRow'
import TableCell from 'commons/components/Table/TableCell'
import TableBody from 'commons/components/Table/TableBody'
import TableHead from 'commons/components/Table/TableHead'

import TableIncomeContentComponent from '../components/IncomeTable.js'
import CallincomelistService from '../services/call-income-list.service'

const CatatanPemasukan = props => {
  const [jsonAllIncome, setjsonAllIncome] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: incomeList } = await CallincomelistService.call()
      setjsonAllIncome(incomeList.data)
    }
    fetch()
  }, [])

  return (
    <React.Fragment>
      <AuthConsumer>
        {values => {
          return <IncomeMenuFeatureComponent {...values} />
        }}
      </AuthConsumer>
      <div class="mx-auto max-w-screen-xl px-6 mb-12">
        <Table id="table-TableIncomeContentComponent">
          <TableHead>
            <TableRow>
              {/* //Data Binding Income List Element*/}
              <TableCell id="Tanggal">Tanggal</TableCell>
              <TableCell id="Program">Program</TableCell>
              <TableCell id="Jumlah">Jumlah</TableCell>
              <TableCell id="Deskripsi">Deskripsi</TableCell>
              <TableCell id="Metode Pembayaran">Metode Pembayaran</TableCell>
              <TableCell>
                <div class="text-center">Aksi</div>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {jsonAllIncome &&
              jsonAllIncome.map(incomeItem => (
                <TableIncomeContentComponent
                  {...props}
                  key={incomeItem.id}
                  incomeItem={incomeItem}
                />
              ))}
          </TableBody>
        </Table>
      </div>
    </React.Fragment>
  )
}

export default CatatanPemasukan
