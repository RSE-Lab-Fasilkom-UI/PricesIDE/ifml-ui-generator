//Halaman Detail Pemasukan Containers
import React, { useEffect, useState } from 'react'
import { AuthConsumer } from '../../commons/auth'
import queryString from 'query-string'

import DetailIncomeComponent from '../components/IncomeDetail.js'
import { useParams } from 'react-router-dom'
import CallincomedetailService from 'income/services/call-income-detail.service'

const HalamanDetailPemasukan = props => {
  const [income, setIncome] = useState()
  const { id } = useParams()

  useEffect(() => {
    const fetch = async () => {
      const { data: incomeDetail } = await CallincomedetailService.call({ id })
      setIncome(incomeDetail.data)
    }
    fetch()
  }, [])

  return (
    <>
      <pre>{JSON.stringify(income, 0, 2)}</pre>
      {income && <DetailIncomeComponent {...props} income={income} />}
    </>
  )
}

export default HalamanDetailPemasukan
