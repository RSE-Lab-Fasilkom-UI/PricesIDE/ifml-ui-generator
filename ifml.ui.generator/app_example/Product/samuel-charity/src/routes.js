import { useRoutes } from 'react-router-dom'

import programRoutes from './programs/routes'
import commonRoutes from 'commons/routes.js'
import incomeRoutes from 'income/routes.js'
import reportRoutes from 'journalReport/routes.js'
import staticPageRoutes from 'staticPage/routes'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...programRoutes,
    ...incomeRoutes,
    ...reportRoutes,
    ...staticPageRoutes,
    ...commonRoutes,
  ])

  return router
}

export default GlobalRoutes
