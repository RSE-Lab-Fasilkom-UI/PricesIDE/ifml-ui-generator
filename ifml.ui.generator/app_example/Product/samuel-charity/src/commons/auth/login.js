import React from 'react'
import { Navigate } from 'react-router-dom'
import _debounce from 'lodash/debounce'
import { AuthConsumer } from '../auth'
import Button from '../components/Button/Button'
import Detail from '../components/Detail/Detail'

class LoginPage extends React.Component {
  handleLoginWithGoogle = () => {
    this.props.loginGoogle()
  }

  render() {
    if (this.props.isAuth) {
      return <Navigate to="/" />
    }

    return (
      <div className="h-full bg-base-100 grid place-items-center">
        <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-white">
          <div className="card-body">
            <div className="form-control">
              <label className="label">
                <span className="label-text">Email</span>
              </label>
              <input
                type="text"
                placeholder="email"
                className="input input-bordered"
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Password</span>
              </label>
              <input
                type="text"
                placeholder="password"
                className="input input-bordered"
              />
            </div>
            <div className="form-control mt-4">
              <button className="btn btn-primary">Login</button>
            </div>
            <div className="divider">atau</div>
            <Button
              onClick={this.handleLoginWithGoogle}
              text="Login with Google"
              variant={{
                shape: 'default',
                color: 'white',
                bgColor: 'blue',
                borderColor: 'blue',
              }}
            />
          </div>
        </div>
      </div>
    )
  }
}

const withAuth = props => (
  <AuthConsumer>{values => <LoginPage {...props} {...values} />}</AuthConsumer>
)

export default withAuth
