import React from 'react'
import './FeatureArrow.css'
import { GoChevronDown } from 'react-icons/go'

const FeatureArrow = props => {
  return <GoChevronDown />
}

export default FeatureArrow
