import React from 'react'
import { ListItemStyle } from './style'

const ListItem = props => {
  const { onClick, variant } = props

  const onClickfunc = onClick ? onClick : undefined

  return (
    <li
      className="w-full rounded-xl shadow-xl p-6 bg-white flex flex-col"
      onClick={onClickfunc}
      {...variant}
    >
      {props.children}
    </li>
  )
}

export default ListItem
