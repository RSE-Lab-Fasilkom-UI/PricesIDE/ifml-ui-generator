import React from 'react'
import { Link } from 'react-router-dom'

const Hero = ({ data }) => {
  const { name, slogan, banner } = data

  return (
    <div
      className="hero min-h-screen"
      style={{ backgroundImage: `url("${banner}")` }}
    >
      <div className="hero-overlay bg-opacity-60"></div>
      <div className="hero-content text-center text-neutral-content">
        <div className="max-w-md">
          <h1 className="mb-5 text-5xl font-bold">{name}</h1>
          <p className="mb-5">{slogan}</p>
          <Link to="/login">
            <button className="btn btn-primary">Get Started</button>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default Hero
