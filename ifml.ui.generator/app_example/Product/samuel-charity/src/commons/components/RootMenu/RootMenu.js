import { AuthConsumer } from 'commons/auth'
import React from 'react'
import { Link } from 'react-router-dom'
import MenuLink from '../MenuLink/MenuLink'
import { FiLogOut } from 'react-icons/fi'

const RootMenu = props => {
  const { isAuth, logout, children, variant } = props

  return (
    <nav
      {...variant}
      className="sticky top-0 navbar justify-between w-full py-0 px-4 bg-primary text-primary-content z-10 shadow-xl"
    >
      <Link to={'/'} className="btn btn-ghost normal-case text-xl">
        SamuelCharity
      </Link>
      <ul class="menu menu-horizontal rounded-box p-2 hidden sm:flex">
        {children}
      </ul>
      {isAuth && (
        <MenuLink
          className="btn btn-ghost items-center gap-2 text-primary-content normal-case hidden sm:inline-flex"
          onClick={logout}
        >
          <FiLogOut className="w-5 h-5" />
          Keluar
        </MenuLink>
      )}
      <div className="flex-none sm:hidden">
        <label htmlFor="drawer-toggle" className="btn btn-square btn-ghost">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            className="inline-block w-6 h-6 stroke-current"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M4 6h16M4 12h16M4 18h16"
            ></path>
          </svg>
        </label>
      </div>
    </nav>
  )
}

export default RootMenu
