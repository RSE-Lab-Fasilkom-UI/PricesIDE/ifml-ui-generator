import React from 'react'

const Form = props => {
  const { title, id_name, variant } = props

  return (
    <form
      className="max-w-lg mx-auto bg-white prose p-6 rounded-xl shadow-xl my-12"
      id={id_name}
      {...props}
      {...variant}
    >
      <h2 className="h2">{title}</h2>
      <fieldset className="space-y-4" {...variant}>
        {props.children}
      </fieldset>
    </form>
  )
}

export default Form
