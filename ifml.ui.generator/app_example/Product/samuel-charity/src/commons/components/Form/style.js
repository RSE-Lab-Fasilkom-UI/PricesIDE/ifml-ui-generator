import styled from 'styled-components'

export const FormContainer = styled.form`
  margin-top: 5%;
`

export const FieldsetContainer = styled.fieldset`
  font-family: Helvetica, sans-serif;
  padding: 20px 30px;
  box-sizing: border-box;
  width: 70%;
  margin: auto 15% 30px 15%;
  position: relative;
  box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
  background-color: ${props => (props.bgColor ? props.bgColor : 'white')};
  align-items: stretch;

  border-radius: ${props => (props.shape === 'rounded' ? '64px' : '4px')};
  border: ${props =>
    props.borderColor
      ? `1px solid ${props.borderColor}`
      : props.color
      ? `1px solid ${props.color}`
      : '1px solid black'};

  h2 {
    text-align: ${props => (props.alignment ? props.alignment : 'center')};
    color: ${props => (props.color ? props.color : 'black')};
    font-size: 1.5em;
  }
`
