import React from 'react'
import { VisualizationDiv } from './style'

const VisualizationAttr = props => {
  const { title_name, content, variant, currency_fmt } = props

  const checkIsImage = url => url.match(/\.(jpeg|jpg|gif|png)$/) != null

  const format_currency = (currency, amount) => {
    switch (currency) {
      case 'CA':
        return new Intl.NumberFormat('en-CA', {
          currency: 'CAD',
          style: 'currency',
        }).format(amount)
      case 'JP':
        return new Intl.NumberFormat('jp-JP', {
          style: 'currency',
          currency: 'JPY',
        }).format(amount)
      case 'US':
        return new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'USD',
        }).format(amount)
      case 'ID':
        return new Intl.NumberFormat('id-ID', {
          style: 'currency',
          currency: 'IDR',
        }).format(amount)
      default:
        return new Intl.NumberFormat('id-ID', {
          style: 'currency',
          currency: 'IDR',
        }).format(amount)
    }
  }

  const isImage = typeof content == 'string' && checkIsImage(content)

  return (
    <VisualizationDiv {...variant}>
      {!isImage && (
        <label className="label">
          <span class="label-text text-sm uppercase font-medium text-gray-400">
            {title_name}
          </span>
        </label>
      )}
      {currency_fmt != null ? (
        <div className="visualization_content">
          {format_currency(currency_fmt, content)}
        </div>
      ) : (
        <div className="visualization_content">
          {isImage ? (
            <img
              src={content}
              alt={title_name}
              className="aspect-square object-cover object-center"
            />
          ) : (
            content
          )}
        </div>
      )}
    </VisualizationDiv>
  )
}

export default VisualizationAttr
