import React from 'react'

const AppLayout = ({ children }) => {
  return (
    <div className="drawer">
      <input
        id="drawer-toggle"
        type="checkbox"
        className="hidden drawer-toggle"
      />
      <div className="drawer-content min-h-screen flex flex-col">
        {children}
      </div>
      <div className="drawer-side">
        <label htmlFor="drawer-toggle" className="drawer-overlay"></label>
        <ul className="menu p-4 overflow-y-auto w-80 bg-base-100">
          {/* <!-- Sidebar content here --> */}
          <li>
            <a>Sidebar Item 1</a>
          </li>
          <li>
            <a>Sidebar Item 2</a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default AppLayout
