import React from 'react'
import { Link } from 'react-router-dom'

const MenuLink = props => {
  const { href, onClick, to, className } = props

  if (to) {
    return (
      <Link to={to}>
        <span className={className}>{props.children}</span>
      </Link>
    )
  }

  return (
    <a
      className={className + ' flex justify-between'}
      href={href}
      onClick={onClick}
    >
      {props.children}
    </a>
  )
}

export default MenuLink
