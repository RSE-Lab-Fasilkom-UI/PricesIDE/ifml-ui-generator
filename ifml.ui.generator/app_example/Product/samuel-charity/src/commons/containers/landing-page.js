import { Footer, Hero } from 'commons/components'
import React from 'react'

const STATIC = {
  name: 'Samuel Charity',
  about:
    'Samuel Charity adalah organisasi sosial yang bergerak di bidang pendidikan',
  slogan: 'Bersama Samuel Charity, Indonesia Cerdas',
  address: 'Jalan Depok, Jakarta Timur, DKI Jakarta',
  map: 'https://goo.gl/maps/nzWYxW9PVuBo6g5d7',
  email: 'samuelcharity@gmail.com',
  phone: '(021) 5555 666',
  banner: 'https://wallpaperaccess.com/full/881024.jpg',
  description:
    'organisasi ini berdiri pada tahun 2018 yang kini sudah memiliki 8 sekolah yang berada di daerah 3T',
  fb_url: 'https://id-id.facebook.com/',
  map_url: 'https://goo.gl/maps/nzWYxW9PVuBo6g5d7',
  instagram_url: 'https://www.instagram.com/',
}

const LandingPage = () => {
  return (
    <div className="landing-page">
      <Hero data={STATIC} />
      <Footer data={STATIC} />
      {/* <About>
        <div className={style.box}>
          <h1 className={`${style.heading_main} ${style.None}`}>About Us</h1>
          <h3 className={`${style.heading_categories} ${style.None}`}>
            {this.state?.about}
          </h3>
          <p className={`${style.writing_content} ${style.None}`}>
            {this.state?.description}
          </p>
        </div>
      </About>
      <Footer>
        <div className="contact">
          <div className={style.box}>
            <h1 className={`${style.heading_main} ${style.None}`}>
              Contact Us
            </h1>

            <h3 className={`${style.heading_categories} ${style.None}`}>
              Address
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state?.address}
            </p>

            <h3 className={`${style.heading_categories} ${style.None}`}>
              Email
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state?.email}
            </p>

            <h3 className={`${style.heading_categories} ${style.None}`}>
              Phone
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state?.phone}
            </p>
          </div>
        </div>
        <div className="sitemap">
          <div className={style.box}>
            <h4 className={`${style.heading_categories} ${style.None}`}>
              Sitemap
            </h4>
            <p className={`${style.writing_content} ${style.None}`}>
              <a href={this.state?.fb_url} className={style.links}>
                Facebook page
              </a>
            </p>
            <p className={`${style.writing_content} ${style.None}`}>
              <a href={this.state?.map_url} className={style.links}>
                Location on map
              </a>
            </p>
            <p className={`${style.writing_content} ${style.None}`}>
              <a href={this.state?.instagram_url} className={style.links}>
                Instagram page
              </a>
            </p>
          </div>
        </div>
      </Footer> */}
    </div>
  )
}

export default LandingPage
