import React from 'react'
import { Link } from 'react-router-dom'

import { FeatureArrow, MenuChildren } from 'commons/components'
import MenuItem from '../components/MenuItem/MenuItem'
import MenuLink from '../components/MenuLink/MenuLink'
import RootMenu from '../components/RootMenu/RootMenu'

class Homepage extends React.Component {
  state = {}

  currencyFormatDE(num) {
    if (!num) {
      return 0
    }
    return num
  }

  indentBasedOnLevel(content, level) {
    switch (level) {
      case 1:
        return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
      case 2:
        return (
          <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
        )
      case 5:
        return (
          <span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {content}
          </span>
        )
      default:
        return content
    }
  }

  setDistinctRow(content) {
    const DISTINCT_ROW = [
      'Aktivitas Operasional',
      'Aktivitas Investasi',
      'Aktivitas Pendanaan',
      '',
    ]
    if (DISTINCT_ROW.indexOf(content) > -1) {
      return true
    }
    return false
  }

  render() {
    return (
      <RootMenu {...this.props}>
        <MenuItem>
          <MenuLink>
            Info Organisasi
            <FeatureArrow feat_type="root" />
          </MenuLink>
          <MenuChildren isFirstLevel>
            <MenuItem>
              <Link
                to="/about">
                Tentang Kami
              </Link>
            </MenuItem>
            <MenuItem>
              <Link
                to="/contact">
                Kontak
              </Link>
            </MenuItem>
            <MenuItem>
              <Link
                to="/partners">
                Mitra Kami
              </Link>
            </MenuItem>
            <MenuItem>
              <Link
                to="/maps">
                Lokasi
              </Link>
            </MenuItem>
            <MenuItem>
              <Link
                to="/donation/accounts">
                Informasi Rekening
              </Link>
            </MenuItem>
          </MenuChildren>
        </MenuItem>

        <MenuItem>
          <Link
            to={'/login'}
            className="btn btn-primary bg-base-100 text-base-content hover:text-base-primary"
          >
            Login
          </Link>
        </MenuItem>
      </RootMenu>
    )
  }
}

export default Homepage
