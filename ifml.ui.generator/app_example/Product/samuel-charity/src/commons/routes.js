import LandingPage from './containers/landing-page'
import LoginPage from './auth/login'

const commonRoutes = [
  { path: '/login', element: <LoginPage /> },
  { path: '/', element: <LandingPage /> },
]

export default commonRoutes
