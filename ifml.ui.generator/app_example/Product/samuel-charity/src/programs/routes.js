import { withAuth } from '../commons/auth'

//Program
import DaftarProgramComponent from './containers/daftar-program.js'
import HalamanTambahProgramComponent from './containers/halaman-tambah-program.js'
import HalamanDetailProgramComponent from './containers/halaman-detail-program.js'
import HalamanUbahProgramComponent from './containers/halaman-ubah-program.js'

const path = '/programs'

const programRoutes = [
  { path: path + '/tambah', element: withAuth(HalamanTambahProgramComponent) },
  { path: path + '/ubah', element: withAuth(HalamanUbahProgramComponent) },
  { path: path + '/:id', element: withAuth(HalamanDetailProgramComponent) },
  { path: path, element: withAuth(DaftarProgramComponent) },
]

export default programRoutes
