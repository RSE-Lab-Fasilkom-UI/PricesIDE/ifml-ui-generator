import axios from 'axios'
import Token from 'commons/utils/token'
import environment from 'commons/utils/environment'

class CallprogramdeleteService {
  static call = async (params = {}) => {
    const token = new Token().get()
    params = Object.assign(params, {
      token,
    })

    const encodedData = `token=${token}`

    try {
      const response = await axios.delete(
        `${environment.rootApi}/call/program/delete?${encodedData}`,
        { data: params }
      )

      return response
    } catch (e) {
      return {}
    }
  }
}

export default CallprogramdeleteService
