import axios from 'axios'
import Token from 'commons/utils/token'
import environment from 'commons/utils/environment'

class CallprogramdetailService {
  static call = async (params = {}) => {
    console.log(
      '🚀 ~ file: call-program-detail.service.js ~ line 7 ~ CallprogramdetailService ~ call= ~ params',
      params
    )
    const token = new Token().get()
    params = Object.assign(params, {
      token,
    })

    const encodedData = Object.keys(params)
      .map(thekey => `${thekey}=${encodeURI(params[thekey])}`)
      .join('&')

    try {
      const response = await axios.get(
        `${environment.rootApi}/call/program/detail?${encodedData}`
      )

      return response
    } catch (e) {
      return {}
    }
  }
}

export default CallprogramdetailService
