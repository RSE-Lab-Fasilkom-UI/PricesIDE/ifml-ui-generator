//List
import React from 'react'
import Button from 'commons/components/Button/Button'
import ListItem from 'commons/components/ListItem/ListItem'
import VisualizationAttr from 'commons/components/VisualizationAttr/VisualizationAttr'
import { Link } from 'react-router-dom'

const ProgramCard = ({ program }) => {
  return (
    <ListItem
      variant={{
        borderColor: 'blue',
        bgColor: 'white',
        shape: 'default',
        alignment: 'left',
      }}
    >
      {/* //Data Binding Program List Element*/}
      <div class="flex-1">
        <VisualizationAttr
          title_name="Gambar"
          content={program?.logoUrl}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <VisualizationAttr
          title_name="Nama"
          content={program?.name}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
      </div>
      <div class="mt-4">
        <Link to={`/programs/${program.id}`}>
          <Button className="btn-primary btn-outline" text="Detail Program" />
        </Link>
      </div>
    </ListItem>
  )
}

export default ProgramCard
