import React from 'react'
import Button from 'commons/components/Button/Button'
import Detail from 'commons/components/Detail/Detail'
import VisualizationAttr from 'commons/components/VisualizationAttr/VisualizationAttr'
import CallprogramdeleteService from '../services/call-program-delete.service'
import { useNavigate, useParams } from 'react-router-dom'

const DetailProgram = ({ program }) => {
  const navigate = useNavigate()
  const Hapus = async () => {
    await CallprogramdeleteService.call({ id: program.id.toString() })
    navigate('/programs')
  }

  const Ubah = async () => {
    navigate(`/programs/ubah?id=${program.id}`)
  }

  return (
    <Detail
      variant={{ shape: 'default', borderColor: 'blue', bgColor: 'white' }}
    >
      {/* //Data Binding Program Data*/}
      <div class="grid grid-cols-2 gap-6">
        <VisualizationAttr
          title_name="URL Gambar Program"
          content={program?.logoUrl}
          variant={{
            shape: 'default',
            color: 'black',
            titleColor: 'blue',
            uppercase: 'no',
          }}
        />
        <div className="flex flex-col">
          <div class="flex-1 space-y-2">
            <VisualizationAttr
              title_name="Nama"
              content={program?.name}
              variant={{
                shape: 'default',
                color: 'black',
                titleColor: 'blue',
                uppercase: 'no',
              }}
            />
            <VisualizationAttr
              title_name="Deskripsi"
              content={program?.description}
              variant={{
                shape: 'default',
                color: 'black',
                titleColor: 'blue',
                uppercase: 'no',
              }}
            />
            <VisualizationAttr
              title_name="Target"
              content={program?.target}
              variant={{
                shape: 'default',
                color: 'black',
                titleColor: 'blue',
                uppercase: 'no',
              }}
            />
            <VisualizationAttr
              title_name="Partner"
              content={program?.partner}
              variant={{
                shape: 'default',
                color: 'black',
                titleColor: 'blue',
                uppercase: 'no',
              }}
            />
          </div>

          <div class="w-full flex gap-4">
            <div className="flex-1">
              <Button
                onClick={Hapus}
                className="btn-error btn-outline"
                text="Hapus"
              />
            </div>
            <div className="flex-1">
              <Button
                onClick={Ubah}
                variant={{
                  shape: 'default',
                  color: 'white',
                  bgColor: 'blue',
                  borderColor: 'blue',
                }}
                text="Ubah"
              />
            </div>
          </div>
        </div>
      </div>
    </Detail>
  )
}

export default DetailProgram
