//Daftar Program Containers
import React, { useEffect, useState } from 'react'
import { AuthConsumer } from 'commons/auth'

import ProgramFeatureComponent from '../components/ProgramFeature.js'

import List from 'commons/components/List/List'

import ProgramCard from '../components/ProgramCard.js'
import CallprogramlistService from '../services/call-program-list.service'

const DaftarProgram = props => {
  const [programs, setPrograms] = useState([])

  useEffect(() => {
    const fetchPrograms = async () => {
      const { data: programList } = await CallprogramlistService.call()
      setPrograms(programList.data)
    }
    fetchPrograms()
  }, [])

  return (
    <div className="bg-base-100">
      <ProgramFeatureComponent programs={programs} />
      <List
        variant={{
          borderColor: 'blue',
          bgColor: 'white',
          shape: 'default',
          alignment: 'left',
        }}
        id_name="list-ListProgramContentComponent"
        className="list-component view-component"
        column="4"
      >
        {programs &&
          programs.map(program => (
            <ProgramCard key={program.id} {...props} program={program} />
          ))}
      </List>
    </div>
  )
}

export default DaftarProgram
