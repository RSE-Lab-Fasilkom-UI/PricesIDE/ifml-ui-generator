import React from 'react';
import { Button, Form, InputField } from 'commons/components';

const StaticPageSaveForm = ({ onSubmit, onCancel }) => {
  let inputTitle = '';

  const handleSubmitData = e => {
    console.log(
      '🚀 ~ file: StaticPageSaveForm.jsx ~ line 6 ~ StaticPageSaveForm ~ inputTitle',
      inputTitle,
    );
    e.preventDefault();
    if (inputTitle?.value !== '') {
      onSubmit(inputTitle.value);
    } else {
      alert('Title cannot be empty');
    }
  };

  return (
    <div style={{ backgroundColor: 'rgb(0, 0, 0, 0.6)' }}>
      <Form
        title="Tambahkan Pemasukan"
        id_name="tambahkan-pemasukan"
        variant={{
          shape: 'default',
          color: 'blue',
          borderColor: 'transparent',
          bgColor: 'white',
          alignment: 'center',
        }}
        onSubmit={handleSubmitData}
      >
        <InputField
          camel_name="title"
          type=""
          dasherized="input-title"
          variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
          label="Title"
          ref={e => {
            inputTitle = e;
          }}
        />
        <Button
          onClick={handleSubmitData}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Save"
        />
        <Button
          onClick={onCancel}
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Cancel"
        />
      </Form>
    </div>
  );
};

export default StaticPageSaveForm;
