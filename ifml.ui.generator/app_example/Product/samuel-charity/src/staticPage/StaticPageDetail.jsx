import axios from 'axios';
import { useState, useEffect } from 'react';
import ReactHTMLParser from 'html-react-parser';
import { useNavigate, useParams } from 'react-router-dom';
import { FiEdit } from "react-icons/fi";

import { Button } from 'commons/components';

const StaticPageDetail = ({ slug = '' }) => {
  const { staticPageId } = useParams();
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [htmlData, setHtmlData] = useState();

  const handleEditStaticPage = () => {
    navigate(`/static-page/edit/${slug || staticPageId}`);
  };

  // References: https://stackoverflow.com/a/60533623
  useEffect(() => {
    const head = document.head;
    const style = document.createElement('style');
    const setStyleElement = (cssData = '') => {
      style.innerHTML = cssData;

      head.appendChild(style);
    };

    const fetchData = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3003/static-data/${slug || staticPageId}`,
        );
        setStyleElement(response.data.cssData);
        setHtmlData(response.data.htmlData);
        setIsLoading(false);
        setIsError(false);
        setErrorMessage('');
      } catch (e) {
        setIsLoading(false);
        setIsError(true);
        setErrorMessage(
          e?.response?.statusText || 'Failed, please try again later!',
        );
      }
    };

    fetchData();

    return () => {
      if (head.contains(style)) head.removeChild(style);
    };
  });

  if (isLoading) return <p>Loading....</p>;

  if (isError) return <p>Error - {errorMessage}</p>;

  return (
    <>
      <div className="fixed bottom-6 right-6">
        <Button
          text={
            <span className="flex items-center gap-2">
              <FiEdit className="w-5 h-5" /> Edit
            </span>
          }
          className="btn-primary shadow-md"
          onClick={handleEditStaticPage}
        />
      </div>
      {htmlData && ReactHTMLParser(htmlData)}
    </>
  );
};

export default StaticPageDetail;
