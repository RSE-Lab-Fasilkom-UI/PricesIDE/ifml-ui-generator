import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import saveUser from '../services/saveUser'

const FormTambahkanUser = ({ roles }) => {
  const { control, handleSubmit } = useForm()


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await saveUser({
      ...cleanData,
      
    })
		navigate(`/user`)
  }

  return (
    <Form 
	  title="Tambahkan User" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Name"
            placeholder="Masukkan name"
            {...field}
          />
        )}
      />
	  <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            label="Email"
            placeholder="Masukkan email"
            {...field}
          />
        )}
      />
	  <Controller
        name="allowedPermissions"
        control={control}
        render={({ field }) => (
          <InputField
            label="Allowed Permissions"
            placeholder="Masukkan allowed permissions"
            {...field}
          />
        )}
      />
	  <Controller
        name="password"
        control={control}
        render={({ field }) => (
          <InputField
            label="Password"
            placeholder="Masukkan password"
			type="password"
            {...field}
          />
        )}
      />
	  <Controller
        name="roleIds"
        control={control}
        render={({ field }) => (
		<MultiSelectionField
            label="Roles"
            options={roles}
            placeholder="Masukkan roles"
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormTambahkanUser
