import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import updateUser from '../services/updateUser'

const FormUbahUser = ({ user }) => {
  const { control, handleSubmit } = useForm({ defaultValues: user })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await updateUser({
      ...cleanData,
      
    })
		navigate(`/user/${user.id}`)
  }

  return (
    <Form 
	  title="Ubah User" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id User"
            placeholder="Masukkan id user"
			type="number"
			disabled            defaultValue={user.id}            {...field}
          />
        )}
      />
	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Name"
            placeholder="Masukkan name"
            defaultValue={user.name}            {...field}
          />
        )}
      />
	  <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            label="Email"
            placeholder="Masukkan email"
            defaultValue={user.email}            {...field}
          />
        )}
      />
	  <Controller
        name="allowedPermissions"
        control={control}
        render={({ field }) => (
          <InputField
            label="Allowed Permissions"
            placeholder="Masukkan allowed permissions"
            defaultValue={user.allowedPermissions}            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormUbahUser
