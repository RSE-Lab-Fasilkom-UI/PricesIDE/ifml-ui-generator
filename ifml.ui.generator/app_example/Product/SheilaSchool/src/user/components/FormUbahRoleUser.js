import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import changeroleUser from '../services/changeroleUser'

const FormUbahRoleUser = ({ user, roles }) => {
  const { control, handleSubmit } = useForm({ defaultValues: user })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await changeroleUser({
      ...cleanData,
      
    })
		navigate(`/user/${user.id}`)
  }

  return (
    <Form 
	  title="Ubah Role User" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id User"
            placeholder="Masukkan id user"
			type="number"
			disabled            defaultValue={user.id}            {...field}
          />
        )}
      />
	  <Controller
        name="roleIds"
        control={control}
        render={({ field }) => (
		<MultiSelectionField
            label="Roles"
            options={roles}
            placeholder="Masukkan roles"
			defaultValue={user.roleIds}
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">kirim</Button>
		</div>
    </Form>
  )
}

export default FormUbahRoleUser
