import React from 'react';
import { Link } from 'react-router-dom';

import { Button, TableRow, TableCell } from 'commons/components';

const UserTable = ({ userItem }) => {
  return (
    <TableRow distinct={false}>
      {/* Data Binding User Table Element*/}
      <TableCell
		
		>{userItem?.name}</TableCell>
      <TableCell
		
		>{userItem?.email}</TableCell>
      <TableCell
		
		>{userItem?.allowedPermissions}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          {/* View Element Event User Table Element*/}
          <Link to={`/user/${userItem.id}`}>
            <Button variant="tertiary" className="btn-sm">Detail</Button>
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
};

export default UserTable;
