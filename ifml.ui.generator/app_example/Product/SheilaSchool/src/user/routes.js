import RequireAuth from 'commons/auth/RequireAuth'

import DaftarUserPage from './containers/DaftarUserPage'
import TambahUserPage from './containers/TambahUserPage'
import DetailUserPage from './containers/DetailUserPage'
import UbahUserPage from './containers/UbahUserPage'
import UbahRoleUserPage from './containers/UbahRoleUserPage'

const userRoutes = [
	{ 
		path: "/user/tambah",
		element: <RequireAuth permissionNeeded="administrator"><TambahUserPage/></RequireAuth>
	},
	{ 
		path: "/user/ubah",
		element: <RequireAuth permissionNeeded="administrator"><UbahUserPage/></RequireAuth>
	},
	{ 
		path: "/user/change-role",
		element: <RequireAuth permissionNeeded="administrator"><UbahRoleUserPage/></RequireAuth>
	},
	{ 
		path: "/user/:id",
		element: <RequireAuth permissionNeeded="administrator"><DetailUserPage/></RequireAuth>
	},
	{ 
		path: "/user",
		element: <RequireAuth permissionNeeded="administrator"><DaftarUserPage/></RequireAuth>
	}
]

export default userRoutes
