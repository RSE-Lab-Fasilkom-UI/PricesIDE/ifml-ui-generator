import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'




const changeroleUser = (data = {}) => {
	let body = data;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.put(`${environment.rootApi}/call/user/changerole`, body,
	{
		params: { token }, 
		
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default changeroleUser
