import { Button, InputField } from "commons/components";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import AuthforgotpasswordtokenService from './services/auth-forgot-password-token.service'
import AuthforgotpasswordService from './services/auth-forgot-password.service'

const ForgotPasswordPage = () => {
  const { control: emailControl, handleSubmit: handleSubmitEmail } = useForm();
  const { control: newPasswordControl, handleSubmit: handleSubmitNewPassword } = useForm();
  const navigate = useNavigate();

  const [forgotPasswordToken, setForgotPasswordToken] = useState();

  const requestForgotPasswordToken = async (data) => {
    const response = await AuthforgotpasswordtokenService.call(data);
    if (response['data']){
      setForgotPasswordToken(response['data']['data']['forgotPasswordToken']);
    }
  };

  const changePassword = async (data) => {
    data = {...data,forgotPasswordToken}
    const response = await AuthforgotpasswordService.call(data);
    if (response['data']){
      alert("Berhasil mengganti password. Silakan login kembali.");
      navigate(`/login`)	
    }
  };

  return (
    <div className="h-full bg-base-200 grid place-items-center">
      <div className="prose w-full max-w-md">
        <h1>Lupa Password</h1>
        <div className="card flex-shrink-0 w-full shadow-2xl bg-white">
          {!forgotPasswordToken ? (
            // Enter Email
            <form
              key="enter-email"
              onSubmit={handleSubmitEmail(requestForgotPasswordToken)}
              className="card-body not-prose"
            >
              <Controller
                name="email"
                control={emailControl}
                render={({ field }) => (
                  <InputField type="email" label="Email" placeholder="Masukkan email" {...field} />
                )}
              />
              <p className="text-sm text-neutral/60">Masukkan email akun Anda untuk mengganti password.</p>
              <Button type="submit">Submit</Button>
            </form>
          ) : (
            // Enter new password
            <form
              key="enter-password"
              onSubmit={handleSubmitNewPassword(changePassword)}
              className="card-body not-prose"
            >
              <Controller
                name="password"
                control={newPasswordControl}
                render={({ field }) => (
                  <InputField
                    type="password"
                    label="Password"
                    placeholder="Masukkan password baru"
                    {...field}
                  />
                )}
              />
              <p className="text-sm text-neutral/60">Masukkan password baru</p>
              <Button type="submit">Ganti Password</Button>
            </form>
          )}
        </div>
      </div>
    </div>
  );
};

export default ForgotPasswordPage;
