import React, {useState} from 'react'
import { useNavigate, useLocation } from 'react-router-dom'
import { AuthConsumer } from './indexDeprecated'
import { Button, InputField } from 'commons/components'
import AuthforgotpasswordService from './services/auth-forgot-password.service'

const ChangePasswordPage = () => {
  const [passwordInput, setPasswordInput] = useState();
  const navigate = useNavigate()
  const location = useLocation();
  // get userId
  let forgotPasswordToken = location.state.forgotPasswordToken;

  const kirim = async () => {
		const data = await AuthforgotpasswordService.call({
			password: passwordInput.value,
      forgotPasswordToken
		});

    if (data['data']){
      navigate(`/login`)	
    }
	}

  return (
    <div className="h-full bg-base-200 grid place-items-center">
      <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-white">
        <p className="m-2">Ubah Password</p>
        <div className="card-body">
          <InputField
            name="password"
            type="password"
            label="Password"
            placeholder="Masukkan password"
            ref={e => setPasswordInput(e)}
          />
          <Button onClick={kirim} variant="primary" className="form-control mt-4">
            Kirim
          </Button>
        </div>
      </div>
    </div>
  )
  }

const withAuth = props => (
  <AuthConsumer>{values => <ChangePasswordPage {...props} {...values} />}</AuthConsumer>
)

export default withAuth
