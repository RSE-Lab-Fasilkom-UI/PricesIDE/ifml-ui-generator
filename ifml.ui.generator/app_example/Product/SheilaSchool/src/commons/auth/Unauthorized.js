import React from "react";
import { Button } from "commons/components";


const Unauthorized = () => {
  return (
    
      <div className="h-full bg-base-200 grid">
        <div className="card-body">
      <Button variant="primary">
          Mohon maaf, Anda tidak memiliki akses untuk mengunjungi halaman ini.
      </Button>
      </div>
      </div>
  );
}

export default Unauthorized;
