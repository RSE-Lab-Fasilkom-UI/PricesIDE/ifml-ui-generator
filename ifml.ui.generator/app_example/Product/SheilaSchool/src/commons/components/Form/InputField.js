import useAppearance from 'commons/appearance/useAppearance'
import React, { forwardRef } from 'react'
import { INPUT_CLASSNAMES } from './variants'

const InputField = forwardRef(function InputField(props, ref) {
  const { label, className, kit } = props
  const interfaceKit = useAppearance()
  const inputStyle = (kit ?? interfaceKit).input
  const inputVariant = INPUT_CLASSNAMES[inputStyle]

  return (
    <div className="form-control">
      {label && <label className="label label-text">{label}</label>}
      <input
        className={`input ${inputVariant} ${className}`}
        ref={ref}
        {...props}
      />
    </div>
  )
})

export default InputField
