import React, { forwardRef, useEffect, useState } from "react";
import { INPUT_CLASSNAMES } from "./variants";
import useAppearance from "commons/appearance/useAppearance";
import CheckBoxField from "./CheckBoxField";

const MultiSelectionField = forwardRef((props, ref) => {
  const { label, variant, options, kit, defaultValue } = props;
  let propsChild = { ...props };
  propsChild["label"] = "";
  delete propsChild.options;
  const interfaceKit = useAppearance();
  const [value, setValue] = useState();
  const inputStyle = (kit ?? interfaceKit).input;

  useEffect(() => {
    let tempDefaultValue = [];

    if (!defaultValue) {
      tempDefaultValue = options.map((item) => {
        return false;
      });
    } else {
      tempDefaultValue = defaultValue.split(",");
      let tempAllChecked = options.map((item) => {
        for (let i = 0; i < tempDefaultValue.length; i++) {
          if (item.id === parseInt(tempDefaultValue[i])) {
            return true;
          }
        }
        return false;
      });
      tempDefaultValue = tempAllChecked;
    }
    setValue(tempDefaultValue);
  }, []);

  useEffect(() => {
    if (value) {
      let valueMultiSelectionField = [];
      for (let i = 0; i < options.length; i++) {
        if (value[i]) {
          valueMultiSelectionField.push(options[i].id);
        }
      }
      valueMultiSelectionField = valueMultiSelectionField.join(",");
      props.onChange(valueMultiSelectionField);
    }
  }, [value]);

  const handleChange = (index, updatedCheck) => {
    let valueTemp = [...value];
    valueTemp[index] = updatedCheck;
    setValue(valueTemp);
  };

  return (
    <div className="form-control" {...variant}>
      {label && <label className="label label-text">{label}</label>}

      {options &&
        value !== undefined &&
        options.map((option, index) => (
          <div key={option.id} className="form-control">
            <label className="label cursor-pointer">
              <div className="label-text not-prose">
                <span className="uppercase font-bold">{option.name}</span>
              </div>
              <CheckBoxField
                ref={ref}
                {...propsChild}
                {...variant}
                checked={value[index]}
                value={option.id}
                name={label.replace(" ", "")}
                onChange={() => handleChange(index, !value[index])}
              />
            </label>
          </div>
        ))}
    </div>
  );
});

export default MultiSelectionField;
