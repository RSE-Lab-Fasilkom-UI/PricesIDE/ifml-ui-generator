import React, { forwardRef } from 'react'
import { INPUT_CLASSNAMES } from './variants'
import useAppearance from 'commons/appearance/useAppearance'

const SelectionField = forwardRef((props, ref) => {
  const { label, variant, options, placeholder, className, kit } = props
  const interfaceKit = useAppearance()
  const inputStyle = (kit ?? interfaceKit).input
  const inputVariant = INPUT_CLASSNAMES[inputStyle]

  return (
    <div className="form-control" {...variant}>
      {label && <label className="label label-text">{label}</label>}
      <select
        className={`select ${inputVariant} ${className}`}
        ref={ref}
        {...props}
        {...variant}
      >
        <option disabled selected hidden>
          {placeholder}
        </option>
        {options.map(option => (
          <option value={option.id}>{option.name}</option>
        ))}
      </select>
    </div>
  )
})

export default SelectionField
