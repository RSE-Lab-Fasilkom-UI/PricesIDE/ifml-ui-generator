import React from 'react'

const TableRow = ({ onClick, children }) => {
  return (
    <tr onClick={onClick} highlight>
      {children}
    </tr>
  )
}

export default TableRow
