import React from 'react'

const TableCell = ({ isCurrency, isHeading, children, className }) => {
  const format_currency = children => {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(children)
  }

  return isHeading ? (
    <th className={className}>
      {isCurrency ? format_currency(children) : children}
    </th>
  ) : (
    <td
      className={className}
      style={{ textAlign: isCurrency ? 'right' : 'inherit' }}
    >
      {isCurrency ? format_currency(children) : children}
    </td>
  )
}

export default TableCell
