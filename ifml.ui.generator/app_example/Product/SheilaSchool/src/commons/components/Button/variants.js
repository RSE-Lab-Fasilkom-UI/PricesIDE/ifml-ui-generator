export const BUTTON_CLASSNAMES = {
  'solid-neutral': '',
  'solid-primary': 'btn-primary',
  'solid-secondary': 'btn-secondary',
  'solid-light':
    'border-none bg-neutral/10 text-neutral hover:bg-neutral/20 hover:text-neutral',
  'outline-neutral': 'btn-outline',
  'outline-primary': 'btn-outline btn-primary',
  'outline-secondary': 'btn-outline btn-secondary',
  'outline-light':
    'btn-outline border-neutral/20 hover:bg-neutral/10 hover:border-transparent hover:text-neutral',
  text: 'btn-ghost',
  'text-link': 'btn-link',
}
