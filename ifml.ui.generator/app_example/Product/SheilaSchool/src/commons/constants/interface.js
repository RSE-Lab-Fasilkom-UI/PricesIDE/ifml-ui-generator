export const INTERFACE_KITS = {
  orion: {
    button: {
      primary: 'solid-primary',
      secondary: 'solid-grey',
      tertiary: 'text',
    },
    input: 'outline',
    table: 'zebra',
    typography: 'archivo',
    rounded: true,
  },
  canopus: {
    button: {
      primary: 'solid-primary',
      secondary: 'outline-primary',
      tertiary: 'solid-light',
    },
    input: 'outline',
    table: 'zebra',
    typography: 'poppins',
    rounded: true,
  },
  capella: {
    button: {
      primary: 'solid-primary',
      secondary: 'outline-secondary',
      tertiary: 'text',
    },
    input: 'outline',
    table: 'outline',
    typography: 'roboto',
    rounded: true,
  },
  vega: {
    button: {
      primary: 'solid-primary',
      secondary: 'outline-grey',
      tertiary: 'text',
    },
    input: 'outline',
    table: 'outline',
    typography: 'source',
    rounded: false,
  },
  hydra: {
    button: {
      primary: 'solid-primary',
      secondary: 'outline-grey',
      tertiary: 'text',
    },
    input: 'solid',
    table: 'outline',
    typography: 'lato',
    rounded: false,
  },
}
export const INTERFACE_KIT = INTERFACE_KITS['canopus']
export const COLOR_THEMES = ['light', 'autumn', 'emerald', 'winter']
