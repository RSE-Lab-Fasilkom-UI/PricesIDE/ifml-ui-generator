import LandingPage from './containers/LandingPage'
import AdminPage from './containers/AdminPage'
import LoginPage from './auth/LoginPage'
import RegisterPage from './auth/RegisterPage'
import ForgotPasswordPage from './auth/ForgotPasswordPage'
import UnauthorizedPage from './auth/Unauthorized'

const commonRoutes = [
  { path: '/appearance', element: <AdminPage /> },
  { path: '/login', element: <LoginPage /> },
  { path: '/', element: <LandingPage /> },
  { path: '/register', element: <RegisterPage /> },
  { path: '/forgot-password', element: <ForgotPasswordPage /> },
  { path: '/unauthorized', element: <UnauthorizedPage /> },
]

export default commonRoutes
