import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import saveRole from '../services/saveRole'

const FormTambahkanRole = ({  }) => {
  const { control, handleSubmit } = useForm()


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await saveRole({
      ...cleanData,
      
    })
		navigate(`/role`)
  }

  return (
    <Form 
	  title="Tambahkan Role" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Name"
            placeholder="Masukkan name"
            {...field}
          />
        )}
      />
	  <Controller
        name="allowedPermissions"
        control={control}
        render={({ field }) => (
          <InputField
            label="Allowed Permissions"
            placeholder="Masukkan allowed permissions"
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormTambahkanRole
