import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import updateRole from '../services/updateRole'

const FormUbahRole = ({ role }) => {
  const { control, handleSubmit } = useForm({ defaultValues: role })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await updateRole({
      ...cleanData,
      
    })
		navigate(`/role/${role.id}`)
  }

  return (
    <Form 
	  title="Ubah Role" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id Role"
            placeholder="Masukkan id role"
			type="number"
			disabled            defaultValue={role.id}            {...field}
          />
        )}
      />
	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Role"
            placeholder="Masukkan nama role"
            defaultValue={role.name}            {...field}
          />
        )}
      />
	  <Controller
        name="allowedPermissions"
        control={control}
        render={({ field }) => (
          <InputField
            label="Allowed Permissions"
            placeholder="Masukkan allowed permissions"
            defaultValue={role.allowedPermissions}            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormUbahRole
