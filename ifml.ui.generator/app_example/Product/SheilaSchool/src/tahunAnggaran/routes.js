import RequireAuth from 'commons/auth/RequireAuth'

import DaftarTahunAnggaranPage from './containers/DaftarTahunAnggaranPage'
import LaporanArusKasTahunAnggaranPage from './containers/LaporanArusKasTahunAnggaranPage'

const tahunAnggaranRoutes = [
	{ 
		path: "/report/summary/:id",
		element: <LaporanArusKasTahunAnggaranPage />,
	},
	{ 
		path: "/report/summary",
		element: <DaftarTahunAnggaranPage />,
	}
]

export default tahunAnggaranRoutes
