import React from 'react';
import { Link } from 'react-router-dom';

import { Button, ListItem, VisualizationAttr } from 'commons/components';

const TahunCard = ({ tahunItem }) => {
  return (
    <ListItem>
      {/* Data Binding Tahun Card Element */}
      <div className="card-body">
		<VisualizationAttr label='Laporan Keuangan Tahun Anggaran' content={tahunItem?.name}/>
      <div className="card-actions justify-end">
        {/* View Element Event Tahun Card Element*/}
        <Link to={`/report/summary/${tahunItem.id}`}>
          <Button variant="tertiary">Lihat</Button>
        </Link>
      </div>
      </div>
    </ListItem>
  )
};

export default TahunCard;
