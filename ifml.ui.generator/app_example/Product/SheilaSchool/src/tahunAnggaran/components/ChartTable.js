import React, { useRef } from 'react'
import { TableBody, TableHead, TableRow, TableCell } from 'commons/components'

const ChartTable = ({ chartItem }) => {
	const DISTINCT_ROW = [
    'Aktivitas Operasional',
    'Aktivitas Investasi',
    'Aktivitas Pendanaan',
    '',
  ]

  const tableBody = useRef([])
  return chartItem.map((row, index) => {
    if (DISTINCT_ROW.includes(row.name)) {
      const tBody = [...tableBody.current]
      tableBody.current = []
      return (
        <>
          {tBody.length !== 0 && (
            <TableBody>
              {tBody.map(chart => (
                <TableRow>
                  <TableCell>
                    <span
                      style={{ paddingLeft: `${parseInt(chart.level)}rem` }}
                    >
                      {chart.name}
                    </span>
                  </TableCell>
                  <TableCell isCurrency={!!chart.amount}>
                    {chart.amount}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          )}
          <TableHead>
            <TableRow>
              <TableCell level={row.level}>
                <span style={{ paddingLeft: `${parseInt(row.level)}rem` }}>
                  {row.name}
                </span>
              </TableCell>
              <TableCell>{row.amount}</TableCell>
            </TableRow>
          </TableHead>
        </>
      )
    } else {
      tableBody.current.push(row)
      if (index === chartItem.length - 1)
        return (
          tableBody.current.length !== 0 && (
            <TableBody>
              {tableBody.current.map(chart => (
                <TableRow>
                  <TableCell>
                    <span
                      style={{ paddingLeft: `${parseInt(chart.level)}rem` }}
                    >
                      {chart.name}
                    </span>
                  </TableCell>
                  <TableCell isCurrency>{chart.amount ?? 0}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          )
        )
    }
  })
};

export default ChartTable;
