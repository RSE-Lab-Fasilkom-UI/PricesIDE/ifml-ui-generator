import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'

const getChartOfAccountEntryElement = (params = {}) => {
	const { getToken } = UseToken();
	const token = getToken();
	let paramsGet = Object.assign(params, {token});
	return axios.get(`${environment.rootApi}/call/automatic-report-periodic/list`, {
		params: paramsGet,		
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default getChartOfAccountEntryElement
