import RequireAuth from 'commons/auth/RequireAuth'

import KonfirmasiDonasiOfflinePage from './containers/KonfirmasiDonasiOfflinePage'
import BerhasilKonfirmasiPage from './containers/BerhasilKonfirmasiPage'
import ListKonfirmasiDonasiOfflinePage from './containers/ListKonfirmasiDonasiOfflinePage'
import DetailKonfirmasiDonasiOfflinePage from './containers/DetailKonfirmasiDonasiOfflinePage'
import UpdateStatusKonfirmasiDonasiOfflinePage from './containers/UpdateStatusKonfirmasiDonasiOfflinePage'

const confirmOfflineDonationRoutes = [
	{ 
		path: "/donation",
		element: <RequireAuth permissionNeeded="ReadCOD"><ListKonfirmasiDonasiOfflinePage/></RequireAuth>
	},
	{ 
		path: "/donation/update",
		element: <RequireAuth permissionNeeded="UpdateCOD"><UpdateStatusKonfirmasiDonasiOfflinePage/></RequireAuth>
	},
	{ 
		path: "/donation/:id",
		element: <BerhasilKonfirmasiPage />,
	},
	{ 
		path: "/donation/detail/:id",
		element: <DetailKonfirmasiDonasiOfflinePage />,
	},
	{ 
		path: "/donation/confirm",
		element: <KonfirmasiDonasiOfflinePage />,
	}
]

export default confirmOfflineDonationRoutes
