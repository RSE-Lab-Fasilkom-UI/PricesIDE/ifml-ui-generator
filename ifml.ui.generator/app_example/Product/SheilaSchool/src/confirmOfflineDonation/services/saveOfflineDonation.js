import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'

import FormData from 'form-data';


const saveOfflineDonation = (data = {}) => {
	let body = data;
	const formData = new FormData();
	Object.entries(data).forEach(([key, value ]) => {
		formData.append(key, value);
	});
	body = formData;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.post(`${environment.rootApi}/call/confirm-offline-donation/save`, body,
	{
		params: { token }, 
		
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default saveOfflineDonation
