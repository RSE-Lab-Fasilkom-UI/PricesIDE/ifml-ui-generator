import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'




const updateStatusOfflineDonation = (data = {}) => {
	let body = data;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.put(`${environment.rootApi}/call/confirm-offline-donation/updatestatus`, body,
	{
		params: { token }, 
		
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default updateStatusOfflineDonation
