import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  VisualizationAttr,
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import updateStatusOfflineDonation from '../services/updateStatusOfflineDonation'

const FormUpdateStatusKonfirmasiDonasiOffline = ({ confirmOfflineDonation, statuses }) => {
  const { control, handleSubmit } = useForm({ defaultValues: confirmOfflineDonation })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await updateStatusOfflineDonation({
      ...cleanData,
      
    })
		navigate(`/donation/detail/${confirmOfflineDonation.id}`)
  }

  return (
    <Form 
	  title="Update Status Konfirmasi Donasi Offline" 
	  onSubmit={handleSubmit(kirim)}
	  >
      <VisualizationAttr
        label="Nama"
        content={confirmOfflineDonation?.name}
		
      />
      <VisualizationAttr
        label="Deskripsi"
        content={confirmOfflineDonation?.description}
		
      />
      <VisualizationAttr
        label="Tanggal"
        content={confirmOfflineDonation?.date}
		
      />
      <VisualizationAttr
        label="Metode Pembayaran"
        content={confirmOfflineDonation?.paymentMethod}
		
      />
      <VisualizationAttr
        label="Jumlah"
        content={confirmOfflineDonation?.amount}
		
      />

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id"
            placeholder="Masukkan id"
			disabled            defaultValue={confirmOfflineDonation.id}            {...field}
          />
        )}
      />
	  <Controller
        name="status"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Status"
            options={statuses}
            placeholder="Masukkan status"
			defaultValue={confirmOfflineDonation.status}
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormUpdateStatusKonfirmasiDonasiOffline
