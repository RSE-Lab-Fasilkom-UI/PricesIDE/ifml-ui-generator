import React, { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  FileInputField,
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import saveOfflineDonation from '../services/saveOfflineDonation'

const FormKonfirmasiDonasiOffline = ({ programs }) => {
  const { control, handleSubmit } = useForm()

  const [ files, setFiles ] = useState({});

  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await saveOfflineDonation({
      ...cleanData,
      ...files,
    })
		navigate(`/donation/:id`)
  }

  return (
    <Form 
	  title="Konfirmasi Donasi Offline" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Donatur"
            placeholder="Masukkan nama donatur"
            {...field}
          />
        )}
      />
	  <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            label="Email"
            placeholder="Masukkan email"
            {...field}
          />
        )}
      />
	  <Controller
        name="phone"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nomor Telepon"
            placeholder="Masukkan nomor telepon"
            {...field}
          />
        )}
      />
	  <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah Donasi"
            placeholder="Masukkan jumlah donasi"
			type="number"
            {...field}
          />
        )}
      />
	  <Controller
        name="date"
        control={control}
        render={({ field }) => (
          <InputField
            label="Tanggal Transfer"
            placeholder="Masukkan tanggal transfer"
            {...field}
          />
        )}
      />
	  <Controller
        name="paymentMethod"
        control={control}
        render={({ field }) => (
          <InputField
            label="Metode Pembayaran"
            placeholder="Masukkan metode pembayaran"
            {...field}
          />
        )}
      />
      <FileInputField
        onChange={(e) => setFiles({ ...files, proofoftransfer: e.target.files[0]})}
        label="Bukti Transfer"
        placeholder="Masukkan bukti transfer"
      />
	  <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            label="Keterangan"
            placeholder="Masukkan keterangan"
            {...field}
          />
        )}
      />
	  <Controller
        name="idprogram"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Nama Program Terkait"
            options={programs}
            placeholder="Masukkan nama program terkait"
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormKonfirmasiDonasiOffline
