import React from 'react';
import { Link } from 'react-router-dom';

import { Button, TableRow, TableCell } from 'commons/components';

const DonasiTable = ({ donasiItem }) => {
  return (
    <TableRow distinct={false}>
      {/* Data Binding Donasi Table Element*/}
      <TableCell
		
		>{donasiItem?.name}</TableCell>
      <TableCell
		
		>{donasiItem?.email}</TableCell>
      <TableCell
		
		>{donasiItem?.phone}</TableCell>
      <TableCell
		
		>{donasiItem?.paymentMethod}</TableCell>
      <TableCell
		
		>{donasiItem?.amount}</TableCell>
      <TableCell
		
		>{donasiItem?.status}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          {/* View Element Event Donasi Table Element*/}
          <Link to={`/donation/update?id=${donasiItem.id}`}>
            <Button variant="secondary" className="btn-sm">Update Status</Button>
          </Link>
          <Link to={`/donation/detail/${donasiItem.id}`}>
            <Button variant="tertiary" className="btn-sm">Detail</Button>
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
};

export default DonasiTable;
