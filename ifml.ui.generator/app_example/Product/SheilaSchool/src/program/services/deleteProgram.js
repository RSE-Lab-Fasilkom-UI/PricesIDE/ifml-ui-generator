import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'
import cleanFormData from 'commons/utils/cleanFormData'



const deleteProgram = (data = {}) => {
	let body = data;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.delete(`${environment.rootApi}/call/program/delete`, 
	{
		params: { token }, 
		data: cleanFormData(body),
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default deleteProgram
