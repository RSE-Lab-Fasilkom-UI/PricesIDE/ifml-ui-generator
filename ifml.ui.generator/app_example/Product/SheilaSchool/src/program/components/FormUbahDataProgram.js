import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import updateProgram from '../services/updateProgram'

const FormUbahDataProgram = ({ programData }) => {
  const { control, handleSubmit } = useForm({ defaultValues: programData })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await updateProgram({
      ...cleanData,
      
    })
		navigate(`/programs/${programData.id}`)
  }

  return (
    <Form 
	  title="Ubah Data Program" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id"
            placeholder="Masukkan id"
			disabled            defaultValue={programData.id}            {...field}
          />
        )}
      />
	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Program"
            placeholder="Masukkan nama program"
            defaultValue={programData.name}            {...field}
          />
        )}
      />
	  <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            label="Deskripsi"
            placeholder="Masukkan deskripsi"
            defaultValue={programData.description}            {...field}
          />
        )}
      />
	  <Controller
        name="target"
        control={control}
        render={({ field }) => (
          <InputField
            label="Target"
            placeholder="Masukkan target"
            defaultValue={programData.target}            {...field}
          />
        )}
      />
	  <Controller
        name="partner"
        control={control}
        render={({ field }) => (
          <InputField
            label="Partner"
            placeholder="Masukkan partner"
            defaultValue={programData.partner}            {...field}
          />
        )}
      />
	  <Controller
        name="logoUrl"
        control={control}
        render={({ field }) => (
          <InputField
            label="URL Gambar Program"
            placeholder="Masukkan url gambar program"
            defaultValue={programData.logoUrl}            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormUbahDataProgram
