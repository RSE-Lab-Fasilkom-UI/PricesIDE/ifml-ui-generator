import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import saveProgram from '../services/saveProgram'

const FormTambahkanProgram = ({  }) => {
  const { control, handleSubmit } = useForm()


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await saveProgram({
      ...cleanData,
      
    })
		navigate(`/programs`)
  }

  return (
    <Form 
	  title="Tambahkan Program" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            label="Nama Program"
            placeholder="Masukkan nama program"
            {...field}
          />
        )}
      />
	  <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            label="Deskripsi"
            placeholder="Masukkan deskripsi"
            {...field}
          />
        )}
      />
	  <Controller
        name="target"
        control={control}
        render={({ field }) => (
          <InputField
            label="Target"
            placeholder="Masukkan target"
            {...field}
          />
        )}
      />
	  <Controller
        name="partner"
        control={control}
        render={({ field }) => (
          <InputField
            label="Partner"
            placeholder="Masukkan partner"
            {...field}
          />
        )}
      />
	  <Controller
        name="logoUrl"
        control={control}
        render={({ field }) => (
          <InputField
            label="URL Gambar Program"
            placeholder="Masukkan url gambar program"
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormTambahkanProgram
