import RequireAuth from 'commons/auth/RequireAuth'

import DaftarProgramPage from './containers/DaftarProgramPage'
import TambahProgramPage from './containers/TambahProgramPage'
import DetailProgramPage from './containers/DetailProgramPage'
import UbahProgramPage from './containers/UbahProgramPage'

const programRoutes = [
	{ 
		path: "/programs/tambah",
		element: <RequireAuth permissionNeeded="CreateProgram"><TambahProgramPage/></RequireAuth>
	},
	{ 
		path: "/programs/ubah",
		element: <RequireAuth permissionNeeded="UpdateProgram"><UbahProgramPage/></RequireAuth>
	},
	{ 
		path: "/programs/:id",
		element: <DetailProgramPage />,
	},
	{ 
		path: "/programs",
		element: <DaftarProgramPage />,
	}
]

export default programRoutes
