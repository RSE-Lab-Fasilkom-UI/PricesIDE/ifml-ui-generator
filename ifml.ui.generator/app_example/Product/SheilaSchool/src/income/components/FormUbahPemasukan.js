import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import updateIncome from '../services/updateIncome'

const FormUbahPemasukan = ({ income, programs, chartOfAccounts }) => {
  const { control, handleSubmit } = useForm({ defaultValues: income })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await updateIncome({
      ...cleanData,
      
    })
		navigate(`/income/${income.id}`)
  }

  return (
    <Form 
	  title="Ubah Pemasukan" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id Income"
            placeholder="Masukkan id income"
			type="number"
			disabled            defaultValue={income.id}            {...field}
          />
        )}
      />
	  <Controller
        name="datestamp"
        control={control}
        render={({ field }) => (
          <InputField
            label="Tanggal"
            placeholder="Masukkan tanggal"
            defaultValue={income.datestamp}            {...field}
          />
        )}
      />
	  <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            label="Deskripsi"
            placeholder="Masukkan deskripsi"
            defaultValue={income.description}            {...field}
          />
        )}
      />
	  <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah"
            placeholder="Masukkan jumlah"
			type="number"
            defaultValue={income.amount}            {...field}
          />
        )}
      />
	  <Controller
        name="paymentMethod"
        control={control}
        render={({ field }) => (
          <InputField
            label="Metode Pembayaran"
            placeholder="Masukkan metode pembayaran"
            defaultValue={income.paymentMethod}            {...field}
          />
        )}
      />
	  <Controller
        name="idProgram"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Nama Program Terkait"
            options={programs}
            placeholder="Masukkan nama program terkait"
			defaultValue={income.idProgram}
            {...field}
          />
        )}
      />
	  <Controller
        name="idCoa"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Kode Akun"
            options={chartOfAccounts}
            placeholder="Masukkan kode akun"
			defaultValue={income.idCoa}
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormUbahPemasukan
