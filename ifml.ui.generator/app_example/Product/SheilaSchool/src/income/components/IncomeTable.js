import React from 'react';
import { Link } from 'react-router-dom';

import { Button, TableRow, TableCell } from 'commons/components';

const IncomeTable = ({ incomeItem }) => {
  return (
    <TableRow distinct={false}>
      {/* Data Binding Income Table Element*/}
      <TableCell
		
		>{incomeItem?.datestamp}</TableCell>
      <TableCell
		
		>{incomeItem?.programName}</TableCell>
      <TableCell
		
		 isCurrency>{incomeItem?.amount}</TableCell>
      <TableCell
		
		>{incomeItem?.paymentMethod}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          {/* View Element Event Income Table Element*/}
          <Link to={`/income/${incomeItem.id}`}>
            <Button variant="tertiary" className="btn-sm">Detail</Button>
          </Link>
          <Link to={`/income/ubah?id=${incomeItem.id}`}>
            <Button variant="secondary" className="btn-sm">Edit</Button>
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
};

export default IncomeTable;
