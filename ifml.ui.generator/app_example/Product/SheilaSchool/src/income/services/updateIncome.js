import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'




const updateIncome = (data = {}) => {
	let body = data;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.put(`${environment.rootApi}/call/income/update`, body,
	{
		params: { token }, 
		
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default updateIncome
