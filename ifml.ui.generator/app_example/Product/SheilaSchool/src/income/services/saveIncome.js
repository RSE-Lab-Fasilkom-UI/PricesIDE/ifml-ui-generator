import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'




const saveIncome = (data = {}) => {
	let body = data;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.post(`${environment.rootApi}/call/income/save`, body,
	{
		params: { token }, 
		
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default saveIncome
