import axios from 'axios'
import UseToken from 'commons/utils/token'
import environment from 'commons/utils/environment'
import cleanFormData from 'commons/utils/cleanFormData'



const deleteExpense = (data = {}) => {
	let body = data;

	const { getToken } = UseToken();
	const token = getToken();
	return axios.delete(`${environment.rootApi}/call/expense/delete`, 
	{
		params: { token }, 
		data: cleanFormData(body),
		headers: {
			'Authorization': token
		}
	}).catch((error) => {
		console.error(error)
	})
} 

export default deleteExpense
