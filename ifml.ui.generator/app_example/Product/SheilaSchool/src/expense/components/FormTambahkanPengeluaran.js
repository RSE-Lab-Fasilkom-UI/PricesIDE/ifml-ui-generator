import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import saveExpense from '../services/saveExpense'

const FormTambahkanPengeluaran = ({ programs, chartOfAccounts }) => {
  const { control, handleSubmit } = useForm()


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await saveExpense({
      ...cleanData,
      
    })
		navigate(`/expense`)
  }

  return (
    <Form 
	  title="Tambahkan Pengeluaran" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="datestamp"
        control={control}
        render={({ field }) => (
          <InputField
            label="Tanggal"
            placeholder="Masukkan tanggal"
            {...field}
          />
        )}
      />
	  <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            label="Deskripsi"
            placeholder="Masukkan deskripsi"
            {...field}
          />
        )}
      />
	  <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah"
            placeholder="Masukkan jumlah"
            {...field}
          />
        )}
      />
	  <Controller
        name="idProgram"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Nama Program Terkait"
            options={programs}
            placeholder="Masukkan nama program terkait"
            {...field}
          />
        )}
      />
	  <Controller
        name="idCoa"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Kode Akun"
            options={chartOfAccounts}
            placeholder="Masukkan kode akun"
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormTambahkanPengeluaran
