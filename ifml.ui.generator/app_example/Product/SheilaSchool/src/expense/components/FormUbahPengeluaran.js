import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import {
  Button,
  Form,
  SelectionField,
  MultiSelectionField,
  InputField,
  
  
} from 'commons/components'

import cleanFormData from 'commons/utils/cleanFormData'

import updateExpense from '../services/updateExpense'

const FormUbahPengeluaran = ({ expense, programs, chartOfAccounts }) => {
  const { control, handleSubmit } = useForm({ defaultValues: expense })


  const navigate = useNavigate()

  const kirim = async (data) => {
    const cleanData = cleanFormData(data)
    await updateExpense({
      ...cleanData,
      
    })
		navigate(`/expense/${expense.objectDetailExpense}`)
  }

  return (
    <Form 
	  title="Ubah Pengeluaran" 
	  onSubmit={handleSubmit(kirim)}
	  >

	  <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            label="Id Expense"
            placeholder="Masukkan id expense"
			disabled            defaultValue={expense.id}            {...field}
          />
        )}
      />
	  <Controller
        name="datestamp"
        control={control}
        render={({ field }) => (
          <InputField
            label="Tanggal"
            placeholder="Masukkan tanggal"
            defaultValue={expense.datestamp}            {...field}
          />
        )}
      />
	  <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            label="Deskripsi"
            placeholder="Masukkan deskripsi"
            defaultValue={expense.description}            {...field}
          />
        )}
      />
	  <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            label="Jumlah"
            placeholder="Masukkan jumlah"
            defaultValue={expense.amount}            {...field}
          />
        )}
      />
	  <Controller
        name="idProgram"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Nama Program Terkait"
            options={programs}
            placeholder="Masukkan nama program terkait"
			defaultValue={expense.idProgram}
            {...field}
          />
        )}
      />
	  <Controller
        name="idCoa"
        control={control}
        render={({ field }) => (
		<SelectionField
            label="Kode Akun"
            options={chartOfAccounts}
            placeholder="Masukkan kode akun"
			defaultValue={expense.idCoa}
            {...field}
          />
        )}
      />
		<div className="card-actions justify-end">
			<Button type="submit" variant="primary">Kirim</Button>
		</div>
    </Form>
  )
}

export default FormUbahPengeluaran
