import React from 'react';
import { Link } from 'react-router-dom';

import { Button, TableRow, TableCell } from 'commons/components';

const ExpenseTable = ({ expenseItem }) => {
  return (
    <TableRow distinct={false}>
      {/* Data Binding Expense Table Element*/}
      <TableCell
		
		>{expenseItem?.datestamp}</TableCell>
      <TableCell
		
		>{expenseItem?.programName}</TableCell>
      <TableCell
		className="whitespace-normal max-w-[32ch]"
		>{expenseItem?.description}</TableCell>
      <TableCell
		
		>{expenseItem?.coaName}</TableCell>
      <TableCell
		
		>{expenseItem?.amount}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          {/* View Element Event Expense Table Element*/}
          <Link to={`/expense/ubah?id=${expenseItem.id}`}>
            <Button variant="secondary" className="btn-sm">Edit</Button>
          </Link>
          <Link to={`/expense/${expenseItem.id}`}>
            <Button variant="tertiary" className="btn-sm">Detail</Button>
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
};

export default ExpenseTable;
