import { useRoutes } from 'react-router-dom'

import programRoutes from 'program/routes'
import commonRoutes from 'commons/routes.js'
import incomeRoutes from 'income/routes.js'
import expenseRoutes from 'expense/routes.js'
import codDonation from 'confirmOfflineDonation/routes.js'
import staticPageRoutes from 'staticPage/routes'
import userRoutes from 'user/routes'
import roleRoutes from 'role/routes'
import tahunAnggaranRoutes from 'tahunAnggaran/routes'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...programRoutes,
    ...incomeRoutes,
    ...expenseRoutes,
    ...userRoutes,
    ...roleRoutes,
    ...staticPageRoutes,
    ...commonRoutes,
    ...codDonation,
    ...tahunAnggaranRoutes
  ])

  return router
}

export default GlobalRoutes
