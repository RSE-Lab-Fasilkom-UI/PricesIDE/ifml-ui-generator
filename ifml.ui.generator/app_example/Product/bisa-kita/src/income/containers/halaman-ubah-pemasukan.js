//Halaman Ubah Pemasukan Containers
import React, { useEffect, useState } from 'react'

import CallprogramlistService from 'programs/services/call-program-list.service'
import CallchartofaccountlistService from 'journalReport/services/call-chart-of-account-list.service'
import FormUbahIncome from '../components/FormUbahIncome.js'
import { useSearchParams } from 'react-router-dom'
import CallincomedetailService from 'income/services/call-income-detail.service'

const HalamanUbahPemasukan = props => {
  const [searchParams] = useSearchParams()
  const id = searchParams.get('id')

  const [income, setIncome] = useState()
  const [programs, setPrograms] = useState()
  const [chartOfAccounts, setChartOfAccounts] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: incomeDetail } = await CallincomedetailService.call({ id })
      const { data: programList } = await CallprogramlistService.call()
      const { data: coa } = await CallchartofaccountlistService.call()
      setIncome(incomeDetail.data)
      setPrograms(programList.data)
      setChartOfAccounts(coa.data)
    }
    fetch()
  }, [])

  return (
    <>
      {income && programs && chartOfAccounts && (
        <FormUbahIncome
          {...props}
          income={income}
          programs={programs}
          chartOfAccounts={chartOfAccounts}
        />
      )}
    </>
  )
}

export default HalamanUbahPemasukan
