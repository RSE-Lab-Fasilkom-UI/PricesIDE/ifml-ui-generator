import { withAuth } from 'commons/auth'

//Income
import CatatanPemasukanComponent from './containers/catatan-pemasukan.js'
import HalamanTambahPemasukanComponent from './containers/halaman-tambah-pemasukan.js'
import HalamanDetailPemasukanComponent from './containers/halaman-detail-pemasukan.js'
import HalamanUbahPemasukanComponent from './containers/halaman-ubah-pemasukan.js'

const path = '/income'

const incomeRoutes = [
  {
    path: path + '/tambah',
    element: withAuth(HalamanTambahPemasukanComponent),
  },
  { path: path + '/ubah', element: withAuth(HalamanUbahPemasukanComponent) },
  { path: path + '/:id', element: withAuth(HalamanDetailPemasukanComponent) },
  { path: path, element: withAuth(CatatanPemasukanComponent) },
]

export default incomeRoutes
