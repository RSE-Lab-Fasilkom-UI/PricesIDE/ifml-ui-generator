import React from 'react'
import { Link } from 'react-router-dom'

const IncomeMenuFeature = props => {
  return (
    <div className="mx-auto max-w-screen-xl p-6 flex flex-col sm:flex-row justify-center sm:justify-end items-center gap-4">
      <Link className="btn" to="/income/tambah">
        Tambah Pemasukan
      </Link>
    </div>
  )
}
export default IncomeMenuFeature
