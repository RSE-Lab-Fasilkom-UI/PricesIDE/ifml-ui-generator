import React from 'react'
import InputField from 'commons/components/InputField/InputField'
import Form from 'commons/components/Form/Form'
import Button from 'commons/components/Button/Button'
import SelectionField from 'commons/components/SelectionField/SelectionField'
import CallincomeupdateService from '../services/call-income-update.service'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import cleanFormData from 'commons/utils/cleanFormData'

const FormUbahPemasukan = ({ income, programs, chartOfAccounts }) => {
  const { control, handleSubmit } = useForm({ defaultValues: income })
  const navigate = useNavigate()

  const Kirim = async data => {
    const cleanData = cleanFormData(data)
    await CallincomeupdateService.call(cleanData)
    navigate(`/income/${income.id}`)
  }

  return (
    <Form
      title="Ubah Pemasukan"
      id_name="ubah-pemasukan"
      onSubmit={handleSubmit(Kirim)}
      variant={{
        shape: 'default',
        color: 'blue',
        borderColor: 'transparent',
        bgColor: 'white',
        alignment: 'center',
      }}
    >
      <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Id Income"
            placeholder="Fill the Id Income"
            defaultValue={income.id}
            {...field}
          />
        )}
      />
      <Controller
        name="datestamp"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Tanggal"
            placeholder="Fill the Tanggal"
            defaultValue={income.datestamp}
            {...field}
          />
        )}
      />
      <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Deskripsi"
            placeholder="Fill the Deskripsi"
            defaultValue={income.description}
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Jumlah"
            placeholder="Fill the Jumlah"
            defaultValue={income.amount}
            {...field}
          />
        )}
      />
      <Controller
        name="paymentMethod"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Metode Pembayaran"
            placeholder="Fill the Metode Pembayaran"
            defaultValue={income.paymentMethod}
            {...field}
          />
        )}
      />
      <Controller
        name="idProgram"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={programs}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Program Terkait"
            defaultValue={income.idProgram}
            placeholder="Fill the Nama Program Terkait"
            {...field}
          />
        )}
      />
      <Controller
        name="idCoa"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={chartOfAccounts}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Kode Akun"
            defaultValue={income.idCoa}
            placeholder="Fill the Kode Akun"
            {...field}
          />
        )}
      />
      <Button
        type="submit"
        variant={{
          shape: 'default',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
        }}
        text="Kirim"
      />
    </Form>
  )
}

export default FormUbahPemasukan
