import { withAuth } from 'commons/auth'

//Expense
import CatatanPengeluaran from './containers/catatan-pengeluaran'
import HalamanDetailPengeluaran from './containers/halaman-detail-pengeluaran'
import HalamanTambahPengeluaran from './containers/halaman-tambah-pengeluaran'
import HalamanUbahPengeluaran from './containers/halaman-ubah-pengeluaran'

const path = '/expense'

const expenseRoutes = [
  {
    path: path + '/tambah',
    element: withAuth(HalamanTambahPengeluaran),
  },
  { path: path + '/ubah', element: withAuth(HalamanUbahPengeluaran) },
  { path: path + '/:id', element: withAuth(HalamanDetailPengeluaran) },
  { path: path, element: withAuth(CatatanPengeluaran) },
]

export default expenseRoutes
