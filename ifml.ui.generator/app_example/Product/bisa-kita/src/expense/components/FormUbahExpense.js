import React from 'react'
import InputField from 'commons/components/InputField/InputField'
import Form from 'commons/components/Form/Form'
import Button from 'commons/components/Button/Button'
import SelectionField from 'commons/components/SelectionField/SelectionField'
import CallexpenseupdateService from '../services/call-expense-update.service'
import { Controller, useForm } from 'react-hook-form'
import { useNavigate } from 'react-router-dom'
import cleanFormData from 'commons/utils/cleanFormData'

const FormUbahExpense = ({ expense, programs, chartOfAccounts }) => {
  const { control, handleSubmit } = useForm({ defaultValues: expense })
  const navigate = useNavigate()

  const Kirim = async data => {
    const cleanData = cleanFormData(data)
    console.log(
      '🚀 ~ file: FormUbahExpense.js ~ line 17 ~ Kirim ~ cleanData',
      cleanData
    )
    await CallexpenseupdateService.call(cleanData)
    navigate(`/expense/${expense.id}`)
  }

  return (
    <Form
      title="Ubah Pengeluaran"
      id_name="ubah-pengeluaran"
      onSubmit={handleSubmit(Kirim)}
      variant={{
        shape: 'default',
        color: 'blue',
        borderColor: 'transparent',
        bgColor: 'white',
        alignment: 'center',
      }}
    >
      <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Id Expense"
            placeholder="Fill the Id Expense"
            defaultValue={expense.id}
            {...field}
          />
        )}
      />
      <Controller
        name="datestamp"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Tanggal"
            placeholder="Fill the Tanggal"
            defaultValue={expense.datestamp}
            {...field}
          />
        )}
      />
      <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Deskripsi"
            placeholder="Fill the Deskripsi"
            defaultValue={expense.description}
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Jumlah"
            placeholder="Fill the Jumlah"
            defaultValue={expense.amount}
            {...field}
          />
        )}
      />
      <Controller
        name="idProgram"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={programs}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Program Terkait"
            placeholder="Fill the Nama Program Terkait"
            defaultValue={expense.idProgram}
            {...field}
          />
        )}
      />
      <Controller
        name="idCoa"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={chartOfAccounts}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Kode Akun"
            placeholder="Fill the Kode Akun"
            defaultValue={expense.idCoa}
            {...field}
          />
        )}
      />
      <Button
        type="submit"
        variant={{
          shape: 'default',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
        }}
        text="Kirim"
      />
    </Form>
  )
}

export default FormUbahExpense
