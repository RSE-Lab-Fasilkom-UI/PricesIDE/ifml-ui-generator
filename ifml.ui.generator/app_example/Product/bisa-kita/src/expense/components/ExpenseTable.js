//Table
import React from 'react'
import Button from 'commons/components/Button/Button'
import TableRow from 'commons/components/Table/TableRow'
import TableCell from 'commons/components/Table/TableCell'
import { Link } from 'react-router-dom'

const ExpenseTable = ({ expense }) => {
  return (
    <TableRow distinct={false}>
      {/* //Data Binding Expense List Element*/}

      <TableCell>{expense.datestamp}</TableCell>

      <TableCell>{expense.programName}</TableCell>

      <TableCell>{expense.description}</TableCell>

      <TableCell>{expense.coaName}</TableCell>

      <TableCell isCurrency>{expense.amount}</TableCell>
      <TableCell>
        <div class="btn-group gap-2">
          <Link to={`/expense/${expense.id}`}>
            <Button className="btn-sm btn-ghost" text="Detail" />
          </Link>
          <Link to={`/expense/ubah?id=${expense.id}`}>
            <Button className="btn-sm btn-primary btn-outline" text="Edit" />
          </Link>
        </div>
      </TableCell>
    </TableRow>
  )
}

export default ExpenseTable
