import React from 'react'
import InputField from 'commons/components/InputField/InputField'
import SelectionField from 'commons/components/SelectionField/SelectionField'
import Form from 'commons/components/Form/Form'
import Button from 'commons/components/Button/Button'
import CallexpensesaveService from '../services/call-expense-save.service'
import { useNavigate } from 'react-router-dom'
import { Controller, useForm } from 'react-hook-form'

const FormTambahExpense = ({ programs, chartOfAccounts }) => {
  const { control, handleSubmit } = useForm()
  const navigate = useNavigate()
  const Kirim = async data => {
    await CallexpensesaveService.call(data)
    navigate('/expense')
  }

  return (
    <Form
      title="Tambahkan Pengeluaran"
      id_name="tambahkan-pengeluaran"
      onSubmit={handleSubmit(Kirim)}
      variant={{
        shape: 'default',
        color: 'blue',
        borderColor: 'transparent',
        bgColor: 'white',
        alignment: 'center',
      }}
    >
      <Controller
        name="datestamp"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Tanggal"
            placeholder="Fill the Tanggal"
            {...field}
          />
        )}
      />
      <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Deskripsi"
            placeholder="Fill the Deskripsi"
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Jumlah"
            placeholder="Fill the Jumlah"
            {...field}
          />
        )}
      />
      <Controller
        name="idProgram"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={programs}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Program Terkait"
            placeholder="Fill the Nama Program Terkait"
            {...field}
          />
        )}
      />
      <Controller
        name="idCoa"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={chartOfAccounts}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Kode Akun"
            placeholder="Fill the Kode Akun"
            {...field}
          />
        )}
      />
      <Button
        type="submit"
        variant={{
          shape: 'default',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
        }}
        text="Kirim"
      />
    </Form>
  )
}

export default FormTambahExpense
