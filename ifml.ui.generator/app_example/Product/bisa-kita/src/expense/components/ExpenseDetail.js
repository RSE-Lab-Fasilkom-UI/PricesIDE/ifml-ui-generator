import React from 'react'
import Button from 'commons/components/Button/Button'
import Detail from 'commons/components/Detail/Detail'
import VisualizationAttr from 'commons/components/VisualizationAttr/VisualizationAttr'
import CallexpensedeleteService from '../services/call-expense-delete.service'
import { useNavigate } from 'react-router-dom'

const DetailExpense = ({ expense }) => {
  const navigate = useNavigate()
  const Hapus = async () => {
    await CallexpensedeleteService.call({
      id: expense.id.toString(),
    })
    navigate('/expense')
  }

  const Ubah = async () => {
    navigate(`/expense/ubah?id=${expense.id}`)
  }

  return (
    <Detail
      variant={{ shape: 'default', borderColor: 'blue', bgColor: 'white' }}
    >
      {/* //Data Binding Expense Data*/}
      <VisualizationAttr
        title_name="Tanggal"
        content={expense.datestamp}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Deskripsi"
        content={expense.description}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Jumlah"
        content={expense.amount}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Nama Program"
        content={expense.programName}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Jenis Pengeluaran"
        content={expense.coaName}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <div class="w-full flex gap-4 mt-6">
        <div className="flex-1">
          <Button
            onClick={Hapus}
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
            text="Hapus"
          />
        </div>
        <div className="flex-1">
          <Button
            onClick={Ubah}
            variant={{
              shape: 'default',
              color: 'white',
              bgColor: 'blue',
              borderColor: 'blue',
            }}
            text="Ubah"
          />
        </div>
      </div>
    </Detail>
  )
}

export default DetailExpense
