//Catatan Pengeluaran Containers
import React, { useEffect, useState } from 'react'
import { AuthConsumer } from 'commons/auth'

import ExpenseFeatureComponent from '../components/ExpenseFeature.js'

import Table from 'commons/components/Table/Table'
import TableRow from 'commons/components/Table/TableRow'
import TableCell from 'commons/components/Table/TableCell'
import TableBody from 'commons/components/Table/TableBody'
import TableHead from 'commons/components/Table/TableHead'

import TableExpenseContentComponent from '../components/ExpenseTable.js'
import CallexpenselistService from '../services/call-expense-list.service'

const CatatanPengeluaran = props => {
  const [jsonAllExpense, setJsonAllExpense] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: expenseList } = await CallexpenselistService.call()
      setJsonAllExpense(expenseList.data)
    }
    fetch()
  }, [])

  return (
    <>
      <AuthConsumer>
        {values => {
          return <ExpenseFeatureComponent {...values} />
        }}
      </AuthConsumer>
      <div class="mx-auto max-w-screen-xl px-6 mb-12">
        <Table id="table-TableExpenseContentComponent">
          <TableHead>
            <TableRow>
              {/* //Data Binding Expense List Element*/}
              <TableCell id="Tanggal">Tanggal</TableCell>
              <TableCell id="Program">Program</TableCell>
              <TableCell id="Deskripsi">Deskripsi</TableCell>
              <TableCell id="Kategori">Kategori</TableCell>
              <TableCell id="Jumlah">Jumlah</TableCell>
              <TableCell id="Aksi">
                <div class="text-center">Aksi</div>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {jsonAllExpense &&
              jsonAllExpense.map(expense => (
                <TableExpenseContentComponent {...props} expense={expense} />
              ))}
          </TableBody>
        </Table>
      </div>
    </>
  )
}

export default CatatanPengeluaran
