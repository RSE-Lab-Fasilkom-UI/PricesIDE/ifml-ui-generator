//Halaman Detail Pengeluaran Containers
import React, { useEffect, useState } from 'react'

import DetailExpenseComponent from '../components/ExpenseDetail.js'
import { useParams } from 'react-router-dom'
import CallexpensedetailService from 'expense/services/call-expense-detail.service.js'

const HalamanDetailPengeluaran = props => {
  const [expense, setExpense] = useState()
  const { id } = useParams()

  useEffect(() => {
    const fetch = async () => {
      const { data: expenseDetail } = await CallexpensedetailService.call({
        id,
      })
      setExpense(expenseDetail.data)
    }
    fetch()
  }, [])

  return (
    <>{expense && <DetailExpenseComponent {...props} expense={expense} />}</>
  )
}

export default HalamanDetailPengeluaran
