//Catatan Pengeluaran Umum Containers
import React from 'react'
import { AuthConsumer } from '../Authentication'
import queryString from 'query-string'

import MenuCatatanPengeluaranUmumComponent from '../../MenuCatatanPengeluaranUmum/menu-catatan-pengeluaran-umum.js'

import Table from '../components/Table/Table'
import TableRow from '../components/Table/TableRow'
import TableCell from '../components/Table/TableCell'
import TableBody from '../components/Table/TableBody'
import TableHead from '../components/Table/TableHead'

import TableExpenseGeneralComponent from '../../TableExpenseGeneral/table-expense-general.js'

class CatatanPengeluaranUmum extends React.Component {
  state = {}

  componentDidMount = async () => {
    this.setState({
      jsonExpenseUmum: JSON.parse(
        queryString.parse(this.props.location.search).jsonExpenseUmum
      ),
    })
  }

  render() {
    return (
      <React.Fragment>
        <AuthConsumer>
          {values => {
            return <MenuCatatanPengeluaranUmumComponent {...values} />
          }}
        </AuthConsumer>
        <Table
          id="table-TableExpenseGeneralComponent"
          variant={{
            headBgColor: 'blue',
            headColor: 'white',
            headTextAlign: 'center',
            headBorderColor: 'blue',
            headUppercase: 'yes',
            headFontWeight: '700',
            bodyBorderColor: 'black',
            bodyFontWeight: 'normal',
            evenBgColor: 'lightgray',
            evenColor: 'black',
            evenBorderColor: 'black',
            distinctBgColor: 'lightblue',
            distinctColor: 'black',
          }}
        >
          <TableHead>
            <TableRow>
              {/* //Data Binding Expense General Row*/}
              <TableCell id="Tanggal">Tanggal</TableCell>
              <TableCell id="Program">Program</TableCell>
              <TableCell id="Kategori">Kategori</TableCell>
              <TableCell id="Jumlah">Jumlah</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.jsonExpenseUmum &&
              this.state.jsonExpenseUmum.map(jsonExpenseUmum => (
                <TableExpenseGeneralComponent
                  {...this.props}
                  jsonExpenseUmum={jsonExpenseUmum}
                />
              ))}
          </TableBody>
        </Table>
      </React.Fragment>
    )
  }
}

export default CatatanPengeluaranUmum
