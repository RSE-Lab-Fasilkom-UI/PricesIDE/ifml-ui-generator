//Halaman Ubah Pengeluaran Containers
import React, { useEffect, useState } from 'react'

import CallprogramlistService from 'programs/services/call-program-list.service'
import CallchartofaccountlistService from 'journalReport/services/call-chart-of-account-list.service'
import FormUbahExpense from '../components/FormUbahExpense'
import CallexpensedetailService from 'expense/services/call-expense-detail.service.js'
import { useSearchParams } from 'react-router-dom'

const HalamanUbahPengeluaran = props => {
  const [searchParams] = useSearchParams()
  const id = searchParams.get('id')

  const [expense, setExpense] = useState()
  const [programs, setPrograms] = useState()
  const [chartOfAccounts, setChartOfAccounts] = useState()

  useEffect(() => {
    const fetch = async () => {
      const { data: expenseDetail } = await CallexpensedetailService.call({
        id,
      })
      const { data: programList } = await CallprogramlistService.call()
      const { data: coa } = await CallchartofaccountlistService.call()
      setExpense(expenseDetail.data)
      setPrograms(programList.data)
      setChartOfAccounts(coa.data)
    }
    fetch()
  }, [])

  return (
    <>
      {expense && programs && chartOfAccounts && (
        <FormUbahExpense
          {...props}
          expense={expense}
          programs={programs}
          chartOfAccounts={chartOfAccounts}
        />
      )}
    </>
  )
}

export default HalamanUbahPengeluaran
