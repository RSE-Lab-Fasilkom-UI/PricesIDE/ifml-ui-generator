import axios from 'axios'
import Token from 'commons/utils/token'
import environment from 'commons/utils/environment'

class CallexpensesaveService {
  static call = async (params = {}) => {
    const token = new Token().get()
    params = Object.assign(params, {
      token,
    })

    const encodedData = `token=${token}`

    try {
      const response = await axios.post(
        `${environment.rootApi}/call/expense/save?${encodedData}`,
        params
      )

      return response
    } catch (e) {
      return {}
    }
  }
}

export default CallexpensesaveService
