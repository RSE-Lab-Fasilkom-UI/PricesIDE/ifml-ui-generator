import {
  StaticPageBuilder,
  StaticPageList,
  StaticPageDetail,
  StaticPageEdit,
} from '.'

const defaultPageRoutes = [
  { path: '/about', element: <StaticPageDetail slug="about" /> },
  { path: '/contact', element: <StaticPageDetail slug="contact" /> },
  { path: '/partners', element: <StaticPageDetail slug="partners" /> },
  { path: '/maps', element: <StaticPageDetail slug="maps" /> },
  {
    path: '/donation/accounts',
    element: <StaticPageDetail slug="bank-account" />,
  },
]

const path = '/static-page'

const staticPageRoutes = [
  ...defaultPageRoutes,
  { path: path + '/create', element: <StaticPageBuilder /> },
  { path: path + '/list', element: <StaticPageList /> },
  { path: path + '/detail/:staticPageId', element: <StaticPageDetail /> },
  { path: path + '/edit/:staticPageId', element: <StaticPageEdit /> },
]

export default staticPageRoutes
