import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const StaticPageList = () => {
  const [staticDatas, setStaticDatas] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`http://localhost:3003/static-data`);
        if (response.data.length !== 0) {
          setStaticDatas(response.data);
        }
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
        setIsError(true);
        setErrorMessage(
          e?.response?.statusText || 'Failed, please try again later!',
        );
      }
    };

    fetchData();
  }, []);

  if (isLoading) return <p>Loading....</p>;

  if (isError) return <p>Error - {errorMessage}</p>;

  return (
    <div style={{ marginTop: 40 }}>
      {staticDatas.map(data => {
        return (
          <Link
            key={data.id}
            to={`/static-page/detail/${data.id}`}
            style={{
              margin: 10,
              display: 'block',
            }}
          >
            {data.id}
          </Link>
        );
      })}
    </div>
  );
};

export default StaticPageList;
