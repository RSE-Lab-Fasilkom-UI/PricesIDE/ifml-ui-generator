export { default as StaticPageBuilder } from './StaticPageBuilder';
export { default as StaticPageDetail } from './StaticPageDetail';
export { default as StaticPageEdit } from './StaticPageEdit';
export { default as StaticPageList } from './StaticPageList';
