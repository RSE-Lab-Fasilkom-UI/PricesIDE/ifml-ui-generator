//Halaman Konfirmasi Donasi Offline Containers
import React, { useEffect, useState } from 'react'

import CallprogramlistService from 'programs/services/call-program-list.service'
import KonfirmasiDonasi from 'donation/components/ConfirmedDonation'
import FormConfirmDonation from 'donation/components/FormConfirmDonation'

const HalamanKonfirmasiDonasiOffline = props => {
  const [isConfirmed, setIsConfirmed] = useState(false)
  const [donation, setDonation] = useState()
  const [programs, setPrograms] = useState()

  useEffect(() => {
    if (!isConfirmed) {
      const fetch = async () => {
        const { data: programList } = await CallprogramlistService.call()
        setPrograms(programList.data)
      }
      fetch()
    }
  }, [])

  const onSubmit = data => {
    setIsConfirmed(true)
    setDonation(data)
  }

  return (
    <>
      {isConfirmed && donation ? (
        <KonfirmasiDonasi {...props} donation={donation} />
      ) : (
        programs && (
          <FormConfirmDonation
            {...props}
            programs={programs}
            onSubmit={onSubmit}
          />
        )
      )}
    </>
  )
}

export default HalamanKonfirmasiDonasiOffline
