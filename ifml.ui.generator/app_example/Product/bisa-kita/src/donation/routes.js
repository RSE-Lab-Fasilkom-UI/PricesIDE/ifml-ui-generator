import { withAuth } from 'commons/auth'
import HalamanKonfirmasiDonasiOffline from './containers/halaman-konfirmasi-donasi-offline'

const path = '/donation'

const donationRoutes = [
  {
    path: path + '/confirm',
    element: withAuth(HalamanKonfirmasiDonasiOffline),
  },
]

export default donationRoutes
