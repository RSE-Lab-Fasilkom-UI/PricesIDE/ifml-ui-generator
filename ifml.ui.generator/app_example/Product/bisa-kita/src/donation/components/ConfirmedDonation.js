import React from 'react'

import Detail from 'commons/components/Detail/Detail'
import VisualizationAttr from 'commons/components/VisualizationAttr/VisualizationAttr'

const KonfirmasiDonasi = ({ donation }) => {
  console.log(
    '🚀 ~ file: ConfirmedDonation.js ~ line 7 ~ KonfirmasiDonasi ~ donation',
    donation
  )

  return (
    <Detail
      variant={{ shape: 'default', borderColor: 'blue', bgColor: 'white' }}
    >
      {/* //Data Binding Donation Data*/}
      <VisualizationAttr
        title_name="Donasi Berhasil! Berikut Kode Donasi Anda"
        content={donation.id}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Donatur"
        content={donation.name}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Jumlah Donasi"
        content={donation.amount}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Tanggal Bayar"
        content={donation.date}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Bukti Transfer"
        content={donation.proofOfTransferUrl}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <VisualizationAttr
        title_name="Nama Program"
        content={donation.programName}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
    </Detail>
  )
}

export default KonfirmasiDonasi
