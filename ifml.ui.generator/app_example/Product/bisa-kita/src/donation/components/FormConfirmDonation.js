import React from 'react'
import InputField from 'commons/components/InputField/InputField'
import SelectionField from 'commons/components/SelectionField/SelectionField'
import Form from 'commons/components/Form/Form'
import Button from 'commons/components/Button/Button'
import CallconfirmofflinedonationsaveService from '../services/call-confirm-offline-donation-save.service'
import { Controller, useForm } from 'react-hook-form'

const FormConfirmDonation = ({ programs, onSubmit }) => {
  const { control, handleSubmit } = useForm()
  const kirim = async data => {
    const {
      data: confirmedDonation,
    } = await CallconfirmofflinedonationsaveService.call(data)
    onSubmit(confirmedDonation.data)
  }

  return (
    <Form
      title="Konfirmasi Donasi Offline"
      id_name="konfirmasi-donasi-offline"
      onSubmit={handleSubmit(kirim)}
      variant={{
        shape: 'default',
        color: 'blue',
        borderColor: 'transparent',
        bgColor: 'white',
        alignment: 'center',
      }}
    >
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Donatur"
            placeholder="Fill the Nama Donatur"
            {...field}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        render={({ field }) => (
          <InputField
            camel_name="emailInput"
            type=""
            dasherized="input-email"
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Email"
            placeholder="Fill the Email"
            {...field}
          />
        )}
      />
      <Controller
        name="phonenumber"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nomor Telepon"
            placeholder="Fill the Nomor Telepon"
            {...field}
          />
        )}
      />
      <Controller
        name="idprogram"
        control={control}
        render={({ field }) => (
          <SelectionField
            options={programs}
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Program Terkait"
            placeholder="Fill the Nama Program Terkait"
            {...field}
          />
        )}
      />
      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Jumlah Donasi"
            placeholder="Fill the Jumlah Donasi"
            {...field}
          />
        )}
      />
      <Controller
        name="transferdate"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Tanggal Transfer"
            placeholder="Fill the Tanggal Transfer"
            {...field}
          />
        )}
      />
      <Controller
        name="paymentmethod"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Metode Pembayaran"
            placeholder="Fill the Metode Pembayaran"
            {...field}
          />
        )}
      />
      <Controller
        name="proofoftransferurl"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="URL Bukti Transfer"
            placeholder="Fill the URL Bukti Transfer"
            {...field}
          />
        )}
      />
      <Controller
        name="message"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Keterangan"
            placeholder="Fill the Keterangan"
            {...field}
          />
        )}
      />
      <Button
        type="submit"
        variant={{
          shape: 'default',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
        }}
        text="kirim"
      />
    </Form>
  )
}

export default FormConfirmDonation
