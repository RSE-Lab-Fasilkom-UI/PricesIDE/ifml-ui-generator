import { useRoutes } from 'react-router-dom'

import programRoutes from './programs/routes'
import commonRoutes from 'commons/routes.js'
import incomeRoutes from 'income/routes.js'
import expenseRoutes from 'expense/routes.js'
import reportRoutes from 'journalReport/routes.js'
import donationRoutes from 'donation/routes'
import staticPageRoutes from 'staticPage/routes'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...programRoutes,
    ...incomeRoutes,
    ...expenseRoutes,
    ...reportRoutes,
    ...donationRoutes,
    ...commonRoutes,
    ...staticPageRoutes,
  ])

  return router

  // return (
  //   <Router>
  //     <>
  //       <Routes>
  //
  //         {/* COA */}
  //         <Route
  //           exact
  //           path="/laporan-arus-kas"
  //           component={withAuth(LaporanArusKasComponent)}
  //         />
  //
  //         {/* Donation */}
  //         <Route
  //           exact
  //           path="/halaman-berhasil-Konfirmasi"
  //           component={withAuth(HalamanBerhasilKonfirmasiComponent)}
  //         />
  //         <Route
  //           exact
  //           path="/halaman-konfirmasi-donasi-offline"
  //           component={withAuth(HalamanKonfirmasiDonasiOfflineComponent)}
  //         />
  //         {/*Periode */}
  //         <Route
  //           exact
  //           path="/laporan-arus-kas-tahun-anggaran"
  //           component={withAuth(LaporanArusKasTahunAnggaranComponent)}
  //         />
  //         <Route
  //           exact
  //           path="/daftar-tahun-anggaran"
  //           component={withAuth(DaftarTahunAnggaranComponent)}
  //         />
  //         <Route exact path="/login" component={LoginComponent} />
  //         <Route exact path="/" component={LandingPageComponent} />
  //       </Routes>
  //     </>
  //   </Router>
  // )
}

export default GlobalRoutes
