import axios from 'axios'
import Token from 'commons/utils/token'
import environment from 'commons/utils/environment'

class CallautomaticreportperiodicmodellistService {
  static call = async (params = {}) => {
    const token = new Token().get()
    params = Object.assign(params, {
      token,
    })

    const encodedData = Object.keys(params)
      .map(thekey => `${thekey}=${encodeURI(params[thekey])}`)
      .join('&')

    try {
      const response = await axios.get(
        `${environment.rootApi}/call/automatic-report-periodic-model/list?${encodedData}`
      )

      return response
    } catch (e) {
      return {}
    }
  }
}

export default CallautomaticreportperiodicmodellistService
