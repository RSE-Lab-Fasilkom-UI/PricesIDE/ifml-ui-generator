//Daftar Tahun Anggaran Containers
import React, { useEffect, useState } from 'react'
import queryString from 'query-string'

import List from 'commons/components/List/List'

import ListTahunAnggaranComponent from '../components/ListTahunAnggaran.js'
import CallautomaticreportperiodicmodellistService from 'journalReport/services/call-automatic-report-periodic-model-list.service.js'

const DaftarTahunAnggaran = props => {
  const [periods, setPeriods] = useState()
  console.log(
    '🚀 ~ file: daftar-tahun-anggaran.js ~ line 12 ~ DaftarTahunAnggaran ~ periods',
    periods
  )

  useEffect(() => {
    const fetch = async () => {
      const {
        data: periodList,
      } = await CallautomaticreportperiodicmodellistService.call()
      setPeriods(periodList.data)
    }
    fetch()
  }, [])

  return (
    <div className="p-12">
      <List
        variant={{
          borderColor: 'blue',
          bgColor: 'white',
          shape: 'default',
          alignment: 'left',
        }}
        id_name="list-ListTahunAnggaranComponent"
        className="list-component view-component"
        column="3"
      >
        {periods &&
          periods.map(period => (
            <ListTahunAnggaranComponent
              key={period.id}
              period={period}
              {...props}
            />
          ))}
      </List>
    </div>
  )
}

export default DaftarTahunAnggaran
