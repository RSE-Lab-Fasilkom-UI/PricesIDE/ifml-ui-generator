//Laporan Arus Kas Tahun Anggaran Containers
import React, { useEffect, useState } from 'react'

import Table from 'commons/components/Table/Table'

import TableChartOfAccountEntry from '../components/TableChartOfAccountEntry.js'
import CallautomaticreportperiodiclistService from 'journalReport/services/call-automatic-report-periodic-list.service.js'
import { useParams, useSearchParams } from 'react-router-dom'

const LaporanArusKasTahunAnggaran = props => {
  const [entries, setEntries] = useState()
  const [searchParams] = useSearchParams()

  const { id } = useParams()
  const year = searchParams.get('year')

  useEffect(() => {
    const fetch = async () => {
      const {
        data: periodEntries,
      } = await CallautomaticreportperiodiclistService.call({
        id,
      })
      setEntries(periodEntries.data)
    }
    fetch()
  }, [])

  return (
    <div className="mx-auto max-w-screen-md px-6 py-12">
      <div class="prose max-w-full mb-6">
        <h2>
          Laporan Arus Kas 1 Januari {year} - 31 Desember {year}
        </h2>
      </div>
      <Table compact id="table-TableChartofAccountEntryPeriodicComponent">
        {entries && <TableChartOfAccountEntry {...{ entries }} />}
      </Table>
    </div>
  )
}

export default LaporanArusKasTahunAnggaran
