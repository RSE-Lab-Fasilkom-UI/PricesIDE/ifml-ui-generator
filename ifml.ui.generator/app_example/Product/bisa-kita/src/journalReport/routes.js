import { withAuth } from 'commons/auth'

import LaporanArusKas from './containers/laporan-arus-kas'
import DaftarTahunAnggaran from './containers/daftar-tahun-anggaran'
import LaporanArusKasTahunAnggaran from './containers/laporan-arus-kas-tahun-anggaran.js'

const path = '/report'

const reportRoutes = [
  {
    path: path + '/arus-kas',
    element: withAuth(LaporanArusKas),
  },
  {
    path: path + '/summary',
    element: withAuth(DaftarTahunAnggaran),
  },
  {
    path: path + '/summary/:id',
    element: withAuth(LaporanArusKasTahunAnggaran),
  },
]

export default reportRoutes
