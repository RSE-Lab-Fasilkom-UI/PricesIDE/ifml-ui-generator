//Table
import React, { useRef } from 'react'
import { TableBody, TableHead, TableRow, TableCell } from 'commons/components'

const TableChartOfAccountEntry = ({ entries }) => {
  const DISTINCT_ROW = [
    'Aktivitas Operasional',
    'Aktivitas Investasi',
    'Aktivitas Pendanaan',
    '',
  ]
  const tableBody = useRef([])
  return entries.map((row, index) => {
    if (DISTINCT_ROW.includes(row.name)) {
      const tBody = [...tableBody.current]
      tableBody.current = []
      return (
        <>
          {tBody.length !== 0 && (
            <TableBody>
              {tBody.map(account => (
                <TableRow>
                  <TableCell>
                    <span
                      style={{ paddingLeft: `${parseInt(account.level)}rem` }}
                    >
                      {account.name}
                    </span>
                  </TableCell>
                  <TableCell isCurrency={!!account.amount}>
                    {account.amount}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          )}
          <TableHead>
            <TableRow>
              <TableCell level={row.level}>
                <span style={{ paddingLeft: `${parseInt(row.level)}rem` }}>
                  {row.name}
                </span>
              </TableCell>
              <TableCell>{row.amount}</TableCell>
            </TableRow>
          </TableHead>
        </>
      )
    } else {
      tableBody.current.push(row)
      if (index === entries.length - 1)
        return (
          tableBody.current.length !== 0 && (
            <TableBody>
              {tableBody.current.map(account => (
                <TableRow>
                  <TableCell>
                    <span
                      style={{ paddingLeft: `${parseInt(account.level)}rem` }}
                    >
                      {account.name}
                    </span>
                  </TableCell>
                  <TableCell isCurrency>{account.amount ?? 0}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          )
        )
    }
  })
}

export default TableChartOfAccountEntry
