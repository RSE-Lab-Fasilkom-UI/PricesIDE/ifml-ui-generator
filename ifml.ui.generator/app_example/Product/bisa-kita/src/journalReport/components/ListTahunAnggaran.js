//List
import React from 'react'
import Button from 'commons/components/Button/Button'
import ListItem from 'commons/components/ListItem/ListItem'
import VisualizationAttr from 'commons/components/VisualizationAttr/VisualizationAttr'
import { Link, useLocation } from 'react-router-dom'

const ListTahunAnggaran = ({ period }) => {
  const { pathname } = useLocation()

  return (
    <ListItem
      variant={{
        borderColor: 'blue',
        bgColor: 'white',
        shape: 'default',
        alignment: 'left',
      }}
    >
      {/* //Data Binding Periode List Element*/}
      <VisualizationAttr
        title_name="Laporan Keuangan Tahun Anggaran"
        content={period.name}
        variant={{
          shape: 'default',
          color: 'black',
          titleColor: 'blue',
          uppercase: 'no',
        }}
      />
      <Link to={`${pathname}/${period.id}?year=${period.name}`}>
        <Button
          variant={{
            shape: 'default',
            color: 'white',
            bgColor: 'blue',
            borderColor: 'blue',
          }}
          text="Lihat"
        />
      </Link>
    </ListItem>
  )
}

export default ListTahunAnggaran
