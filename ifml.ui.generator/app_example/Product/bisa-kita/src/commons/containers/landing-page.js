import { About, Footer, Hero } from 'commons/components'
import React from 'react'
import ApiadminStaticService from 'commons/services/apiadmin-static.service'
import ContactService from 'commons/services/contact.service'
import PageService from 'commons/services/page.service'
import style from '../styles/css_modules/static-page.module.css'

const LandingPage = () => {
  const DUMMY_BANNER =
    'https://images.unsplash.com/photo-1488521787991-ed7bbaae773c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'

  return (
    <div className="landing-page">
      <Hero banner={DUMMY_BANNER} />
      <Footer />
      {/* <About>
        <div className={style.box}>
          <h1 className={`${style.heading_main} ${style.None}`}>About Us</h1>
          <h3 className={`${style.heading_categories} ${style.None}`}>
            {this.state?.about}
          </h3>
          <p className={`${style.writing_content} ${style.None}`}>
            {this.state?.description}
          </p>
        </div>
      </About>
      <Footer>
        <div className="contact">
          <div className={style.box}>
            <h1 className={`${style.heading_main} ${style.None}`}>
              Contact Us
            </h1>

            <h3 className={`${style.heading_categories} ${style.None}`}>
              Address
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state?.address}
            </p>

            <h3 className={`${style.heading_categories} ${style.None}`}>
              Email
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state?.email}
            </p>

            <h3 className={`${style.heading_categories} ${style.None}`}>
              Phone
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state?.phone}
            </p>
          </div>
        </div>
        <div className="sitemap">
          <div className={style.box}>
            <h4 className={`${style.heading_categories} ${style.None}`}>
              Sitemap
            </h4>
            <p className={`${style.writing_content} ${style.None}`}>
              <a href={this.state?.fb_url} className={style.links}>
                Facebook page
              </a>
            </p>
            <p className={`${style.writing_content} ${style.None}`}>
              <a href={this.state?.map_url} className={style.links}>
                Location on map
              </a>
            </p>
            <p className={`${style.writing_content} ${style.None}`}>
              <a href={this.state?.instagram_url} className={style.links}>
                Instagram page
              </a>
            </p>
          </div>
        </div>
      </Footer> */}
    </div>
  )
}

export default LandingPage
