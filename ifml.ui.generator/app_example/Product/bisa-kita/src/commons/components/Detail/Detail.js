import React from 'react'
import { DetailStyle } from './style'

const Detail = ({ children }) => {
  return (
    <div className="mx-auto w-full max-w-screen-lg bg-white shadow-xl rounded-box p-8 m-8">
      {children}
    </div>
  )
}

export default Detail
