import React from 'react'

const TableRow = props => {
  const { onClick } = props

  const onClickfunc = onClick ? onClick : undefined

  return (
    <tr onClick={onClickfunc} highlight>
      {props.children}
    </tr>
  )
}

export default TableRow
