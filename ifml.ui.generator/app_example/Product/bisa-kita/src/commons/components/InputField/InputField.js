import React, { forwardRef } from 'react'

const InputField = forwardRef(function InputField(props, ref) {
  const { label, variant } = props

  return (
    <div className="form-control" {...variant}>
      {label && <label className="label label-text">{label}</label>}
      <input
        className="input input-bordered"
        ref={ref}
        type="text"
        {...props}
        {...variant}
      />
    </div>
  )
})

export default InputField
