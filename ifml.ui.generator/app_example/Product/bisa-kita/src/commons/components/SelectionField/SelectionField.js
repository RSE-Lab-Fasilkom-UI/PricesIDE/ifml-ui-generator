import React, { forwardRef } from 'react'

const SelectionField = forwardRef((props, ref) => {
  const { label, variant, options, placeholder } = props

  return (
    <div className="form-control" {...variant}>
      {{ label } && <label className="label label-text">{label}</label>}
      <select
        className="select select-bordered"
        ref={ref}
        {...props}
        {...variant}
      >
        <option disabled selected hidden>
          {placeholder}
        </option>
        {options.map(option => (
          <option value={option.id}>{option.name}</option>
        ))}
      </select>
    </div>
  )
})

export default SelectionField
