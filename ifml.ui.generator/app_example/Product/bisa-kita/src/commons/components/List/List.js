import React from 'react'
import { ListStyle } from './style'

const List = props => {
  const { id_name, className, column } = props

  return (
    <ul
      className={`grid grid-cols-3 w-full px-6 pb-12 mx-auto max-w-screen-xl gap-6 ${className}`}
      style={{ gridTemplateColumns: `repeat(${column}, minmax(0, 1fr))` }}
      id={id_name}
      column={column}
    >
      {props.children}
    </ul>
  )
}

export default List
