import styled from 'styled-components'

export const ListStyle = styled.ul`
  list-style-type: none;
  position: relative;
  column-count: ${props => (props.column ? props.column : 1)};
`
