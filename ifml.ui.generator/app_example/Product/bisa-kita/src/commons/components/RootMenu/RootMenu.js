import React from 'react'
import { Link } from 'react-router-dom'
import MenuLink from '../MenuLink/MenuLink'
import { FiLogOut, FiMenu } from 'react-icons/fi'
import MenuItem from '../MenuItem/MenuItem'
import MenuChildren from '../MenuChildren/MenuChildren'
import { GoChevronDown } from 'react-icons/go'
import Brand from '../Brand'

const Menu = ({ menu, isFirstLevel }) => {
  return (
    <MenuItem>
      <MenuLink to={menu.route}>
        {menu.label}
        {menu?.subMenus && <GoChevronDown />}
      </MenuLink>
      {menu?.subMenus && (
        <MenuChildren isFirstLevel={isFirstLevel}>
          {menu.subMenus.map(subMenu => (
            <Menu key={subMenu.label} menu={subMenu} />
          ))}
        </MenuChildren>
      )}
    </MenuItem>
  )
}

const RootMenu = ({ isAuth, logout, menus, toggleSidebar }) => {
  return (
    <nav className="sticky top-0 navbar justify-between w-full py-0 px-4 bg-primary text-primary-content z-10 shadow-xl">
      <Brand />
      <ul className="menu menu-horizontal rounded-box p-2 hidden sm:flex">
        {isAuth ? (
          menus.map(menu => <Menu key={menu.label} menu={menu} isFirstLevel />)
        ) : (
          <>
            {menus.slice(0, 1).map(menu => (
              <Menu key={menu.label} menu={menu} isFirstLevel />
            ))}
            <MenuItem>
              <Link
                to={'/login'}
                className="btn btn-primary bg-base-100 text-base-content hover:text-base-primary"
              >
                Login
              </Link>
            </MenuItem>
          </>
        )}
      </ul>
      {isAuth && (
        <button
          className="btn btn-ghost items-center gap-2 text-primary-content normal-case hidden sm:inline-flex"
          onClick={logout}
        >
          <FiLogOut className="w-5 h-5" />
          Keluar
        </button>
      )}
      <div className="flex-none sm:hidden">
        <button onClick={toggleSidebar} className="btn btn-square btn-ghost">
          <FiMenu />
        </button>
      </div>
    </nav>
  )
}

export default RootMenu
