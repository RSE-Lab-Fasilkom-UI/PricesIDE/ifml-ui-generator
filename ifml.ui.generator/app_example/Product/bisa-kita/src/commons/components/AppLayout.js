import menus from 'menus'
import React, { Fragment, useState } from 'react'
import { FiLogOut } from 'react-icons/fi'
import { GoChevronDown } from 'react-icons/go'
import { Link } from 'react-router-dom'
import Brand from './Brand'
import MenuChildren from './MenuChildren/MenuChildren'
import MenuItem from './MenuItem/MenuItem'
import MenuLink from './MenuLink/MenuLink'
import RootMenu from './RootMenu/RootMenu'

const SidebarMenu = ({ menu, onClick }) => {
  if (menu?.subMenus) {
    return (
      <div tabIndex={0} className="collapse collapse-arrow">
        <input type="checkbox" />
        <MenuLink to={menu.route} className="collapse-title">
          {menu.label}
        </MenuLink>
        <ul className="collapse-content menu">
          {menu.subMenus.map(subMenu => (
            <SidebarMenu key={subMenu.label} menu={subMenu} onClick={onClick} />
          ))}
        </ul>
      </div>
    )
  }
  return (
    <MenuItem>
      <MenuLink to={menu.route} onClick={onClick}>
        {menu.label}
      </MenuLink>
    </MenuItem>
  )
}

const AppLayout = ({ isAuth, logout, children }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const toggleSidebar = () => setIsSidebarOpen(!isSidebarOpen)

  return (
    <div className="drawer">
      <input
        type="checkbox"
        checked={isSidebarOpen}
        onChange={setIsSidebarOpen}
        className="drawer-toggle"
      />
      <div className="drawer-content min-h-screen flex flex-col">
        <RootMenu {...{ isAuth, logout, menus, toggleSidebar }} />
        <div className="flex-1">{children}</div>
      </div>
      <div className="drawer-side">
        <div onClick={toggleSidebar} className="drawer-overlay"></div>
        <div className="overflow-y-auto flex flex-col relative h-screen w-80 bg-base-100">
          <div
            onClick={toggleSidebar}
            className="pt-4 px-4 pb-3 border-b sticky top-0 bg-base-100 z-[1]"
          >
            <Brand />
          </div>
          <ul className="menu flex-1 p-4">
            {isAuth
              ? menus.map(menu => (
                  <SidebarMenu
                    key={menu.label}
                    menu={menu}
                    onClick={toggleSidebar}
                  />
                ))
              : menus
                  .slice(0, 1)
                  .map(menu => <SidebarMenu key={menu.label} menu={menu} />)}
          </ul>
          {isAuth ? (
            <div class="p-4 sticky bottom-0 bg-base-100">
              <button
                className="btn btn-error btn-outline items-center w-full gap-2 text-primary-content normal-case"
                onClick={logout}
              >
                <FiLogOut className="w-5 h-5" />
                Keluar
              </button>
            </div>
          ) : (
            <div className="sticky bg-base-100 bottom-0">
              <Link
                to="/login"
                onClick={toggleSidebar}
                className="btn btn-primary w-full"
              >
                <label htmlFor="drawer-toggle">Login</label>
              </Link>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}

export default AppLayout
