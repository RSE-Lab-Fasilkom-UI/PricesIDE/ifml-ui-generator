import styled from 'styled-components'

export const VisualizationDiv = styled.div`
  .visualization_label {
    font-weight: bold;
    font-size: 16px;
  }

  .visual_label {
    color: ${props =>
      props.titleColor
        ? props.titleColor
        : props.color
        ? props.color
        : 'black'};
    text-transform: ${props =>
      props.uppercase === 'yes' ? 'uppercase' : 'none'};
  }

  .visualization_content {
    color: ${props => (props.color ? props.color : 'black')};

    img {
      max-width: 100%;
      border-radius: 8px;
    }
  }
`
