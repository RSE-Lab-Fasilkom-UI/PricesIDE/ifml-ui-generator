import React from 'react'
import InputField from 'commons/components/InputField/InputField'
import Form from 'commons/components/Form/Form'
import Button from 'commons/components/Button/Button'
import { useForm, Controller } from 'react-hook-form'

import CallprogramupdateService from '../services/call-program-update.service'
import { useNavigate } from 'react-router-dom'
import cleanFormData from 'commons/utils/cleanFormData'

const FormUbahProgram = ({ program }) => {
  const { control, handleSubmit } = useForm({ defaultValues: program })
  const navigate = useNavigate()

  const Kirim = async data => {
    const cleanData = cleanFormData(data)
    await CallprogramupdateService.call(cleanData)
    navigate(`/programs/${program.id}`)
  }

  return (
    <Form
      title="Ubah Data Program"
      id_name="ubah-data-program"
      onSubmit={handleSubmit(Kirim)}
      variant={{
        shape: 'default',
        color: 'blue',
        borderColor: 'transparent',
        bgColor: 'white',
        alignment: 'center',
      }}
    >
      <Controller
        name="id"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="ID"
            placeholder="Fill the id"
            defaultValue={program.id}
            {...field}
          />
        )}
      />

      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Program"
            placeholder="Fill the Nama Program"
            defaultValue={program.name}
            {...field}
          />
        )}
      />
      <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Deskripsi"
            placeholder="Fill the Deskripsi"
            defaultValue={program.description}
            {...field}
          />
        )}
      />
      <Controller
        name="target"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Target"
            placeholder="Fill the Target"
            defaultValue={program.target}
            {...field}
          />
        )}
      />
      <Controller
        name="partner"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Partner"
            placeholder="Fill the partner"
            defaultValue={program.partner}
            {...field}
          />
        )}
      />
      <Controller
        name="logoUrl"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Url Gambar Program"
            placeholder="Fill the Url Gambar Program"
            defaultValue={program.logoUrl}
            {...field}
          />
        )}
      />
      <Button
        type="submit"
        variant={{
          shape: 'default',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
        }}
        text="Kirim"
      />
    </Form>
  )
}

export default FormUbahProgram
