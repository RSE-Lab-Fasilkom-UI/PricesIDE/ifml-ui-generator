import React from 'react'
import InputField from '../../commons/components/InputField/InputField'
import Form from '../../commons/components/Form/Form'
import Button from '../../commons/components/Button/Button'
import CallprogramsaveService from '../services/call-program-save.service'
import { useNavigate } from 'react-router-dom'
import { useForm, Controller } from 'react-hook-form'

const FormTambahProgram = props => {
  const { control, handleSubmit } = useForm()
  const navigate = useNavigate()

  const Kirim = async data => {
    await CallprogramsaveService.call(data)
    navigate(`/programs`)
  }

  return (
    <Form
      title="Tambahkan Program"
      id_name="tambahkan-program"
      onSubmit={handleSubmit(Kirim)}
      variant={{
        shape: 'default',
        color: 'blue',
        borderColor: 'transparent',
        bgColor: 'white',
        alignment: 'center',
      }}
    >
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Nama Program"
            placeholder="Fill the Nama Program"
            {...field}
          />
        )}
      />
      <Controller
        name="description"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Deskripsi"
            placeholder="Fill the Deskripsi"
            {...field}
          />
        )}
      />
      <Controller
        name="target"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Target"
            placeholder="Fill the Target"
            {...field}
          />
        )}
      />
      <Controller
        name="partner"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Partner"
            placeholder="Fill the partner"
            {...field}
          />
        )}
      />
      <Controller
        name="logoUrl"
        control={control}
        render={({ field }) => (
          <InputField
            variant={{ shape: 'default', color: 'black', labelColor: 'blue' }}
            label="Url Gambar Program"
            placeholder="Fill the Url Gambar Program"
            {...field}
          />
        )}
      />
      <Button
        type="submit"
        variant={{
          shape: 'default',
          color: 'white',
          bgColor: 'blue',
          borderColor: 'blue',
        }}
        text="Kirim"
      />
    </Form>
  )
}

export default FormTambahProgram
