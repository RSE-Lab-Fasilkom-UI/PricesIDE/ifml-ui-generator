import React from 'react'
import { Link } from 'react-router-dom'

const ProgramFeature = ({ programs }) => {
  return (
    <div className="mx-auto max-w-screen-xl p-6 flex flex-col sm:flex-row justify-center sm:justify-end items-center gap-4">
      <div class="text-gray-400 order-last sm:order-none text-center sm:text-left">
        Terdapat {programs?.length ?? 0} program
      </div>
      <Link className="btn" to="/programs/tambah">
        Tambah Program
      </Link>
    </div>
  )
}

export default ProgramFeature
