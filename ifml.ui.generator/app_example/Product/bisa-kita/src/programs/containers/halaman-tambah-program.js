//Halaman Tambah Program Containers
import React from 'react'

import FormTambahProgram from '../components/FormTambahProgram'

const HalamanTambahProgram = props => {
  return <FormTambahProgram {...props} />
}

export default HalamanTambahProgram
