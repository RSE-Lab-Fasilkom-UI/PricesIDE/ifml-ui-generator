//Halaman Ubah Program Containers
import React, { useEffect, useState } from 'react'

import UbahDataProgramComponent from '../components/FormUbahProgram.js'
import { useSearchParams } from 'react-router-dom'
import CallprogramdetailService from '../services/call-program-detail.service.js'

const HalamanUbahProgram = props => {
  const [searchParams] = useSearchParams()
  const id = searchParams.get('id')
  const [program, setProgram] = useState()

  useEffect(() => {
    const fetchProgram = async () => {
      const { data: programDetail } = await CallprogramdetailService.call({
        id,
      })
      setProgram(programDetail.data)
    }
    fetchProgram()
  }, [])

  return (
    <div className="bg-base-100">
      {program !== undefined && (
        <UbahDataProgramComponent
          {...props}
          program={program}
        ></UbahDataProgramComponent>
      )}
    </div>
  )
}

export default HalamanUbahProgram
