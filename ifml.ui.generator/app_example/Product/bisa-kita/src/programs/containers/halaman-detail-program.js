//Halaman Detail Program Containers
import React, { useEffect, useState } from 'react'

import DetailProgramComponent from '../components/DetailProgram.js'
import { useParams } from 'react-router-dom'
import CallprogramdetailService from '../services/call-program-detail.service.js'

const HalamanDetailProgram = props => {
  const { id } = useParams()
  const [program, setProgram] = useState()

  useEffect(() => {
    const fetchProgram = async () => {
      const { data: programDetail } = await CallprogramdetailService.call({
        id,
      })
      setProgram(programDetail.data)
    }
    fetchProgram()
  }, [])

  return (
    <div className="bg-base-100 p-6">
      {program && <DetailProgramComponent {...props} program={program} />}
    </div>
  )
}

export default HalamanDetailProgram
