import { useRoutes } from 'react-router-dom'

import programRoutes from 'program/routes'
import commonRoutes from 'commons/routes.js'
import incomeRoutes from 'income/routes.js'
import staticPageRoutes from 'staticPage/routes'

const GlobalRoutes = () => {
  const router = useRoutes([
    ...programRoutes,
    ...incomeRoutes,
    ...staticPageRoutes,
    ...commonRoutes,
  ])

  return router
}

export default GlobalRoutes
