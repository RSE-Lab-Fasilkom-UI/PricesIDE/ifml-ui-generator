import LandingPage from './containers/LandingPage'
import LoginPage from './auth/login'

const commonRoutes = [
  { path: '/login', element: <LoginPage /> },
  { path: '/', element: <LandingPage /> },
]

export default commonRoutes
