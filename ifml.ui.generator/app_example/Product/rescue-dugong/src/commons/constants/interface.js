export const INTERFACE_KITS = {
  a: {
    button: {
      primary: 'solid-primary',
      secondary: 'outline-primary',
      tertiary: 'text',
    },
    input: 'outline',
    table: 'bordered',
    typography: 'sans',
  },
  b: {
    button: {
      primary: 'primary',
      secondary: 'secondary',
      tertiary: 'tertiary',
    },
    input: 'bordered',
    table: 'bordered',
    typography: 'sans-serif',
  },
}
export const INTERFACE_KIT = INTERFACE_KITS['a']
