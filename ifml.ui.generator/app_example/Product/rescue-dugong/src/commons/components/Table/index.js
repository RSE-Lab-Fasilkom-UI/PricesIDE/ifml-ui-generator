import React from 'react'

const Table = ({ children, compact }) => {
  return (
    <div className="overflow-x-auto">
      <table
        className={`table w-full ${compact && 'table-compact'} `}
      >
        {children}
      </table>
    </div>
  )
}

export default Table
