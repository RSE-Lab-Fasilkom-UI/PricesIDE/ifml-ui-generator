import useAppearance from 'commons/appearance/useAppearance'
import React, { forwardRef } from 'react'
import { INPUT_CLASSNAMES } from './variants'

const CheckBoxField = forwardRef(function CheckBoxField(props, ref) {
  const { label, className, kit, checked } = props
  const interfaceKit = useAppearance()
  const inputStyle = (kit ?? interfaceKit).input
  const inputVariant = INPUT_CLASSNAMES[inputStyle]

  return (
    <div className="form-control">
      {label && <label className="label label-text">{label}</label>}
      <input 
        type="checkbox"
        checked={checked}
        className={`input ${inputVariant} ${className}`} 
        ref={ref}
        {...props}
      />
    </div>
  )
})

export default CheckBoxField
