import React from 'react'

const VisualizationAttr = ({ label, content, isCurrency }) => {
  const checkIsImage = url => url.match(/\.(jpeg|jpg|gif|png)$/) != null

  const formatCurrency = amount => {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    }).format(amount)
  }

  const isImage = typeof content == 'string' && checkIsImage(content)

  return (
    <div>
      {!isImage && (
        <label className="label">
          <span class="label-text text-sm uppercase font-medium text-gray-400">
            {label}
          </span>
        </label>
      )}
      {isCurrency ? (
        <div>{formatCurrency(content)}</div>
      ) : (
        <div>
          {isImage ? (
            <img
              src={content}
              alt={label}
              className="aspect-[4/3] w-full max-w-xl object-cover object-center rounded-btn mx-auto"
            />
          ) : (
            content
          )}
        </div>
      )}
    </div>
  )
}

export default VisualizationAttr
