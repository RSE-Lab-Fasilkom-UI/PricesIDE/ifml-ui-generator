import React from 'react'
import { Navigate } from 'react-router-dom'
import { AuthConsumer } from '.'
import { Button, InputField } from 'commons/components'

class LoginPage extends React.Component {
  handleLoginWithGoogle = () => {
    this.props.loginGoogle()
  }

  render() {
    if (this.props.isAuth) {
      return <Navigate to="/" />
    }

    return (
      <div className="h-full bg-base-200 grid place-items-center">
        <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-white">
          <div className="card-body">
            <InputField
              name="email"
              type="email"
              label="Email"
              placeholder="Masukkan email"
            />
            <InputField
              name="password"
              type="password"
              label="Password"
              placeholder="Masukkan password"
            />
            <Button variant='primary' className="form-control mt-4">Login</Button>
            <div className="divider">atau</div>
            <Button onClick={this.handleLoginWithGoogle}>Login with Google</Button>
          </div>
        </div>
      </div>
    )
  }
}

const withAuth = props => (
  <AuthConsumer>{values => <LoginPage {...props} {...values} />}</AuthConsumer>
)

export default withAuth
