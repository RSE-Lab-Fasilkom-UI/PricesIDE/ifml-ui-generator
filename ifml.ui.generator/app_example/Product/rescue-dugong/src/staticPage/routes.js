import { withAuth } from 'commons/auth';

import StaticPageDetail from './containers/StaticPageDetail';
import StaticPageEdit from './containers/StaticPageEdit';

const defaultPageRoutes = [
  { path: '/about', element: <StaticPageDetail slug="about" /> },
  { path: '/contact', element: <StaticPageDetail slug="contact" /> },
  { path: '/partners', element: <StaticPageDetail slug="partners" /> },
  { path: '/maps', element: <StaticPageDetail slug="maps" /> },
  {
    path: '/donation/accounts',
    element: <StaticPageDetail slug="bank-account" />,
  },
]

const path = '/static-page'

const staticPageRoutes = [
  ...defaultPageRoutes,
  { path: path + '/detail/:staticPageId', element: <StaticPageDetail /> },
  { path: path + '/edit/:staticPageId', element: withAuth(StaticPageEdit)},
]

export default staticPageRoutes
