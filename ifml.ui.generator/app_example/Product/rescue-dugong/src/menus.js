const menus = [
  {
    route: '#',
    label: 'Info Organisasi',
    subMenus: [
      {
        route: '/about',
        label: 'Tentang Kami',
      },
      {
        route: '/contact',
        label: 'Kontak',
      },
      {
        route: '/partners',
        label: 'Mitra Kami',
      },
      {
        route: '/maps',
        label: 'Lokasi',
      },
    ],
  },
  {
    route: '/programs',
    label: 'Program',
  },
  {
    route: '#',
    label: 'Laporan Keuangan',
    subMenus: [
      {
        route: '/income',
        label: 'Pemasukan ',
      },
      {
        route: '/expense',
        label: 'Pengeluaran',
      },
      {
        route: '#',
        label: 'Jurnal Keuangan',
        subMenus: [
          {
            route: '/report/arus-kas',
            label: 'Laporan Arus Kas',
          },
          {
            route: '/report/summary',
            label: 'Laporan Tahun Anggaran',
          },
        ],
      },
    ],
  },
  {
    route: '#',
    label: 'Donasi',
    subMenus: [
      {
        route: '#',
        label: 'Donasi Online',
        subMenus: [
          {
            route: '/donation/confirm',
            label: 'Konfirmasi Transfer',
          },
        ],
      },
      {
        route: '/donation/accounts',
        label: 'Rekening Donasi',
      },
    ],
  },
]

export default menus
