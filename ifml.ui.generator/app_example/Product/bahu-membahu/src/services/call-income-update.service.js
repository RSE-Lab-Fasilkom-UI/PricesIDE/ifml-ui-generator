import axios from 'axios';
import Token from '../utils/token';
import environment from '../utils/environment';


class CallincomeupdateService {

    static call = async (params = {}) => {

        const token = new Token().get();
        params = Object.assign(params, {
            token
        });
	
		const encodedData = `token=${token}`;

        try {
            const response = await axios.put(`${environment.rootApi}/call/income/update?${encodedData}`, params);

            return response;
        } catch (e) {
            return {};
        }
    };
}

export default CallincomeupdateService;
