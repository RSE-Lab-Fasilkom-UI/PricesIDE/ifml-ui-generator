//Halaman Tambah Komunitas Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import TambahkanKomunitasComponent from '../TambahkanKomunitas/tambahkan-komunitas.js'

class HalamanTambahKomunitas extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			nama:JSON.parse(queryString.parse(this.props.location.search).nama),
		});
	};

	
	render() {
		return (
			<React.Fragment>
					{
					<TambahkanKomunitasComponent {...this.props}
					></TambahkanKomunitasComponent>
					}
					
			</React.Fragment>
		);
	}
}

export default HalamanTambahKomunitas;

