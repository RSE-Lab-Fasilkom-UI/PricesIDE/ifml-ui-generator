//Halaman Tambah Volunteer Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import TambahkanVolunteerComponent from '../TambahkanVolunteer/tambahkan-volunteer.js'

class HalamanTambahVolunteer extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			nama:JSON.parse(queryString.parse(this.props.location.search).nama),
			nomorTelepon:JSON.parse(queryString.parse(this.props.location.search).nomorTelepon),
			email:JSON.parse(queryString.parse(this.props.location.search).email),
		});
	};

	
	render() {
		return (
			<React.Fragment>
					{
					<TambahkanVolunteerComponent {...this.props}
					></TambahkanVolunteerComponent>
					}
					
			</React.Fragment>
		);
	}
}

export default HalamanTambahVolunteer;

