import React from 'react';
import queryString from 'query-string';
import Button from '../components/Button/Button';
import Detail from '../components/Detail/Detail';
import VisualizationAttr from '../components/VisualizationAttr/VisualizationAttr';
import CallprogramdeleteService from '../services/call-program-delete.service'
class DetailProgram extends React.Component {
	state = {}
	
	componentWillMount = () => {
		this.programData = this.props.objectDetailProgram;this.id = String(this.programData.id);
	};
	
	Hapus = async () => {
		const data = await CallprogramdeleteService.call({
			id: this.id,
		});
		
		this.props.history.push({
			pathname: '/daftar-program',
            search: queryString.stringify({
                jsonAllProgram: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
	}
	Ubah = async () => {
		this.props.history.push({
			pathname: '/halaman-ubah-program',
            search: queryString.stringify({
                objectEditProgram: JSON.stringify(this.programData),
            })
		})
	}
	render() {
		return (
			<Detail variant={{'shape': 'default', 'borderColor': 'blue', 'bgColor': 'white'}}>
				{/* //Data Binding Program Data*/}
				<VisualizationAttr title_name="Nama" content={ this.programData && this.programData.name } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Deskripsi" content={ this.programData && this.programData.description } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Target" content={ this.programData && this.programData.target } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Partner" content={ this.programData && this.programData.partner } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="URL Gambar Program" content={ this.programData && this.programData.logoUrl } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<Button onClick={(e) => {e.preventDefault(); this.Hapus()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Hapus" />
				<Button onClick={(e) => {e.preventDefault(); this.Ubah()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Ubah" />
			</Detail>
		)
	}
}

export default DetailProgram;
