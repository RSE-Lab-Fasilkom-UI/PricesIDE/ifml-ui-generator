import React from 'react';
import InputField from '../components/InputField/InputField';
import SelectionField from '../components/SelectionField/SelectionField';
import Form from '../components/Form/Form';
import Button from '../components/Button/Button';
import queryString from 'query-string';
import CallvolunteersaveService from '../services/call-volunteer-save.service'

class TambahkanVolunteer extends React.Component {
	state = {};
	componentWillMount = () => {
	};

	Kirim = async () => {

		const data = await CallvolunteersaveService.call({
			name: this.namaInput.value,
			phone: this.nomorTeleponInput.value,
			email: this.emailInput.value,
		});


		this.props.history.push({
			pathname: '/daftar-volunteer',
            search: queryString.stringify({
                jSONAllVolunteer: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
		
	}

	render() {
		return (
			<Form title="Tambahkan Volunteer" id_name="tambahkan-volunteer" variant={{'shape': 'default', 'color': 'blue', 'borderColor': 'transparent', 'bgColor': 'white', 'alignment': 'center'}}>
				<InputField camel_name="namaInput" type="" dasherized="input-nama" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Nama" placeholder="Fill the Nama"  ref_func={e => {this.namaInput = e;}}/>
				<InputField camel_name="nomorTeleponInput" type="" dasherized="input-nomor-telepon" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Nomor Telepon" placeholder="Fill the Nomor Telepon"  ref_func={e => {this.nomorTeleponInput = e;}}/>
				<InputField camel_name="emailInput" type="" dasherized="input-email" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Email" placeholder="Fill the Email"  ref_func={e => {this.emailInput = e;}}/>
				<Button onClick={(e) => {e.preventDefault(); this.Kirim()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Kirim" />
			</Form>
		)
	}
}

export default TambahkanVolunteer;
