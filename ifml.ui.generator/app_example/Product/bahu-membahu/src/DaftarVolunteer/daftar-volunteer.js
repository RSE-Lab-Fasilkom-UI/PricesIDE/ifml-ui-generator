//Daftar Volunteer Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';

import VolunteerFeatureComponent from '../VolunteerFeature/volunteer-feature.js';





import List from '../components/List/List';

import ListDaftarVolunteerComponent from '../ListDaftarVolunteer/list-daftar-volunteer.js'


class DaftarVolunteer extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			jSONAllVolunteer:JSON.parse(queryString.parse(this.props.location.search).jSONAllVolunteer),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				<AuthConsumer>{ (values) => {         
					const Component = withRouter(VolunteerFeatureComponent);
						return <Component {...values} />
					}}
				</AuthConsumer>
				<List variant={{'borderColor': 'blue', 'bgColor': 'white', 'shape': 'default', 'alignment': 'left'}} id_name='list-ListDaftarVolunteerComponent' className='list-component view-component' column='2'>
					{ this.state.jSONAllVolunteer && this.state.jSONAllVolunteer.map((jSONAllVolunteer) => (
						<ListDaftarVolunteerComponent {...this.props} jSONAllVolunteer={jSONAllVolunteer}/>
					))} 
				</List>
			</React.Fragment>
		);
	}
}

export default DaftarVolunteer;

