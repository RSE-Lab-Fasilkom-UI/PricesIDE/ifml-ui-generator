import styled from "styled-components";

export const AboutStyle = styled.div`
  width: 100vw;
  height: 60vh;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
