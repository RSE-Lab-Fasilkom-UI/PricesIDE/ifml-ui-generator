import styled from "styled-components";

export const VisualizationDiv = styled.div`
  padding-left: 1.5em;
  font-family: Helvetica, sans-serif;

  .visualization_label {
    font-weight: bold;
    font-size: 16px;
  }

  .visual_label {
    color: ${(props) =>
      props.titleColor
        ? props.titleColor
        : props.color
        ? props.color
        : "black"};
    text-transform: ${(props) =>
      props.uppercase === "yes" ? "uppercase" : "none"};
  }

  .visualization_content {
    color: ${(props) => (props.color ? props.color : "black")};
  }
`;

