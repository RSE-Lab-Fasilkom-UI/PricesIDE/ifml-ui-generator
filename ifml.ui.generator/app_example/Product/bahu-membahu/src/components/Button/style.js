import styled from "styled-components";

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ButtonStyle = styled.button`
  text-align: center;
  font-weight: bold;
  padding: 10px 10px;
  margin: 10px 5px;
  font-size: 1em;
  line-height: 1.3;
  width: 90%;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    opacity: 0.5;
  }

  &:hover {
    background-color: ${(props) =>
      props.bgColor ? `${props.bgColor}4f` : "#00000014"};
    transition: 0.4s;
  }

  background-color: ${(props) => (props.bgColor ? props.bgColor : "#fff")};
  color: ${(props) => (props.color ? props.color : "black")};
  border-radius: ${(props) => (props.shape === "rounded" ? "64px" : "4px")};
  border: ${(props) =>
    props.borderColor ? `1px solid ${props.borderColor}` : "1px solid black"};
`;
