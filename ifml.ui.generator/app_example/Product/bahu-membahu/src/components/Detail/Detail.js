import React from "react";
import { DetailStyle } from "./style";

const Detail = (props) => {
  const { variant } = props;

  return <DetailStyle {...variant}>{props.children}</DetailStyle>;
};

export default Detail;

