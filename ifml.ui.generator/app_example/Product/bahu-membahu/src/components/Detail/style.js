import styled from "styled-components";

export const DetailStyle = styled.div`
  width: 70%;
  margin-left: 15%;
  margin-top: 2em;
  margin-bottom: 2em;
  padding: 20px 30px;
  box-sizing: border-box;
  box-shadow: 0 0 5px 1px rgba(0, 0, 0, 0.4);

  border-radius: ${(props) => (props.shape === "rounded" ? "16px" : "4px")};
  border: 1px solid
    ${(props) => (props.borderColor ? props.borderColor : "transparent")};
  background-color: ${(props) => (props.bgColor ? props.bgColor : "white")};
`;

