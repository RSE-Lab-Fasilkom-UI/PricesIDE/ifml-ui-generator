import React from "react";
import { ListItemStyle } from "./style";

const ListItem = (props) => {
  const { onClick, variant } = props;

  const onClickfunc = onClick ? onClick : undefined;

  return (
    <ListItemStyle onClick={onClickfunc} {...variant}>
      {props.children}
    </ListItemStyle>
  );
};

export default ListItem;
