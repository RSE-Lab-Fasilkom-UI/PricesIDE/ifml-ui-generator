import styled from "styled-components";

export const ListItemStyle = styled.div`
  position: relative;
  align-items: center;
  width: 80%;
  padding: 20px;
  cursor: pointer;
  -webkit-column-break-inside: avoid;
  page-break-inside: avoid;
  break-inside: avoid;
  margin-bottom: 1em;
  border: 1px solid
    ${(props) => (props.borderColor ? props.borderColor : "transparent")};
  background-color: ${(props) => (props.bgColor ? props.bgColor : "white")};
  border-radius: ${(props) => (props.shape === "rounded" ? "16px" : "4px")};

  &:hover {
    border-color: ${(props) =>
      props.hoverColor ? props.hoverColor : props.borderColor};
    background-color: ${(props) =>
      props.bgColor ? `${props.bgColor}4f` : "#00000014"};
  }
`;

