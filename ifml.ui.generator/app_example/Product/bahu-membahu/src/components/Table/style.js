import styled from "styled-components";

export const TableStyle = styled.table`
  border-spacing: 0px;
  background: #fff;
  box-shadow: 0 1px 0 0 rgba(22, 29, 37, 0.05);
  margin-top: 1rem;
  margin-bottom: 1rem;
  width: 70%;
  margin-left: 15%;
  margin-right: 15%;
  border-collapse: collapse;

  thead > tr {
    background-color: ${(props) =>
      props.headBgColor ? props.headBgColor : "lightgray"};
    text-transform: ${(props) =>
      props.headUppercase === "yes" ? "uppercase" : "none"};
    letter-spacing: 3px;
    font-size: 1em;
    text-align: ${(props) =>
      props.headTextAlign ? props.headTextAlign : "center"};
    color: ${(props) => (props.headColor ? props.headColor : "#000")};
    font-weight: ${(props) =>
      props.headFontWeight ? props.headFontWeight : "700"};
    border: 1px solid
      ${(props) => (props.headBgColor ? props.headBgColor : "lightgray")};
  }

  thead > tr > td {
    padding: 1.5em;
    border-right: 1px solid
      ${(props) =>
        props.headBorderColor ? props.headBorderColor : "transparent"};
  }

  thead > tr > td:last-child {
    border-right: none;
  }

  thead > tr > td:first-child {
    border-left: none;
  }

  tbody {
    border: 1px solid
      ${(props) => (props.bodyBorderColor ? props.bodyBorderColor : "black")};
  }

  tbody > tr > td:last-child {
    border-right: none !important;
  }

  tbody > tr > td:first-child {
    border-left: none !important;
  }

  tbody > tr:nth-child(even) {
    background: ${(props) => props.evenBgColor};
    color: ${(props) => props.evenColor};
  }

  tbody > tr:nth-child(even) > td {
    border-bottom: 1px solid ${(props) => props.evenBorderColor};
    border-right: 1px solid ${(props) => props.evenBorderColor};
  }

  tbody > tr:nth-child(odd) > td {
    border-bottom: 1px solid ${(props) => props.oddBorderColor};
    border-right: 1px solid ${(props) => props.oddBorderColor};
  }

  tbody > tr:nth-child(odd) {
    background: ${(props) => props.oddBgColor};
    color: ${(props) => props.oddColor};
  }

  tbody > tr > td {
    padding: 0.8em;
    font-weight: ${(props) =>
      props.bodyFontWeight ? props.bodyFontWeight : "normal"};
    border-bottom: 1px solid
      ${(props) =>
        props.bodyBorderColor ? props.bodyBorderColor : "lightgray"};
    border-right: 1px solid
      ${(props) =>
        props.bodyBorderColor ? props.bodyBorderColor : "lightgray"};
  }

  td {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
  }

  .distinct-row td:nth-child(1) {
    border-right: 0px !important;
    background-color: ${(props) =>
      props.distinctBgColor ? props.distinctBgColor : "lightgray"};
    color: ${(props) => (props.distinctColor ? props.distinctColor : "black")};
    border: none;
  }

  .distinct-row td:nth-child(2) {
    color: transparent;
    background-color: ${(props) =>
      props.distinctBgColor ? props.distinctBgColor : "lightgray"};
    border: none;
  }
`;

