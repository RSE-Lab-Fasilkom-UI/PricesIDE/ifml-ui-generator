import React from "react";

const MenuItem = (props) => {
  return <ul className="submenu">{props.children} </ul>;
};

export default MenuItem;
