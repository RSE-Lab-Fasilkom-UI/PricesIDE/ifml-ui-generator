import styled from "styled-components";

export const RootMenuStyle = styled.nav`
  text-align: center;
  font-family: sans-serif;
  list-style-type: none;
  display: flex;
  background-color: ${(props) => (props.bgColor ? props.bgColor : "lightgray")};
  color: ${(props) => (props.color ? props.color : "#fff")};
  font-weight: ${(props) => (props.fontWeight ? props.fontWeight : "normal")};

  a {
    color: ${(props) => (props.color ? props.color : "#fff")};
  }

  &:hover {
    cursor: pointer;
  }

  .root_span {
    text-transform: ${(props) =>
      props.uppercase === "yes" ? "uppercase" : "none"};

    .submenu {
      display: none;
      left: 0;
      position: absolute;
      top: 100%;
      list-style-type: none;
      margin: 0;
      padding: 0;
      z-index: 9;
      width: auto;
      white-space: nowrap;
      background-color: ${(props) =>
        props.bgColor ? props.bgColor : "lightgray"};
    }

    .submenu .menu_list {
      width: 100%;
      text-transform: ${(props) =>
        props.uppercase === "yes" ? "uppercase" : "none"};
      color: ${(props) => (props.color ? props.color : "#fff")};
    }

    .submenu .submenu {
      left: 100%;
      position: absolute;
      top: 0;
    }

    .menu_list:hover > .submenu {
      display: block;
    }

    .menu_list {
      float: left;
      position: relative;
      height: 100%;
      text-transform: ${(props) =>
        props.uppercase === "yes" ? "uppercase" : "none"};
      color: ${(props) => (props.color ? props.color : "#fff")};
    }

    .menu_list > a {
      color: ${(props) => (props.color ? props.color : "white")};
      font-weight: ${(props) =>
        props.fontWeight ? props.fontWeight : "normal"};
    }

    .menu_list:hover {
      background-color: ${(props) =>
        props.itemBgColor ? props.itemBgColor : "gray"};
    }
  }
`;
