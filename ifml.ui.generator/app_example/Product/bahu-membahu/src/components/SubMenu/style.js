import styled from "styled-components";

export const SubMenuStyle = styled.nav`
  font-family: Helvetica, sans-serif;
  background-color: ${(props) =>
    props.submenuBgColor ? props.submenuBgColor : "lightgray"};

  &:hover {
    background-color: ${(props) =>
      props.submenuHoverBgColor ? props.submenuHoverBgColor : "lightgray"};
  }

  .submenu_list {
    list-style-type: none;
    overflow: hidden;
    margin: 0;
    text-transform: ${(props) =>
      props.uppercase === "yes" ? "uppercase" : "none"};

    .menu_list {
      float: left;
      position: relative;
      height: 100%;
    }

    .menu_list > a {
      color: ${(props) => (props.submenuColor ? props.submenuColor : "white")};
      font-weight: ${(props) =>
        props.submenuFontWeight ? props.submenuFontWeight : "normal"};
    }

    .menu_list:hover {
      background-color: ${(props) =>
        props.submenuItemBgColor ? props.submenuItemBgColor : "gray"};
    }
  }
`;
