import React from "react";
import { SubMenuStyle } from "./style";

const SubMenu = (props) => {
  const { variant } = props;

  return (
    <SubMenuStyle {...variant}>
      <ul className={"submenu_list"}>{props.children}</ul>
    </SubMenuStyle>
  );
};

export default SubMenu;
