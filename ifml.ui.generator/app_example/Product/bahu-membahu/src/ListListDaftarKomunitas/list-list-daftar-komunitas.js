
//List
import React from 'react';
import Button from '../components/Button/Button';
import ListItem from '../components/ListItem/ListItem';
import queryString from 'query-string';
import VisualizationAttr from '../components/VisualizationAttr/VisualizationAttr';

class ListListDaftarKomunitas extends React.Component {
	state = {}
	
	componentWillMount = () => {
		this.komunitasListElement = this.props.jsonAllKomunitas;this.id = this.komunitasListElement.id
	};
	
	
	currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }

    indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }

	render() {
		return(
		<ListItem  variant={{'borderColor': 'blue', 'bgColor': 'white', 'shape': 'default', 'alignment': 'left'}}>
			{/* //Data Binding Komunitas List Element*/}
			<VisualizationAttr title_name="Nama" content={ this.komunitasListElement && this.komunitasListElement.name } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
		</ListItem>
		)
	}
}
export default ListListDaftarKomunitas;
