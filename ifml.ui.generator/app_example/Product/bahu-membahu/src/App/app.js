import React from 'react';
import {
    AuthProvider,
    AuthConsumer,
    withAuth
} from '../Authentication';
import {
    BrowserRouter,
    Route,
    Link,
    Switch
} from 'react-router-dom';
import {
    CookiesProvider
} from 'react-cookie';
import {
    RootMenu,
    MenuItem,
    MenuLink
} from '../components';
import FeatureMainMenuComponent from '../FeatureMainMenu/feature-main-menu.js';

//Program
import DaftarProgramComponent from '../DaftarProgram/daftar-program.js';
import HalamanTambahProgramComponent from '../HalamanTambahProgram/halaman-tambah-program.js';
import HalamanDetailProgramComponent from '../HalamanDetailProgram/halaman-detail-program.js';
import HalamanUbahProgramComponent from '../HalamanUbahProgram/halaman-ubah-program.js';

//Income
import CatatanPemasukanComponent from '../CatatanPemasukan/catatan-pemasukan.js';
import HalamanTambahPemasukanComponent from '../HalamanTambahPemasukan/halaman-tambah-pemasukan.js';
import HalamanDetailPemasukanComponent from '../HalamanDetailPemasukan/halaman-detail-pemasukan.js';
import HalamanUbahPemasukanComponent from '../HalamanUbahPemasukan/halaman-ubah-pemasukan.js'

//Volunteer
import DaftarVolunteerComponent from '../DaftarVolunteer/daftar-volunteer';
import HalamanTambahVolunteerComponent from '../HalamanTambahVolunteer/halaman-tambah-volunteer';

//Komunitas
import DaftarKomunitasComponent from '../DaftarKomunitas/daftar-komunitas';
import HalamanTambahKomunitasComponent from '../HalamanTambahKomunitas/halaman-tambah-komunitas';

import LoginComponent from '../Login/login.js';
import HomepageComponent from '../Homepage/homepage.js';
import LandingPageComponent from '../LandingPage/landing-page.js';

class App extends React.Component {
    state = {};
    onLogoutClicked = e => {
        e.preventDefault();
        this.props.logout();
    };




    currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }

    indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }

    render() {
        return (<CookiesProvider><BrowserRouter><AuthProvider>
<React.Fragment>
<AuthConsumer>{ (values) => { this.props = values; console.log(values);       if(values.isAuth){        return <FeatureMainMenuComponent variant={{"bgColor": "blue", "submenuBgColor": "lightblue", "submenuHoverBgColor": "lightblue", "submenuItemBgColor": "blue", "submenuColor": "white", "uppercase": "no", "itemBgColor": "darkblue", "color": "white"}} />         }else {        return (<HomepageComponent/>); } } }</AuthConsumer>
<Switch>


{/* Program */}
<Route
  exact
  path="/daftar-program"
  component={
  withAuth(DaftarProgramComponent
  )

  }
/>
<Route
  exact
  path="/halaman-tambah-program"
  component={
  withAuth(HalamanTambahProgramComponent
  )

  }
/>
<Route
  exact
  path="/halaman-detail-program"
  component={
  withAuth(HalamanDetailProgramComponent
  )

  }
/>
<Route
  exact
  path="/halaman-ubah-program"
  component={
  withAuth(HalamanUbahProgramComponent
  )

  }
/>
{/* Income */}
<Route
  path="/catatan-pemasukan"
  component={
  withAuth(CatatanPemasukanComponent
  )

  }
/>
<Route
  exact
  path="/halaman-tambah-pemasukan"
  component={
  withAuth(HalamanTambahPemasukanComponent
  )

  }
/>
<Route
  exact
  path="/halaman-ubah-pemasukan"
  component={
  withAuth(HalamanUbahPemasukanComponent
  )

  }
/>
<Route
  exact
  path="/halaman-detail-pemasukan"
  component={
  withAuth(HalamanDetailPemasukanComponent
  )

  }
/>

{/* Volunteer */}
<Route
  exact
  path="/daftar-volunteer"
  component={
  withAuth(DaftarVolunteerComponent)
  }
/>
<Route
  exact
  path="/halaman-tambah-volunteer"
  component={
  withAuth(HalamanTambahVolunteerComponent)
  }
/>

{/* Komunitas */}
<Route
  exact
  path="/daftar-komunitas"
  component={
  withAuth(DaftarKomunitasComponent)
  }
/>
<Route
  exact
  path="/halaman-tambah-komunitas"
  component={
  withAuth(HalamanTambahKomunitasComponent)
  }
/>

<Route
  exact
  path="/login"
  component={LoginComponent

  }
/>
<Route
  exact
  path="/"
  component={LandingPageComponent

  }
/>
</Switch>
</React.Fragment>
</AuthProvider></BrowserRouter></CookiesProvider>);
    }
}

export default App;