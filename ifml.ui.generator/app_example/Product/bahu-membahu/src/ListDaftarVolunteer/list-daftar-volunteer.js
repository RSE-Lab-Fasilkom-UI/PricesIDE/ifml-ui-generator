
//List
import React from 'react';
import Button from '../components/Button/Button';
import ListItem from '../components/ListItem/ListItem';
import queryString from 'query-string';
import VisualizationAttr from '../components/VisualizationAttr/VisualizationAttr';

class ListDaftarVolunteer extends React.Component {
	state = {}
	
	componentWillMount = () => {
		this.volunteerListElement = this.props.jsonAllVolunteer;this.id = this.volunteerListElement.id
	};
	
	
	currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }

    indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }

	render() {
		return(
		<ListItem  variant={{'borderColor': 'blue', 'bgColor': 'white', 'shape': 'default', 'alignment': 'left'}}>
			{/* //Data Binding Volunteer List Element*/}
			<VisualizationAttr title_name="Nama" content={ this.volunteerListElement && this.volunteerListElement.name } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<VisualizationAttr title_name="Nomor Telepon" content={ this.volunteerListElement && this.volunteerListElement.phone } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<VisualizationAttr title_name="Email" content={ this.volunteerListElement && this.volunteerListElement.email } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
		</ListItem>
		)
	}
}
export default ListDaftarVolunteer;
