import React from 'react';
import style from '../css_modules/static-page.module.css';
import ApiadminStaticService from '../services/apiadmin-static.service';
import PageService from '../services/page.service';
import ContactService from '../services/contact.service';
import Footer from '../components/Footer/index';
import Hero from '../components/Hero/index';
import About from '../components/About/index';

class LandingPage extends React.Component {
    state = {};
    componentDidMount = () => {
        ApiadminStaticService.call()
            .then(res => res['data'])
            .then(json => this.setState({
                about: json['page']['about_us'],
                slogan: json['page']['slogan'],
                address: json['contact']['address'],
                map: json['contact']['google_map_url'],
                email: json['contact']['email'],
                phone: json['contact']['phone'],
                logo_url: json['essential_assets']['logo_url'],
                banner: json['essential_assets']['banner_url']
            }));
        PageService.call()
            .then(res => res['data'])
            .then(json => this.setState({
                about: json['page']['about_us'],
                description: json['page']['description']
            }));
        ContactService.call()
            .then(res => res['data'])
            .then(json => this.setState({
                address: json['contact']['address'],
                email: json['contact']['email'],
                phone: json['contact']['phone'],
                fb_url: json['contact']['url']['fb_url'],
                map_url: json['contact']['url']['google_map_url'],
                instagram_url: json['contact']['url']['instagram_url'],
            }));
    };




    currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }

    indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }

    render() {
        return (<div className="landing-page">
        <Hero banner={this.state.banner}>
          <div className={style.box}>
            <h1 className={`${style.heading_main} ${style.None}`}>
              {this.state.slogan}
            </h1>
            <h4 className={`${style.heading_categories} ${style.None}`}>
              {this.state.about}
            </h4>
          </div>
        </Hero>
        <About>
          <div className={style.box}>
            <h1 className={`${style.heading_main} ${style.None}`}>About Us</h1>
            <h3 className={`${style.heading_categories} ${style.None}`}>
              {this.state.about}
            </h3>
            <p className={`${style.writing_content} ${style.None}`}>
              {this.state.description}
            </p>
          </div>
        </About>
        <Footer>
          <div className="contact">
            <div className={style.box}>
              <h1 className={`${style.heading_main} ${style.None}`}>
                Contact Us
              </h1>

              <h3 className={`${style.heading_categories} ${style.None}`}>
                Address
              </h3>
              <p className={`${style.writing_content} ${style.None}`}>
                {this.state.address}
              </p>

              <h3 className={`${style.heading_categories} ${style.None}`}>
                Email
              </h3>
              <p className={`${style.writing_content} ${style.None}`}>
                {this.state.email}
              </p>

              <h3 className={`${style.heading_categories} ${style.None}`}>
                Phone
              </h3>
              <p className={`${style.writing_content} ${style.None}`}>
                {this.state.phone}
              </p>
            </div>
          </div>
          <div className="sitemap">
            <div className={style.box}>
              <h4 className={`${style.heading_categories} ${style.None}`}>
                Sitemap
              </h4>
              <p className={`${style.writing_content} ${style.None}`}>
                <a href={this.state.fb_url} className={style.links}>
                  Facebook page
                </a>
              </p>
              <p className={`${style.writing_content} ${style.None}`}>
                <a href={this.state.map_url} className={style.links}>
                  Location on map
                </a>
              </p>
              <p className={`${style.writing_content} ${style.None}`}>
                <a href={this.state.instagram_url} className={style.links}>
                  Instagram page
                </a>
              </p>
            </div>
          </div>
        </Footer>
      </div>);
    }
}

export default LandingPage;