import React from 'react';
import InputField from '../components/InputField/InputField';
import SelectionField from '../components/SelectionField/SelectionField';
import Form from '../components/Form/Form';
import Button from '../components/Button/Button';
import queryString from 'query-string';
import CallcommunitysaveService from '../services/call-community-save.service'

class TambahkanKomunitas extends React.Component {
	state = {};
	componentWillMount = () => {
	};

	Kirim = async () => {

		const data = await CallcommunitysaveService.call({
			name: this.namaInput.value,
		});


		this.props.history.push({
			pathname: '/daftar-komunitas',
            search: queryString.stringify({
                jsonAllKomunitas: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
		
	}

	render() {
		return (
			<Form title="Tambahkan Komunitas" id_name="tambahkan-komunitas" variant={{'shape': 'default', 'color': 'blue', 'borderColor': 'transparent', 'bgColor': 'white', 'alignment': 'center'}}>
				<InputField camel_name="namaInput" type="" dasherized="input-nama" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Nama" placeholder="Fill the Nama"  ref_func={e => {this.namaInput = e;}}/>
				<Button onClick={(e) => {e.preventDefault(); this.Kirim()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Kirim" />
			</Form>
		)
	}
}

export default TambahkanKomunitas;
