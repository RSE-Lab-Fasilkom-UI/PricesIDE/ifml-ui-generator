//Daftar Program Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';

import ProgramFeatureComponent from '../ProgramFeature/program-feature.js';





import List from '../components/List/List';

import ListProgramContentComponent from '../ListProgramContent/list-program-content.js'


class DaftarProgram extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			jsonAllProgram:JSON.parse(queryString.parse(this.props.location.search).jsonAllProgram),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				<AuthConsumer>{ (values) => {         
					const Component = withRouter(ProgramFeatureComponent);
						return <Component {...values} />
					}}
				</AuthConsumer>
				<List variant={{'borderColor': 'blue', 'bgColor': 'white', 'shape': 'default', 'alignment': 'left'}} id_name='list-ListProgramContentComponent' className='list-component view-component' column='2'>
					{ this.state.jsonAllProgram && this.state.jsonAllProgram.map((jsonAllProgram) => (
						<ListProgramContentComponent {...this.props} jsonAllProgram={jsonAllProgram}/>
					))} 
				</List>
			</React.Fragment>
		);
	}
}

export default DaftarProgram;

