import React from 'react';
import MenuItem from '../components/MenuItem/MenuItem';
import MenuLink from '../components/MenuLink/MenuLink';
import queryString from 'query-string';
import CallvolunteerlistService from '../services/call-volunteer-list.service'

class VolunteerMainMenu extends React.Component {
    state = {};
	onClickFunc = async () => {
	    const data = await CallvolunteerlistService.call();
		
	    this.props.history.push({
	        pathname: '/daftar-volunteer',
	        search: queryString.stringify({
	            jsonAllVolunteer: JSON.stringify(data['data'] ? data['data']['data'] : [])
	        })
	    });
	};

    currencyFormatDE(num) {
        if (!num) {
            return "0,00"
        }
        return (
            num.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    }

    render() {
        return (
        <MenuItem variant={this.props.variant}>
        <MenuLink variant={this.props.variant} href="#" 
		onClick={(e) => { e.preventDefault();
		this.onClickFunc(); } }>
		Volunteer
		</MenuLink>
		</MenuItem>
        );
    }
}

export default VolunteerMainMenu;
