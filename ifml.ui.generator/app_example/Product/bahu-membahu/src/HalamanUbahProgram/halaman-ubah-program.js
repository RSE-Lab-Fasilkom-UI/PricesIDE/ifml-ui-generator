//Halaman Ubah Program Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import UbahDataProgramComponent from '../UbahDataProgram/ubah-data-program.js'

class HalamanUbahProgram extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			objectEditProgram:JSON.parse(queryString.parse(this.props.location.search).objectEditProgram),
		});
	};

	
	render() {
		return (
			<React.Fragment>
					{ 
					this.state.objectEditProgram !== undefined && 
					<UbahDataProgramComponent {...this.props} 
					objectEditProgram={this.state.objectEditProgram}
					></UbahDataProgramComponent>}
			</React.Fragment>
		);
	}
}

export default HalamanUbahProgram;

