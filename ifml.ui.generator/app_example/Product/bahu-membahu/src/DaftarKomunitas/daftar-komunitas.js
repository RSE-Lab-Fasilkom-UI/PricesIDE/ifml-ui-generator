//Daftar Komunitas Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';

import KomunitasFeatureComponent from '../KomunitasFeature/komunitas-feature.js';





import List from '../components/List/List';

import ListListDaftarKomunitasComponent from '../ListListDaftarKomunitas/list-list-daftar-komunitas.js'


class DaftarKomunitas extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			jsonAllKomunitas:JSON.parse(queryString.parse(this.props.location.search).jsonAllKomunitas),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				<AuthConsumer>{ (values) => {         
					const Component = withRouter(KomunitasFeatureComponent);
						return <Component {...values} />
					}}
				</AuthConsumer>
				<List variant={{'borderColor': 'blue', 'bgColor': 'white', 'shape': 'default', 'alignment': 'left'}} id_name='list-ListListDaftarKomunitasComponent' className='list-component view-component' column='2'>
					{ this.state.jsonAllKomunitas && this.state.jsonAllKomunitas.map((jsonAllKomunitas) => (
						<ListListDaftarKomunitasComponent {...this.props} jsonAllKomunitas={jsonAllKomunitas}/>
					))} 
				</List>
			</React.Fragment>
		);
	}
}

export default DaftarKomunitas;

