//Halaman Detail Program Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import DetailProgramComponent from '../DetailProgram/detail-program.js'

class HalamanDetailProgram extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			objectDetailProgram:JSON.parse(queryString.parse(this.props.location.search).objectDetailProgram),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				{this.state.objectDetailProgram && <DetailProgramComponent {...this.props} objectDetailProgram={ this.state.objectDetailProgram}></DetailProgramComponent>}
			</React.Fragment>
		);
	}
}

export default HalamanDetailProgram;

