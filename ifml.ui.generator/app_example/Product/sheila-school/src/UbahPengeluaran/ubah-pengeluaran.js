import React from 'react';
import InputField from '../components/InputField/InputField';
import Form from '../components/Form/Form';
import Button from '../components/Button/Button';
import queryString from 'query-string';
import SelectionField from '../components/SelectionField/SelectionField'
import CallexpenseupdateService from '../services/call-expense-update.service'

class UbahPengeluaran extends React.Component {
	state = {};
	
	componentWillMount = () => {
		this.expenseData = this.props.objectExpense;this.idExpense = this.expenseData.id;this.tanggal = this.expenseData.datestamp;this.deskripsi = this.expenseData.description;this.jumlah = Number(this.expenseData.amount.toString().replace(",00", "").split('.').join(''));this.namaProgramTerkait = this.expenseData.idProgram;this.kodeAkun = this.expenseData.idCoa;this.program = this.props.program; this.chartofaccount = this.props.chartofaccount
	};

	Kirim = async () => {
		const data = await CallexpenseupdateService.call({
			id: this.idExpenseInput.value,
			datestamp: this.tanggalInput.value,
			description: this.deskripsiInput.value,
			amount: this.jumlahInput.value,
			idProgram: this.namaProgramTerkaitInput.value,
			idCoa: this.kodeAkunInput.value,
		});

		this.props.history.push({
			pathname: '/halaman-detail-pengeluaran',
            search: queryString.stringify({
                objectDetailExpense: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
		
	}
	
	render() {
		return (
			<Form title="Ubah Pengeluaran" id_name="ubah-pengeluaran" variant={{'shape': 'default', 'color': 'blue', 'borderColor': 'transparent', 'bgColor': 'white', 'alignment': 'center'}}>
				<InputField camel_name="idExpenseInput" type="" dasherized="input-id-expense" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Id Expense" placeholder="Fill the Id Expense"  defaultValue={ this.idExpense } ref_func={e => {this.idExpenseInput = e;}}/>
				<InputField camel_name="tanggalInput" type="" dasherized="input-tanggal" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Tanggal" placeholder="Fill the Tanggal"  defaultValue={ this.tanggal } ref_func={e => {this.tanggalInput = e;}}/>
				<InputField camel_name="deskripsiInput" type="" dasherized="input-deskripsi" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Deskripsi" placeholder="Fill the Deskripsi"  defaultValue={ this.deskripsi } ref_func={e => {this.deskripsiInput = e;}}/>
				<InputField camel_name="jumlahInput" type="" dasherized="input-jumlah" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Jumlah" placeholder="Fill the Jumlah"  defaultValue={ this.jumlah } ref_func={e => {this.jumlahInput = e;}}/>
				<SelectionField options={this.program} camel_name="namaProgramTerkaitInput" type="" dasherized="input-nama-program-terkait" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Nama Program Terkait" placeholder="Fill the Nama Program Terkait"  defaultValue={ this.namaProgramTerkait } ref_func={e => {this.namaProgramTerkaitInput = e;}}/>
				<SelectionField options={this.chartofaccount} camel_name="kodeAkunInput" type="" dasherized="input-kode-akun" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Kode Akun" placeholder="Fill the Kode Akun"  defaultValue={ this.kodeAkun } ref_func={e => {this.kodeAkunInput = e;}}/>
				<Button onClick={(e) => {e.preventDefault(); this.Kirim()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Kirim" />
			</Form>
		)
	}
}
export default UbahPengeluaran;
