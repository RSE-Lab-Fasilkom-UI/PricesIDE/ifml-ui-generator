import React from 'react';
import AuthConsumer from '../Authentication';
import { withRouter } from 'react-router-dom';
import RootMenu from '../components/RootMenu/RootMenu';
import MenuItem from '../components/MenuItem/MenuItem';
import MenuLink from '../components/MenuLink/MenuLink';
import MenuChildren from '../components/MenuChildren/MenuChildren';
import FeatureArrow from '../components/FeatureArrow/FeatureArrow';


import FinancialReportMainMenuComponent from '../FinancialReportMainMenu/financial-report-main-menu.js';
import IncomeMainMenuComponent from '../IncomeMainMenu/income-main-menu.js';
import ExpenseMainMenuComponent from '../ExpenseMainMenu/expense-main-menu.js';
import ProgramMainMenuComponent from '../ProgramMainMenu/program-main-menu.js';
import ReportMainMenuComponent from '../ReportMainMenu/report-main-menu.js';
import AutomaticMainMenuComponent from '../AutomaticMainMenu/automatic-main-menu.js';
import CoaMainMenuComponent from '../CoaMainMenu/coa-main-menu.js';


class FeatureMainMenu extends React.Component {
    state = {};
    
    onLogoutClicked = e => {
            e.preventDefault();
            this.props.logout();
        };
    
    currencyFormatDE(num) {
        if (!num) {
            return "0,00"
        }
        return (
            num.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    }

    render() {
		const appVariant = this.props.variant;
        return (
	        <AuthConsumer>{ (values) => 
	        	{ this.props = values; return (<RootMenu isAuth={this.props.isAuth} variant={appVariant}>
	        		<AuthConsumer>{
	        			(values) => {
	        				return (
	        					<MenuItem variant={appVariant}>
	        						<MenuLink variant={appVariant} href="#" onClick={this.onLogoutClicked}>
	        							Logout
	        						</MenuLink>
	        					</MenuItem>
	        				);
	        			}
	        		}</AuthConsumer>
					<AuthConsumer>{ 
						(values) => {
							const Component = withRouter(ProgramMainMenuComponent);
							return <Component {...values} variant={appVariant}/>
						} 
					}</AuthConsumer>
					<AuthConsumer>{ (values) => 
						{ return (<FinancialReportMainMenuComponent variant={appVariant}>
							<AuthConsumer>{ 
								(values) => {
									const Component = withRouter(IncomeMainMenuComponent);
									return <Component {...values} variant={appVariant}/>
								} 
							}</AuthConsumer>
							<AuthConsumer>{ 
								(values) => {
									const Component = withRouter(ExpenseMainMenuComponent);
									return <Component {...values} variant={appVariant}/>
								} 
							}</AuthConsumer>
							<AuthConsumer>{ (values) => 
								{ return (<ReportMainMenuComponent variant={appVariant}>
									
									<AuthConsumer>{ (values) => 
										{ return (<AutomaticMainMenuComponent variant={appVariant}>
											<AuthConsumer>{ 
												(values) => {
													const Component = withRouter(CoaMainMenuComponent);
													return <Component {...values} variant={appVariant}/>
												} 
											}</AuthConsumer>
											
										</AutomaticMainMenuComponent>);}
									}</AuthConsumer>
									
								</ReportMainMenuComponent>);}
							}</AuthConsumer>
						</FinancialReportMainMenuComponent>);}
					}</AuthConsumer>
					
				</RootMenu>)}
			}</AuthConsumer>
        );
    }
}

export default FeatureMainMenu;
