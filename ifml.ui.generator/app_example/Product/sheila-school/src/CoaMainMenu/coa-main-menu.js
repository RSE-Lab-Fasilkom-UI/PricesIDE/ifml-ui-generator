import React from 'react';
import MenuItem from '../components/MenuItem/MenuItem';
import MenuLink from '../components/MenuLink/MenuLink';
import MenuChildren from '../components/MenuChildren/MenuChildren';
import FeatureArrow from '../components/FeatureArrow/FeatureArrow';

import queryString from 'query-string';
import CallAutomaticReportTwoLevelListService from '../services/call-automatic-report-twolevel-list.service';

class CoaMainMenu extends React.Component {
    state = {};
	onClickFunc = async () => {
	    const data = await CallAutomaticReportTwoLevelListService .call();
		
	    this.props.history.push({
	        pathname: '/laporan-arus-kas',
	        search: queryString.stringify({
	            jsonAllChartOfAccountEntry: JSON.stringify(data['data'] ? data['data']['data'] : [])
	        })
	    });
	};

    currencyFormatDE(num) {
        if (!num) {
            return "0,00"
        }
        return (
            num.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    }

    render() {
        return (
        <MenuItem variant={this.props.variant}>
        <MenuLink variant={this.props.variant} href="#" 
		onClick={(e) => { e.preventDefault();
		this.onClickFunc(); } }>
		Laporan Arus Kas
		</MenuLink>
		</MenuItem>
        );
    }
}

export default CoaMainMenu;
