//Halaman Ubah Pengeluaran Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import CallprogramlistService from '../services/call-program-list.service';
import CallchartofaccountlistService from '../services/call-chart-of-account-list.service';
import UbahPengeluaranComponent from '../UbahPengeluaran/ubah-pengeluaran.js'

class HalamanUbahPengeluaran extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		const program = await CallprogramlistService.call()
		const chartofaccount = await CallchartofaccountlistService.call()
		this.setState({
			objectExpense:JSON.parse(queryString.parse(this.props.location.search).objectExpense),
			program: program['data']['data'],
			chartofaccount: chartofaccount['data']['data'],
		});
	};

	
	render() {
		return (
			<React.Fragment>
					{ 
					this.state.objectExpense !== undefined && 
					this.state.program &&
					this.state.chartofaccount &&
					<UbahPengeluaranComponent {...this.props} 
					objectExpense={this.state.objectExpense}
					program={this.state.program}
					chartofaccount={this.state.chartofaccount}
					></UbahPengeluaranComponent>}
			</React.Fragment>
		);
	}
}

export default HalamanUbahPengeluaran;

