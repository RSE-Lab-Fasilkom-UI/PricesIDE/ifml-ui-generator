//Table
import React from 'react';
import Button from '../components/Button/Button';
import TableRow from '../components/Table/TableRow';
import TableCell from '../components/Table/TableCell';
import queryString from 'query-string';
import CallexpensedetailService from '../services/call-expense-detail.service'

class TableExpenseContent extends React.Component {
	state = {}
	
	componentWillMount = () => {
			this.expenseListElement = this.props.jsonAllExpense;this.idExpense = this.expenseListElement.id;this.expenseListElement.amount = this.currencyFormatDE(this.expenseListElement.amount)
	};
	currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }
	
	indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }

	Edit = async () => {
		
		this.props.history.push({
			pathname: '/halaman-ubah-pengeluaran',
            search: queryString.stringify({
                objectExpense: JSON.stringify(this.expenseListElement),
            })
		})
	}
	Detail = async () => {
		
		const data = await CallexpensedetailService.call({
			id: this.idExpense,
		});
		
		this.props.history.push({
			pathname: '/halaman-detail-pengeluaran',
            search: queryString.stringify({
                objectDetailExpense: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
	}


	render() {
		return (
			<TableRow  distinct={false}>
				{/* //Data Binding Expense List Element*/}
				
				<TableCell content={ this.expenseListElement && this.expenseListElement.datestamp } />
				
				<TableCell content={ this.expenseListElement && this.expenseListElement.programName } />
				
				<TableCell content={ this.expenseListElement && this.expenseListElement.description } />
				
				<TableCell content={ this.expenseListElement && this.expenseListElement.coaName } />
				
				<TableCell content={ this.expenseListElement && this.expenseListElement.amount } />
				<TableCell variant="{&quot;borderColor&quot;: &quot;blue&quot;, &quot;bgColor&quot;: &quot;white&quot;, &quot;shape&quot;: &quot;default&quot;, &quot;alignment&quot;: &quot;left&quot;}"><Button onClick={(e) => {e.preventDefault(); this.Edit()} } variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Edit" /></TableCell>
				<TableCell variant="{&quot;borderColor&quot;: &quot;blue&quot;, &quot;bgColor&quot;: &quot;white&quot;, &quot;shape&quot;: &quot;default&quot;, &quot;alignment&quot;: &quot;left&quot;}"><Button onClick={(e) => {e.preventDefault(); this.Detail()} } variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Detail" /></TableCell>
			</TableRow>
		)
	}
}

export default TableExpenseContent;
