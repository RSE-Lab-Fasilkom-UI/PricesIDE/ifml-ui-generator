
//List
import React from 'react';
import Button from '../components/Button/Button';
import ListItem from '../components/ListItem/ListItem';
import queryString from 'query-string';
import VisualizationAttr from '../components/VisualizationAttr/VisualizationAttr';
import CallprogramdetailService from '../services/call-program-detail.service'

class ListProgramContent extends React.Component {
	state = {}
	
	componentWillMount = () => {
		this.programListElement = this.props.jsonAllProgram;this.id = this.programListElement.id
	};
	
	detailProgram = async () => {
		const data = await CallprogramdetailService.call({
			id: this.id,
		});
		
		this.props.history.push({
			pathname: '/halaman-detail-program',
            search: queryString.stringify({
                objectDetailProgram: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
	}
	
	currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }

    indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }

	render() {
		return(
		<ListItem  variant={{'borderColor': 'blue', 'bgColor': 'white', 'shape': 'default', 'alignment': 'left'}}>
			{/* //Data Binding Program List Element*/}
			<VisualizationAttr title_name="Nama" content={ this.programListElement && this.programListElement.name } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<VisualizationAttr title_name="Deskripsi" content={ this.programListElement && this.programListElement.description } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<VisualizationAttr title_name="Partner" content={ this.programListElement && this.programListElement.partner } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<VisualizationAttr title_name="Target" content={ this.programListElement && this.programListElement.target } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<VisualizationAttr title_name="Gambar" content={ this.programListElement && this.programListElement.logoUrl } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
			<Button onClick={(e) => {e.preventDefault(); this.detailProgram()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Detail Program" />
		</ListItem>
		)
	}
}
export default ListProgramContent;
