import React from 'react';

import MenuItem from '../components/MenuItem/MenuItem';
import MenuLink from '../components/MenuLink/MenuLink';
import MenuChildren from '../components/MenuChildren/MenuChildren';
import FeatureArrow from '../components/FeatureArrow/FeatureArrow';


class AutomaticMainMenu extends React.Component {
    state = {};

    currencyFormatDE(num) {
        if (!num) {
            return "0,00"
        }
        return (
            num.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    }

    render() {
        return (
        <MenuItem variant={this.props.variant}>
        <MenuLink variant={this.props.variant} href="#" 
		onClick="#">
		Automatic
		<FeatureArrow feat_type="sub"/>
		</MenuLink>
		<MenuChildren variant={this.props.variant}>
		{this.props.children}
		</MenuChildren>
		</MenuItem>
        );
    }
}

export default AutomaticMainMenu;
