//Catatan Pengeluaran Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';

import ExpenseFeatureComponent from '../ExpenseFeature/expense-feature.js';


import Table from '../components/Table/Table';
import TableRow from '../components/Table/TableRow';
import TableCell from '../components/Table/TableCell';
import TableBody from '../components/Table/TableBody';
import TableHead from '../components/Table/TableHead';


import TableExpenseContentComponent from '../TableExpenseContent/table-expense-content.js'


class CatatanPengeluaran extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			jsonAllExpense:JSON.parse(queryString.parse(this.props.location.search).jsonAllExpense),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				<AuthConsumer>{ (values) => {         
					const Component = withRouter(ExpenseFeatureComponent);
						return <Component {...values} />
					}}
				</AuthConsumer>
				<Table id='table-TableExpenseContentComponent' variant={{'headBgColor': 'blue', 'headColor': 'white', 'headTextAlign': 'center', 'headBorderColor': 'blue', 'headUppercase': 'yes', 'headFontWeight': '700', 'bodyBorderColor': 'black', 'bodyFontWeight': 'normal', 'evenBgColor': 'lightgray', 'evenColor': 'black', 'evenBorderColor': 'black', 'distinctBgColor': 'lightblue', 'distinctColor': 'black'}}>
					<TableHead>
						<TableRow>
							{/* //Data Binding Expense List Element*/}
							<TableCell id='Tanggal'>Tanggal</TableCell>
							<TableCell id='Program'>Program</TableCell>
							<TableCell id='Deskripsi'>Deskripsi</TableCell>
							<TableCell id='Kategori'>Kategori</TableCell>
							<TableCell id='Jumlah'>Jumlah</TableCell>
							<TableCell id='Edit'>Edit</TableCell>
							<TableCell id='Detail'>Detail</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{ this.state.jsonAllExpense && this.state.jsonAllExpense.map((jsonAllExpense) => (
							<TableExpenseContentComponent {...this.props} jsonAllExpense={jsonAllExpense}/>
						))} 
					
					</TableBody>
				</Table>
			</React.Fragment>
		);
	}
}

export default CatatanPengeluaran;

