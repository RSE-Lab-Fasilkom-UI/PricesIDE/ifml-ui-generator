//Table
import React from 'react';
import Button from '../components/Button/Button';
import TableRow from '../components/Table/TableRow';
import TableCell from '../components/Table/TableCell';
import queryString from 'query-string';

class TableChartOfAccountEntry extends React.Component {
	state = {}
	
	componentWillMount = () => {
			this.chartOfAccountEntryElement = this.props.jsonAllChartOfAccountEntry;this.chartOfAccountEntryElement.name = this.indentBasedOnLevel(this.chartOfAccountEntryElement.name, this.chartOfAccountEntryElement.level);this.chartOfAccountEntryElement.formattedAmount = this.currencyFormatDE(this.chartOfAccountEntryElement.amount);this.chartOfAccountEntryElement.distinct = this.setDistinctRow(this.chartOfAccountEntryElement.name)
	};
	currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }
	
	indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }



	render() {
		return (
			<TableRow  distinct={this.chartOfAccountEntryElement.distinct}>
				{/* //Data Binding Chart Of Account Entry Element*/}
				
				<TableCell content={ this.chartOfAccountEntryElement && this.chartOfAccountEntryElement.name } />
				
				<TableCell content={ this.chartOfAccountEntryElement && this.chartOfAccountEntryElement.formattedAmount } />
			</TableRow>
		)
	}
}

export default TableChartOfAccountEntry;
