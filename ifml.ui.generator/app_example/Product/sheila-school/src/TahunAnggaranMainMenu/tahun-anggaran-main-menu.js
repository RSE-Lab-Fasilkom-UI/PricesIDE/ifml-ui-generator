import React from 'react';
import MenuItem from '../components/MenuItem/MenuItem';
import MenuLink from '../components/MenuLink/MenuLink';
import MenuChildren from '../components/MenuChildren/MenuChildren';
import FeatureArrow from '../components/FeatureArrow/FeatureArrow';

import CallautomaticreportperiodicmodellistService from '../services/call-automatic-report-periodic-model-list.service';

import queryString from 'query-string';

class TahunAnggaranMainMenu extends React.Component {
    state = {};
	onClickFunc = async () => {
		const data = await CallautomaticreportperiodicmodellistService.call()
 	    this.props.history.push({
			pathname: '/daftar-tahun-anggaran',
            search: queryString.stringify({
                jsonAllPeriode: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
	};

    currencyFormatDE(num) {
        if (!num) {
            return "0,00"
        }
        return (
            num.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    }

    render() {
        return (
        <MenuItem variant={this.props.variant}>
        <MenuLink variant={this.props.variant} href="#" 
		onClick={(e) => { e.preventDefault();
		this.onClickFunc(); } }>
		Laporan Tahunan
		</MenuLink>
		</MenuItem>
        );
    }
}

export default TahunAnggaranMainMenu;
