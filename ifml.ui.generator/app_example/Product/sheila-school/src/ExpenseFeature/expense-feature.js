import React from 'react';
import MenuItem from '../components/MenuItem/MenuItem';
import MenuLink from '../components/MenuLink/MenuLink';
import MenuChildren from '../components/MenuChildren/MenuChildren';
import FeatureArrow from '../components/FeatureArrow/FeatureArrow';
import SubMenu from '../components/SubMenu/SubMenu';

class ExpenseFeature extends React.Component {
	state = {}
	tambahPengeluaran = () => {
		this.props.history.push({
			pathname: '/halaman-tambah-pengeluaran',
		})
	}

	render() {
        return (
            <SubMenu variant={{'bgColor': 'blue', 'submenuBgColor': 'lightblue', 'submenuHoverBgColor': 'lightblue', 'submenuItemBgColor': 'blue', 'submenuColor': 'white', 'uppercase': 'no', 'itemBgColor': 'darkblue', 'color': 'white'}}>
        		<MenuItem variant="{'bgColor': 'blue', 'submenuBgColor': 'lightblue', 'submenuHoverBgColor': 'lightblue', 'submenuItemBgColor': 'blue', 'submenuColor': 'white', 'uppercase': 'no', 'itemBgColor': 'darkblue', 'color': 'white'}">
					<MenuLink variant={{"bgColor": "blue", "submenuBgColor": "lightblue", "submenuHoverBgColor": "lightblue", "submenuItemBgColor": "blue", "submenuColor": "white", "uppercase": "no", "itemBgColor": "darkblue", "color": "white"}} href="#" onClick={(e) => { e.preventDefault();this.tambahPengeluaran(); } }>
						Tambah Pengeluaran
					</MenuLink>
				</MenuItem>
    		</SubMenu>
        );
    }
}
export default ExpenseFeature;
