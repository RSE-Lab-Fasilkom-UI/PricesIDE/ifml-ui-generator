import styled from "styled-components";

export const InputContainer = styled.div`
  label {
    font-family: Helvetica, sans-serif;
    font-weight: bold;
    font-size: 14px;
    margin-bottom: 10px;
    margin-left: 5%;
    color: ${(props) =>
      props.labelColor
        ? props.labelColor
        : props.color
        ? props.color
        : "black"};
  }
`;

export const InputFieldStyle = styled.select`
  padding: 13px;
  margin-bottom: 5px;
  margin-top: 5px;
  width: 90%;
  margin-left: 5%;
  box-sizing: border-box;
  font-family: Helvetica, sans-serif;
  outline: none !important;
  font-size: 0.9em;
  font-weight: normal;
  background-color: ${(props) => (props.bgColor ? props.bgColor : "white")};

  &:hover,
  &:focus {
    background-color: ${(props) =>
      props.bgColor ? `${props.bgColor}4f` : "#00000014"};
    transition: 0.4s;
  }

  border-radius: ${(props) =>
    props.shape === "rounded"
      ? "64px"
      : props.shape === "underline"
      ? "0px"
      : "4px"};
  border: ${(props) =>
    props.shape === "underline"
      ? ""
      : props.borderColor
      ? `1px solid ${props.borderColor}`
      : props.color
      ? `1px solid ${props.color}`
      : "1px solid black"};
  border-bottom: ${(props) =>
    props.shape !== "underline"
      ? ""
      : props.borderColor
      ? `1px solid ${props.borderColor}`
      : props.color
      ? `1px solid ${props.color}`
      : "1px solid black"};
  border-top: ${(props) => props.shape === "underline" && "0px"};
  border-left: ${(props) => props.shape === "underline" && "0px"};
  border-right: ${(props) => props.shape === "underline" && "0px"};
  option {
    color: black;
    background: white;
    display: flex;
    white-space: pre;
    min-height: 20px;
    padding: 0px 2px 1px;
  }
`;

