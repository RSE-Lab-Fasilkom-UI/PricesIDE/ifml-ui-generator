import React from "react";
import { InputContainer, InputFieldStyle } from "./style";

const SelectionField = (props) => {
  const {
    camel_name,
    type,
    dasherized,
    label,
    placeholder,
    defaultValue,
    ref_func,
    disabled,
    variant,
    options,
  } = props;

  var html_props = {
    name: camel_name,
    id: dasherized,
    type: type,
    placeholder: placeholder ? placeholder : "",
    defaultValue: defaultValue ? defaultValue : "",
    ref: ref_func,
    disabled: disabled,
  };

  return (
    <InputContainer {...variant}>
      {{ label } ? (
        <label className="input_label" htmlFor={dasherized}>
          <strong>{label}</strong>
        </label>
      ) : null}
      <InputFieldStyle {...html_props} {...variant}> 
          {options.map((option) => (
              <option value={option.id}>{option.name}</option>
            ))}
      </InputFieldStyle>
    </InputContainer>
  );
};

export default SelectionField;

