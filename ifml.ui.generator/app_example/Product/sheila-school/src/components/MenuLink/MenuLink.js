import React from "react";
import { Link } from "react-router-dom";
import { MenuLinkStyle, LinkStyle } from "./style";

const MenuLink = (props) => {
  const { href, onClick, to } = props;

  if (to) {
    return (
      <LinkStyle>
        <Link to={to} className="link">
          {props.children}
        </Link>
      </LinkStyle>
    );
  }

  return (
    <MenuLinkStyle href={href} onClick={onClick}>
      {props.children}
    </MenuLinkStyle>
  );
};

export default MenuLink;
