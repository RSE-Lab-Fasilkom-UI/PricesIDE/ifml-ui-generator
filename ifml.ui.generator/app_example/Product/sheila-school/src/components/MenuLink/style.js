import styled from "styled-components";

export const MenuLinkStyle = styled.a`
  display: block;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  text-align: left;
`;

export const LinkStyle = styled.a`
  .link {
    display: block;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    height: 100%;
    text-align: left;
  }
`;
