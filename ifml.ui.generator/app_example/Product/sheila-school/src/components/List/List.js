import React from "react";
import { ListStyle } from "./style";

const List = (props) => {
  const { id_name, className, column } = props;

  return (
    <ListStyle id={id_name} column={column} className={className}>
      {props.children}
    </ListStyle>
  );
};

export default List;
