import styled from "styled-components";

export const HeroStyle = styled.div`
  width: 100vw;
  height: 60vh;

  background-image: ${(props) => `url(${props.banner})`};
  background-size: cover;

  color: white;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
