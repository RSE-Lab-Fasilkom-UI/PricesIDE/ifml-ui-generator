import React from "react";
import { HeroStyle } from "./style";

const Hero = (props) => {
  const { variant, children, banner } = props;

  return <HeroStyle banner={banner}>{children}</HeroStyle>;
};

export default Hero;
