import React, { forwardRef } from 'react';

const DateInputField = ({ camel_name, dasherized, placeholder, default_value, value, onClick, onChange }, ref) => (
    <input
        name={camel_name}
        id={dasherized}
        placeholder={placeholder}
        defaultValue={default_value}
        value={value}
        onClick={onClick}
        onChange={onChange}
        ref={ref}
    />
);

export default forwardRef(DateInputField);
