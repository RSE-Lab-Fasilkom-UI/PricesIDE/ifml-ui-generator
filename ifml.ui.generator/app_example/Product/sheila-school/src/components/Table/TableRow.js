import React from "react";

const TableRow = (props) => {
  const { onClick, distinct } = props;

  const onClickfunc = onClick ? onClick : undefined;

  return (
    <tr
      onClick={onClickfunc}
      highlight
      className={distinct ? "distinct-row" : ""}
    >
      {props.children}
    </tr>
  );
};

export default TableRow;

