import React from "react";
import { AboutStyle } from "./style";

const About = (props) => {
  const { variant, children } = props;

  return <AboutStyle>{children}</AboutStyle>;
};

export default About;
