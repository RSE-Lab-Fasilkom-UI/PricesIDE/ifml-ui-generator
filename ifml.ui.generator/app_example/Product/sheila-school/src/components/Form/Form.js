import React from "react";
import { FieldsetContainer, FormContainer } from "./style";

const Form = (props) => {
  const { title, on_submit, id_name, variant } = props;

  return (
    <FormContainer id={id_name} {...on_submit} {...variant}>
      <FieldsetContainer {...variant}>
        <h2>{title}</h2>
        {props.children}
      </FieldsetContainer>
    </FormContainer>
  );
};

export default Form;
