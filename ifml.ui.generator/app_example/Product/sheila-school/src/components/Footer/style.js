import styled from "styled-components";

export const FooterStyle = styled.footer`
  display: flex;
  width: 100vw;
  padding: 2rem;
  background-color: darkgray;

  .contact {
    width: 100%;
  }

  .sitemap {
    width: 100%;
  }
`;
