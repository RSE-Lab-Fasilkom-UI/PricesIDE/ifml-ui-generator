import React from "react";
import { FooterStyle } from "./style";

const Footer = (props) => {
  const { children, variant } = props;
  return <FooterStyle {...variant}>{children}</FooterStyle>;
};

export default Footer;
