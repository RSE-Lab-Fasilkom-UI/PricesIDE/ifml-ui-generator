import React from 'react';
import queryString from 'query-string';
import Button from '../components/Button/Button';
import Detail from '../components/Detail/Detail';
import VisualizationAttr from '../components/VisualizationAttr/VisualizationAttr';
import CallexpensedeleteService from '../services/call-expense-delete.service'
class DetailExpense extends React.Component {
	state = {}
	
	componentWillMount = () => {
		this.expenseData = this.props.objectDetailExpense;this.idExpense = String(this.expenseData.id);
	};
	
	Hapus = async () => {
		const data = await CallexpensedeleteService.call({
			id: this.idExpense,
		});
		
		this.props.history.push({
			pathname: '/catatan-pengeluaran',
            search: queryString.stringify({
                jsonAllExpense: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
	}
	render() {
		return (
			<Detail variant={{'shape': 'default', 'borderColor': 'blue', 'bgColor': 'white'}}>
				{/* //Data Binding Expense Data*/}
				<VisualizationAttr title_name="Tanggal" content={ this.expenseData && this.expenseData.datestamp } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Deskripsi" content={ this.expenseData && this.expenseData.description } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Jumlah" content={ this.expenseData && this.expenseData.amount } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Nama Program" content={ this.expenseData && this.expenseData.programName } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<VisualizationAttr title_name="Jenis Pengeluaran" content={ this.expenseData && this.expenseData.idCoa } variant={{'shape': 'default', 'color': 'black', 'titleColor': 'blue', 'uppercase': 'no'}}  />
				<Button onClick={(e) => {e.preventDefault(); this.Hapus()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Hapus" />
			</Detail>
		)
	}
}

export default DetailExpense;
