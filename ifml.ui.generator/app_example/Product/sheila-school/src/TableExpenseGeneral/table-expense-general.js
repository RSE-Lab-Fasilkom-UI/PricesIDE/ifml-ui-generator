//Table
import React from 'react';
import Button from '../components/Button/Button';
import TableRow from '../components/Table/TableRow';
import TableCell from '../components/Table/TableCell';
import queryString from 'query-string';

class TableExpenseGeneral extends React.Component {
	state = {}
	
	componentWillMount = () => {
			this.expenseGeneralRow = new Expense(this.jsonExpenseUmum);this.expenseGeneralRow.amount = this.currencyFormatDE(this.expenseGeneralRow.amount);
	};
	currencyFormatDE(num) {
        if (!num) {
            return 0
        }
        return num
    }
	
	indentBasedOnLevel(content, level) {
        switch (level) {
            case 1:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 2:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            case 5:
                return <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{content}</span>
            default:
                return content
        }
    }

    setDistinctRow(content) {
        const DISTINCT_ROW = [
            "Aktivitas Operasional",
            "Aktivitas Investasi",
            "Aktivitas Pendanaan",
            ""
        ];
        if (DISTINCT_ROW.indexOf(content) > -1) {
            return true;
        }
        return false;
    }



	render() {
		return (
			<TableRow  distinct={false}>
				{/* //Data Binding Expense General Row*/}
				
				<TableCell content={ this.expenseGeneralRow && this.expenseGeneralRow.datestamp } />
				
				<TableCell content={ this.expenseGeneralRow && this.expenseGeneralRow.programName } />
				
				<TableCell content={ this.expenseGeneralRow && this.expenseGeneralRow.coaName } />
				
				<TableCell content={ this.expenseGeneralRow && this.expenseGeneralRow.amount } />
			</TableRow>
		)
	}
}

export default TableExpenseGeneral;
