//Halaman Ubah Pemasukan Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import CallprogramlistService from '../services/call-program-list.service';
import CallchartofaccountlistService from '../services/call-chart-of-account-list.service';
import UbahPemasukanComponent from '../UbahPemasukan/ubah-pemasukan.js'

class HalamanUbahPemasukan extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		const program = await CallprogramlistService.call()
		const chartofaccount = await CallchartofaccountlistService.call()
		this.setState({
			objectIncome:JSON.parse(queryString.parse(this.props.location.search).objectIncome),
			program: program['data']['data'],
			chartofaccount: chartofaccount['data']['data'],
		});
	};

	
	render() {
		return (
			<React.Fragment>
					{ 
					this.state.objectIncome !== undefined && 
					this.state.program &&
					this.state.chartofaccount &&
					<UbahPemasukanComponent {...this.props} 
					objectIncome={this.state.objectIncome}
					program={this.state.program}
					chartofaccount={this.state.chartofaccount}
					></UbahPemasukanComponent>}
			</React.Fragment>
		);
	}
}

export default HalamanUbahPemasukan;

