//Halaman Tambah Pemasukan Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import CallprogramlistService from '../services/call-program-list.service';
import CallchartofaccountlistService from '../services/call-chart-of-account-list.service';
import TambahkanPemasukanComponent from '../TambahkanPemasukan/tambahkan-pemasukan.js'

class HalamanTambahPemasukan extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		const program = await CallprogramlistService.call()
		const chartofaccount = await CallchartofaccountlistService.call()
		this.setState({
			program: program['data']['data'],
			chartofaccount: chartofaccount['data']['data'],
		});
	};

	
	render() {
		return (
			<React.Fragment>
					{
					this.state.program &&
					this.state.chartofaccount &&
					<TambahkanPemasukanComponent {...this.props}
					program={this.state.program}
					chartofaccount={this.state.chartofaccount}
					></TambahkanPemasukanComponent>
					}
					
			</React.Fragment>
		);
	}
}

export default HalamanTambahPemasukan;

