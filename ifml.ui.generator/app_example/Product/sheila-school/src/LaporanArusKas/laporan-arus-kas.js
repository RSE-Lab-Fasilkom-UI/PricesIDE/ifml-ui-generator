//Laporan Arus Kas Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';


import Table from '../components/Table/Table';
import TableRow from '../components/Table/TableRow';
import TableCell from '../components/Table/TableCell';
import TableBody from '../components/Table/TableBody';
import TableHead from '../components/Table/TableHead';


import TableChartOfAccountEntryComponent from '../TableChartOfAccountEntry/table-chart-of-account-entry.js'


class LaporanArusKas extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			jsonAllChartOfAccountEntry:JSON.parse(queryString.parse(this.props.location.search).jsonAllChartOfAccountEntry),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				<Table id='table-TableChartOfAccountEntryComponent' variant={{'headBgColor': 'blue', 'headColor': 'white', 'headTextAlign': 'center', 'headBorderColor': 'blue', 'headUppercase': 'yes', 'headFontWeight': '700', 'bodyBorderColor': 'black', 'bodyFontWeight': 'normal', 'evenBgColor': 'lightgray', 'evenColor': 'black', 'evenBorderColor': 'black', 'distinctBgColor': 'lightblue', 'distinctColor': 'black'}}>
					<TableHead>
						<TableRow>
							{/* //Data Binding Chart Of Account Entry Element*/}
							<TableCell id='Nama'>Nama</TableCell>
							<TableCell id='Jumlah'>Jumlah</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{ this.state.jsonAllChartOfAccountEntry && this.state.jsonAllChartOfAccountEntry.map((jsonAllChartOfAccountEntry) => (
							<TableChartOfAccountEntryComponent {...this.props} jsonAllChartOfAccountEntry={jsonAllChartOfAccountEntry}/>
						))} 
					
					</TableBody>
				</Table>
			</React.Fragment>
		);
	}
}

export default LaporanArusKas;

