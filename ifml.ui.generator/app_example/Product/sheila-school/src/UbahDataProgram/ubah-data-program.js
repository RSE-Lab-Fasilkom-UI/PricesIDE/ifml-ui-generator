import React from 'react';
import InputField from '../components/InputField/InputField';
import Form from '../components/Form/Form';
import Button from '../components/Button/Button';
import queryString from 'query-string';
import SelectionField from '../components/SelectionField/SelectionField'
import CallprogramupdateService from '../services/call-program-update.service'

class UbahDataProgram extends React.Component {
	state = {};
	
	componentWillMount = () => {
		this.programData = this.props.objectEditProgram;this.id = this.programData.id;this.namaProgram = this.programData.name;this.deskripsi = this.programData.description;this.partner = this.programData.partner;this.target = this.programData.target;this.urlGambarProgram = this.programData.logoUrl;
	};

	Kirim = async () => {
		const data = await CallprogramupdateService.call({
			id: this.idInput.value,
			name: this.namaProgramInput.value,
			description: this.deskripsiInput.value,
			target: this.targetInput.value,
			partner: this.partnerInput.value,
			logoUrl: this.urlGambarProgramInput.value,
		});

		this.props.history.push({
			pathname: '/halaman-detail-program',
            search: queryString.stringify({
                objectDetailProgram: JSON.stringify(data['data'] ? data['data']['data'] : [])
            })
		})
		
	}
	
	render() {
		return (
			<Form title="Ubah Data Program" id_name="ubah-data-program" variant={{'shape': 'default', 'color': 'blue', 'borderColor': 'transparent', 'bgColor': 'white', 'alignment': 'center'}}>
				<InputField camel_name="idInput" type="" dasherized="input-id" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="id" placeholder="Fill the id"  defaultValue={ this.id } ref_func={e => {this.idInput = e;}}/>
				<InputField camel_name="namaProgramInput" type="" dasherized="input-nama-program" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Nama Program" placeholder="Fill the Nama Program"  defaultValue={ this.namaProgram } ref_func={e => {this.namaProgramInput = e;}}/>
				<InputField camel_name="deskripsiInput" type="" dasherized="input-deskripsi" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Deskripsi" placeholder="Fill the Deskripsi"  defaultValue={ this.deskripsi } ref_func={e => {this.deskripsiInput = e;}}/>
				<InputField camel_name="targetInput" type="" dasherized="input-target" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Target" placeholder="Fill the Target"  defaultValue={ this.target } ref_func={e => {this.targetInput = e;}}/>
				<InputField camel_name="partnerInput" type="" dasherized="input-partner" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="partner" placeholder="Fill the partner"  defaultValue={ this.partner } ref_func={e => {this.partnerInput = e;}}/>
				<InputField camel_name="urlGambarProgramInput" type="" dasherized="input-url-gambar-program" variant={{'shape': 'default', 'color': 'black', 'labelColor': 'blue'}} label="Url Gambar Program" placeholder="Fill the Url Gambar Program"  defaultValue={ this.urlGambarProgram } ref_func={e => {this.urlGambarProgramInput = e;}}/>
				<Button onClick={(e) => {e.preventDefault(); this.Kirim()}} variant={{"shape": "default", "color": "white", "bgColor": "blue", "borderColor": "blue"}} text="Kirim" />
			</Form>
		)
	}
}
export default UbahDataProgram;
