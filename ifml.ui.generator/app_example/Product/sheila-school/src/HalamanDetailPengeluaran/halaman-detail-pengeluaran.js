//Halaman Detail Pengeluaran Containers
import React from 'react';
import {
    AuthConsumer
} from '../Authentication';
import queryString from 'query-string';
import {
    withRouter
} from 'react-router-dom';





import DetailExpenseComponent from '../DetailExpense/detail-expense.js'

class HalamanDetailPengeluaran extends React.Component {
	state = {};
	
	componentDidMount = async () => {
		this.setState({
			objectDetailExpense:JSON.parse(queryString.parse(this.props.location.search).objectDetailExpense),
		});
	};

	
	render() {
		return (
			<React.Fragment>
				{this.state.objectDetailExpense && <DetailExpenseComponent {...this.props} objectDetailExpense={ this.state.objectDetailExpense}></DetailExpenseComponent>}
			</React.Fragment>
		);
	}
}

export default HalamanDetailPengeluaran;

