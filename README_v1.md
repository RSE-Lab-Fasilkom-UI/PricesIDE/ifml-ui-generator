# IFML-UI Generator Repository

Repositori ini digunakan untuk mengembangkan IFML-UI Generator sebagai Eclipse Plugin.

## Getting Started
### Prerequisites

Berikut merupakan tools-tools yang perlu diinstall atau didownload sebelum menjalankan projek IFML UI Generator
1. [Java 11]( https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html)
2. [Eclipse Modeling Tools 2020-12](https://www.eclipse.org/downloads/packages/release/2020-12/r/eclipse-modeling-tools)
3. [Acceleo 3.7](http://download.eclipse.org/acceleo/updates/releases/3.7)
4. Clone branch `ilma/dev` pada repositori ini
5.  Clone branch `ilma/dev`pada repositori [ifml-aisco](https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/ifml-aisco/-/tree/ilma/dev) untuk mendapatkan model IFML diagram

### Menjalankan Acceleo Project Tanpa Plug-in
Tutorial ini digunakan untuk menjalankan Acceleo project tanpa perlu menginstall plug-in. Tutorial ini digunakan pada tahap pengembangan plugin. 
#### Membuka project pada Eclipse
1. Buka Eclipse Modeling Tools anda pada workspace yang diinginkan.
2. Pada tool bar atas silahkan klik `File>Open Projects from File System` pada `import source` arahkan pada folder `ifml-ui-generator` menggunakan tombol `Directory...` kemudian klik `Finish`
3. Lakukan langkah yang sama pada langkah 2 untuk membuka folder `ifml-aisco`
#### Menjalankan Program untuk menghasilkan website
1. pada folder `app_example` salin folder `template` lalu ubah danam folder tersebut dengan nama website yang diinginkan. 
2. Pada tool bar atas silahkan klik `File>Open Projects from File System` pada `import source` arahkan pada folder hasil salinan `template` tadi, menggunakan tombol `Directory...` kemudian klik `Finish`
3. Klik kanan pada `ifml.ui.generator>src>ifml.ui.generator.main>generate_ui.mtl` kemudian klik `Run as > Launch Acceleo Application` 
4. Pilih model yang ingin di transformasi dari `aisco-ifml`
5. Atur target agar mengarah ke salinan template tadi menggunakan tombol `browse` lalu klik tombol `Run`
6. Ulangi langkah ke 3-5 pada model-model yang diperlukan. 
7. Jika sudah buka folder proyek website anda (hasil salinan template), hapus semua folder `MainMenu` yang tidak berhubungan dengan fitur yang anda pilih didalam folder `src`.
8. Buka `src\App\app.js` hapus import komponen dan rooting fitur yang tidak anda perlukan.
9. Buka `src\FeatureMainMenu\feature-main-menu.js` hapus import komponen dan menu pada fitur yang tidak diperlukan. 
10. Buka terminal anda pada root folder proyek website anda, lalu lakukan `npm install` kemudian `npm run start`. maka Website anda dapat dijalankan dilocal pada alamat `http://localhost:3000/`
Untuk lebih jelasnya anda dapat melihat video contoh pada [link berikut](https://drive.google.com/file/d/138L1E2NzxnIscnwRRoF3maQgqL8szanU/view). 

### Installing The Plugin 
#### Via Release
1. Download the zip (ifml.ui.gnerator.updatesite.zip) releases and unzip it 
2. On top toolbar, click on `Help` > `Install New Software`
3. Click on `Add` > `Local`
4. Select the file you downloaded before
5. Make sure you check `Uncategorized` 
6. Click `Finish` or `Next`. You will get a notification about untransted plugin,  click `Install Anyway`
7. After the plugin has been installed, you will be asked to restart your Eclipse, click `Restart Now` 
8. Now the plugin has been installed and ready to use

