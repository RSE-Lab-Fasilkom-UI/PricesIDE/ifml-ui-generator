package ifml.ui.generator.ui.console;

import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class UIGeneratorConsole {
	
	private static final String CONSOLE_NAME = "UIGenerator Console";
	private static MessageConsole consoleInstance;
	private static MessageConsoleStream consoleStream;

	private  UIGeneratorConsole() {
		// TODO Auto-generated constructor stub
	}
	
	private static MessageConsole findConsole() {
		ConsolePlugin consolePlugin = ConsolePlugin.getDefault();
		IConsoleManager consoleManager = consolePlugin.getConsoleManager();
		IConsole[] consoleList = consoleManager.getConsoles();
		for (IConsole console : consoleList) {
			if (console.getName().equals(CONSOLE_NAME)) {
				return (MessageConsole) console;
			}
		}
		MessageConsole console = new MessageConsole(CONSOLE_NAME, null);
		consoleManager.addConsoles(new IConsole[] {console});
		return console;
	}
	
	private static MessageConsole getConsoleInstance() {
		if (consoleInstance == null) {
			consoleInstance = findConsole();
		}
		return consoleInstance;
	}
	
	private static MessageConsoleStream getConsoleStream() {
		if (consoleStream == null) {
			consoleStream = getConsoleInstance().newMessageStream();
		}
		return consoleStream;
	}
	
	public static void showConsole() {
		MessageConsole console = getConsoleInstance();
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
		IWorkbenchPage page = win.getActivePage();
		try {
			IConsoleView view = (IConsoleView) page.showView(IConsoleConstants.ID_CONSOLE_VIEW);
			view.display(console);
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void println(String message) {
		print(message + "\n");
	}
	
	public static void print(String message) {
		getConsoleStream().print(message);
	}
}
