package ifml.ui.generator.ui.handlers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.dialogs.ListDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import freemarker.template.utility.StringUtil;
import ifml.ui.generator.main.Generate_ui;
import ifml.ui.generator.ui.common.RunnerFactory;
import ifml.ui.generator.ui.common.RunnerOperator;
import ifml.ui.generator.ui.console.UIGeneratorConsole;
import ifml.ui.generator.utils.TargetProjectMaker;
import ifml.ui.generator.utils.TemplateUtils;

public class GenerateUIHandler implements IHandler {
	
	private static IProject[] getProjectList(String... exclude) {
		List<String> excludeList = Arrays.asList(exclude);
		Stream<IProject> projectStreams = Arrays.stream(ResourcesPlugin.
				getWorkspace().getRoot().getProjects());
		return projectStreams
				.filter(project -> !excludeList.contains(project.getName()))
				.toArray(IProject[]::new);
	}
	
	private static List<IFile> getSelectedFile(ExecutionEvent event) {
		TreeSelection selection = (TreeSelection) HandlerUtil.getActiveMenuSelection(event);
		return selection.toList();
	}
	
	private static boolean projectHasSelectedFeature(IProject project) {
		return project.getFile("SelectedFeature").exists();
	}
	private static boolean projectIsReact(IProject project) {
		IFolder srcFolder = project.getFolder("src");
		return project.getFile("package.json").exists() &&
				srcFolder.exists() &&
				srcFolder.getFile("index.js").exists();
	}
	
	private static ListDialog createProjectSelectorDialog(Object projectList, String title, String messages) {
		ListDialog dialog = new ListDialog(null);
		dialog.setContentProvider(new ArrayContentProvider());
		dialog.setLabelProvider(new WorkbenchLabelProvider());
		dialog.setTitle(title);
		dialog.setMessage(messages);
		dialog.setInput(projectList);
		return dialog;
	}

	private static IProject selectProjectFromDialog() {
		IProject selectedProject = null;
		IProject[] filteredProject = Arrays.stream(getProjectList())
				.filter(project -> projectHasSelectedFeature(project))
				.toArray(IProject[]::new);
		ListDialog dialog = createProjectSelectorDialog(filteredProject,
				"Select Target Project",
				"Select target project to generate UI");
		if (dialog.open() == Window.OK) {
			Object[] result = dialog.getResult();
			if (result.length > 0) {
				selectedProject =  (IProject)Platform.getAdapterManager().getAdapter(result[0], IProject.class);
			} else {
				UIGeneratorConsole.println("No target project selected.");
			}

		} else {
			UIGeneratorConsole.println("Process cancelled.");
		}
		dialog.close();
		return selectedProject; 
	}
	
	private static String projectsToProductsName(String name) {
		return StringUtil.capitalize(name.replaceAll("-+", " "));
	}
	
	private static IInputValidator createProductsNameValidator(IProject targetProject) {
		return new IInputValidator() {
			@Override
			public String isValid(String inputName) {
				// TODO Auto-generated method stub
				if (inputName.length() == 0) {
					return "Product name cannot be empty";
				}
				if (inputName.stripLeading() != inputName) {
					return "Products name cannot starts with whitespace";
				}
				if (inputName.stripTrailing() != inputName) {
					return "Products name cannot ends with whitespace";
				}
				return null;
			}
		};
	}
	
	private static String getProductsNameFromDialog(IProject targetProject) {
		IInputValidator nameValidator = createProductsNameValidator(targetProject);
		InputDialog customDialog = new InputDialog(null,
				"Project Name Input",
				"Insert Project Name",
				projectsToProductsName(targetProject.getName()),
				nameValidator);
		if (customDialog.open() == Window.OK) {
			return customDialog.getValue();			
		}
		return null;
	}

	public void addHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

	public void dispose() {
		// TODO Auto-generated method stub

	}
	
	private static ListDialog createTemplateSelectorDialog() {
		String[] templateList = TemplateUtils.getTemplateSelection();
		ListDialog dialog = new ListDialog(null);
		dialog.setContentProvider(new ArrayContentProvider());
		dialog.setLabelProvider(new LabelProvider());
		dialog.setTitle("Select Template");
		dialog.setMessage("Silahkan pilih template yang diinginkan");
		dialog.setInput(templateList);
		return dialog;
	}

	private static String[] getTemplateNameAndPathFromDialog() {
		ListDialog dialog = createTemplateSelectorDialog();
		String selectedTemplate = null;
		String templatePath = null;
		if (dialog.open() == Window.OK) {
			Object[] result = dialog.getResult();
			if (result.length > 0) {
				selectedTemplate = TemplateUtils.getValue((String) result[0]);
				templatePath = TemplateUtils.getSelectedTemplatePath(selectedTemplate);
			} else {
				UIGeneratorConsole.println("No template selected.");
				return null;
			}
			dialog.close();
		}
		return new String[]{selectedTemplate,templatePath};
	}
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		// Select Empty Project with SelectedFeature
		// Copy Template
		// Generate UI
		UIGeneratorConsole.showConsole();
	
		List<IFile> selectedFiles = getSelectedFile(event);
		UIGeneratorConsole.println("Selecting target project...");
		IProject targetProject = selectProjectFromDialog();
		
		if (targetProject != null) {
			String[] templateStrings = getTemplateNameAndPathFromDialog();
			String selectedTemplate = templateStrings[0],templatePath = templateStrings[1];

//			TargetProjectMaker.copyTemplate(targetProject,templatePath);
			Job copyTemplateJob = TargetProjectMaker.getCopyTemplateJob(targetProject, templatePath);
			copyTemplateJob.addJobChangeListener(new JobChangeAdapter(){
				@Override
				public void done(IJobChangeEvent event){
				    Display.getDefault().asyncExec(new Runnable() {
				    	@Override
				    	public void run() {
							String productsName = getProductsNameFromDialog(targetProject);
							if (productsName == null) {
								UIGeneratorConsole.println("Process cancelled.");
							} else {
								TargetProjectMaker.insertProductsNameToApp(targetProject, productsName);
								IFile selectedFeatureFile = targetProject.getFile("SelectedFeature");
								if (selectedFeatureFile.exists()) {
									try {
										String selectedFeatureString = new String(selectedFeatureFile.getContents().readAllBytes(), StandardCharsets.UTF_8);
										String selectedRegex = "^(" + selectedFeatureString.strip().replaceAll("[\r\n]+", "|") + ")$";
										Generate_ui.setSelectedRegex(selectedRegex);
										IRunnableWithProgress generatorRunner = RunnerFactory.createGeneratorRunner(selectedFiles, targetProject,selectedTemplate);
										RunnerOperator.executeRunner(generatorRunner);
									} catch (IOException | CoreException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								} else {
									UIGeneratorConsole.println("SelectedFeature not found in target.");
								}
							}
				    	}
				    });
				}
			});
			
			TargetProjectMaker.showMonitor();
			copyTemplateJob.schedule();


		}
		UIGeneratorConsole.println("Generate UI end.");
		return null;
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isHandled() {
		// TODO Auto-generated method stub
		return true;
	}

	public void removeHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

}
