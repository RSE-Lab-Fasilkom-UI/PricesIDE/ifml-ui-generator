package ifml.ui.generator.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.dialogs.ListDialog;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import freemarker.template.Template;
import ifml.ui.generator.ui.console.UIGeneratorConsole;
import ifml.ui.generator.utils.TargetProjectMaker;
import ifml.ui.generator.utils.TemplateUtils;

@Deprecated
/**
 * @deprecated
 * Starting from version 3.1.3, the Copy Template command is integrated in {@link GenerateUIHandler}.
 * Therefore, this handler is no longer used as an extension in MANIFEST.MF and may be removed in future releases
 */
public class CopyTemplateHandler implements IHandler {	
	private static IProject getSelectedProject(ExecutionEvent event) {
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getActiveMenuSelection(event);
		return (IProject) selection.getFirstElement();
	}

	private static ListDialog createTemplateSelectorDialog() {
		String[] templateList = TemplateUtils.getTemplateSelection();
		ListDialog dialog = new ListDialog(null);
		dialog.setContentProvider(new ArrayContentProvider());
		dialog.setLabelProvider(new LabelProvider());
		dialog.setTitle("Select Template");
		dialog.setMessage("Silahkan pilih template yang diinginkan");
		dialog.setInput(templateList);
		return dialog;
	}
	
	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// TODO Auto-generated method stub
		UIGeneratorConsole.showConsole();
		final IProject selectedProject = getSelectedProject(event);
		ListDialog dialog = createTemplateSelectorDialog();
		String templatePath = null;
		if (dialog.open() == Window.OK) {
			Object[] result = dialog.getResult();
			if (result.length > 0) {
				String selectedTemplate = (String) result[0];
				templatePath = TemplateUtils.getSelectedTemplatePath(selectedTemplate);
			} else {
				UIGeneratorConsole.println("No target project selected.");
			}
			dialog.close();
		}
		
		TargetProjectMaker.copyTemplate(selectedProject,templatePath);

		return null;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isHandled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub

	}

}
