package ifml.ui.generator.ui.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.operation.IRunnableWithProgress;

import ifml.ui.generator.ui.Activator;

public class RunnerFactory {
	
	public static IRunnableWithProgress createGeneratorRunner(List<IFile> fileList) {
		return createGeneratorRunner(fileList, null,"");
	}
	
	public static IRunnableWithProgress createGeneratorRunner(List<IFile> fileList, IContainer outputFolder,String selectedTemplate) {
		ArrayList args = new ArrayList<String>();
		args.add(selectedTemplate);
		return createGeneratorRunner(fileList, args, outputFolder);
	}

	public static IRunnableWithProgress createGeneratorRunner(List<IFile> fileList,
			List<? extends Object> arguments, IContainer outputFolder) {
		return new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) {
				try {
					Iterator<IFile> filesIterator = fileList.iterator();
					while (filesIterator.hasNext()) {
						IFile model = (IFile)filesIterator.next();
						URI modelURI = URI.createPlatformResourceURI(model.getFullPath().toString(), true);
						IContainer targetFolder = outputFolder;
						try {
							if (targetFolder == null) {
								IContainer target = model.getProject().getFolder("src-gen");
								targetFolder = target;
							}
							GenerateAll generator = new GenerateAll(modelURI, targetFolder, arguments);
							generator.doGenerate(monitor);
						} catch (IOException e) {
							IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
							Activator.getDefault().getLog().log(status);
						} finally {
							if (targetFolder != null) {
								targetFolder.getProject().refreshLocal(IResource.DEPTH_INFINITE, monitor);
							}
						}
					}
				} catch (CoreException e) {
					IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, e.getMessage(), e);
					Activator.getDefault().getLog().log(status);
				}
			}
		};
	}
	
}
