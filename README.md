# IFML-to-React UI Generator

**UI Generator is an Eclipse Plugin that transforms an IFML Diagram into a fully working React application.** 

This tool is a part of Prices-IDE, a development framework built for Software Product Line Engineering (SPLE). 


## Table of Contents
[[_TOC_]]

## Getting Started

### Prerequisites

Listed below are the requirements that need to be downloaded or installed before we can run the UI Generator. 

- [ ] [Java 11]( https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html)
- [ ] [Eclipse Modeling Tools 2020-12](https://www.eclipse.org/downloads/packages/release/2020-12/r/eclipse-modeling-tools)
- [ ] [Acceleo 3.7](http://download.eclipse.org/acceleo/updates/releases/3.7)
- [ ]  IFML Diagram. Clone branch `vanni/dev-merge-to-alisha-dop` from [ifml-aisco](https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/ifml-aisco/-/tree/dev/iterasi-4). 

### Plugin Installation

1. On top toolbar, click on `Help` > `Install New Software`

    ![Install new software](https://cdn.discordapp.com/attachments/999549689240244315/1060154384522166322/IwOiN67QTE.png)
2. Work with: [https://amanah.cs.ui.ac.id/priceside/ifml-ui-generator/updatesite/](https://amanah.cs.ui.ac.id/priceside/ifml-ui-generator/updatesite/)
    > 💡 The full Prices-IDE Eclipse Plugins can be installed from [https://amanah.cs.ui.ac.id/priceside/updatesite/](https://amanah.cs.ui.ac.id/priceside/updatesite/) 
4. Select `IFML UI Generator`
5. Click `Next` and approve all licenses, then click `Finish`

    ![Installing UI Generator Plugin](https://cdn.discordapp.com/attachments/999549689240244315/1060155517965713490/image.png)
6. If you get a window pop-up about untrunsted plugin,  click `Install Anyway`
7. After the plugin has been installed, you will be asked to restart your Eclipse, click `Restart Now`
8. Now the plugin has been installed and ready to use.

### Using The Plugin 

1. Create empty folder that will contain generated front end application code.

2. Open it in eclipse.
3. Right click on the empty folder. Choose `IFML UI Generator > Copy Template Here` to populate the folder with a React application boilerplate.

    ![Copy template here](https://cdn.discordapp.com/attachments/999549689240244315/1060173982453747772/image.png)

> ⚠ Prerequisites:
> Steps 4 onwards need to be executed only after **SelectedFeature** are generated.
> <details><summary>
> 
> **How to generate SelectedFeature?**
>
> </summary>
> 
> Complete steps can be found on RSE-Lab-Fasilkom-UI/PricesIDE/winvmj-composer>.
> 
> 1. Clone this repository `https://gitlab.com/RSE-Lab-Fasilkom-UI/PricesIDE/winvmj-project-amanah/-/tree/staging?ref_type=heads`
> 2. Open `Amanah` directory on Eclipse.
> 3. Right-click on `Amanah` and find `FeatureIDE > WinVMJ > Generate SelectedFeature`
>   ![Generate SelectedFeature](https://cdn.discordapp.com/attachments/814323773696114690/1133035122237657178/generate-selected-feature.png)
> 4. A window will pop up, pick the project folder of your choice and click `OK`. If it doesn't show, refer to Step 6. 
>   ![Select project folder](https://cdn.discordapp.com/attachments/999549689240244315/1060174481454276619/image.png)
> 5. `SelectedFeature` file will be generated and contain features according to the one you've selected in Feature Selection.
> 
> </details>

4. Open folder `aisco-ifml` from `ifml-aisco` repository.

5. Right click on `aisco.core`. Choose `IFML UI Generator > Generate UI`.

    ![Generate UI](https://cdn.discordapp.com/attachments/999549689240244315/1060163875519078430/image.png)
6. Select your app folder and click `OK`. 

    ![Select app folder](https://cdn.discordapp.com/attachments/999549689240244315/1060164289874362409/image.png)
    <details><summary>⚠ <b>Why is my app folder not showing as an option?</b></summary>
    The folders that will show up as an option are those containing `packages.json` file and a `src` folder. Try making sure if you did Steps 1 to 3 correctly. 
    </details>

7. Input your app name and click `OK`. This will be treated as the website's name and displayed on the Navbar and Footer of every page of the site. 

    ![Input app name](https://cdn.discordapp.com/attachments/999549689240244315/1060166015666896946/image.png)
    <details><summary>⚠ <b>Can I rename my site later?</b></summary>
    Currently, no. But you can always redo Steps 4 to 8 to redo the UI Generation process and pick a different name for your site.  
    </details>

8. After the process finished, you can find generated front end application code on your app folder.

### Running Generated React Application

1. If this is your first time running the application, install the required dependencies by running the command below in your terminal. 

    ```
    npm install
    ```
2. Run the React application using the command below. 
    ```
    npm start
    ```
    Open a new terminal and run the command below. This will start the Static Server on `localhost:3003`.
    > ⚠ **This step is required when it's your first time running the application.** Otherwise, it's only needed for when you want to customize the site's appearance or access/modify the organization info pages (static pages). 
    ```
    npm run json:server
    ```
3. Your website will be running on [`http://localhost:3000`](http://localhost:3000).

### Tutorial Video

The video below shows the end-to-end process of generating a web application using Prices-IDE. Bear in mind that the video is silent. You can find timestamps in the comments. 

[![Process tutorial video](https://img.youtube.com/vi/p3xkrHeOCJY/0.jpg)](https://www.youtube.com/watch?v=p3xkrHeOCJY)

Video can also be accessed on: [https://youtu.be/p3xkrHeOCJY](https://youtu.be/p3xkrHeOCJY)

## How to Contribute

TBA.
